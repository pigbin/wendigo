﻿using UnityEngine;
using UnityEditor;

[CustomEditor( typeof (TemperatureZone) )]
[CanEditMultipleObjects]
public class TemeratureZoneEditor : Editor {

	private void OnSceneGUI() {
		var myTarget = (TemperatureZone) target;

		// SPHERES
		// ==============================================================
		// Get colour relative to current temperature
		float percentage = (TemperatureZone.TemperatureMaximum - myTarget.Temperature) / (TemperatureZone.TemperatureMaximum - TemperatureZone.TemperatureMinimum);
		Color colourTemperature = Color.Lerp( TemperatureZone.MinColour, TemperatureZone.MaxColour, 1 - percentage );

		//Draw min sphere
		colourTemperature.a = 0.5f;
		Handles.color = colourTemperature;
		Handles.SphereCap( 0, myTarget.transform.position, myTarget.transform.rotation, myTarget.MinimumZoneRange );

		// Draw max sphere
		colourTemperature.a = 0.25f;
		Handles.color = colourTemperature;
		Handles.SphereCap( 0, myTarget.transform.position, myTarget.transform.rotation, myTarget.ZoneRange );


		// SCALE
		// ==============================================================
		Handles.color = Color.white;
		float size = HandleUtility.GetHandleSize( myTarget.transform.position );
		myTarget.ZoneRange = Handles.ScaleSlider( myTarget.ZoneRange, myTarget.transform.position, Vector3.up, Quaternion.identity, 2f * size, 0.01f );
		myTarget.MinimumZoneRange = Handles.ScaleSlider( myTarget.MinimumZoneRange, myTarget.transform.position, Vector3.up, Quaternion.identity, 1.5f * size, 0.01f );

		Handles.Label( myTarget.transform.position - Vector3.up * 1f, myTarget.Temperature + "° C", GUI.skin.box );
	}

}