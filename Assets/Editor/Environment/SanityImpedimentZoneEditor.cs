﻿using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( SanityImpedimentZone ) )]
[CanEditMultipleObjects]
public class SanityImpedimentZoneEditor : Editor {

	void OnSceneGUI() {
		var myTarget = (SanityImpedimentZone) target;

		// SPHERES
		// ==============================================================
		// Get colour relative to current temperature
		float percentage = (SanityImpedimentZone.ValueMaximum - myTarget.Impediment) / (SanityImpedimentZone.ValueMaximum - SanityImpedimentZone.ValueMinimum);
		Color colourSanity = Color.Lerp( SanityImpedimentZone.MinColour, SanityImpedimentZone.MaxColour, 1 - percentage );

		//Draw min sphere
		colourSanity.a = 0.5f;
		Handles.color = colourSanity;
		Handles.SphereCap( 0, myTarget.transform.position, myTarget.transform.rotation, myTarget.MinimumZoneRange );

		// Draw max sphere
		colourSanity.a = 0.25f;
		Handles.color = colourSanity;
		Handles.SphereCap( 0, myTarget.transform.position, myTarget.transform.rotation, myTarget.ZoneRange );


		// SCALE
		// ==============================================================
		Handles.color = Color.white;
		float size = HandleUtility.GetHandleSize( myTarget.transform.position );
		myTarget.ZoneRange = Handles.ScaleSlider( myTarget.ZoneRange, myTarget.transform.position, Vector3.up, Quaternion.identity, 2f * size, 0.01f );
		myTarget.MinimumZoneRange = Handles.ScaleSlider( myTarget.MinimumZoneRange, myTarget.transform.position, Vector3.up, Quaternion.identity, 1.5f * size, 0.01f );

		// LABEL
		// ==============================================================
		Handles.Label( myTarget.transform.position - Vector3.up * 1f, (myTarget.Impediment * 100) + "%", GUI.skin.box );
	}

}
