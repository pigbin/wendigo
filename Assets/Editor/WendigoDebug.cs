﻿using UnityEditor;
using UnityEngine;


public class WendigoDebug : EditorWindow {

	//[MenuItem( "Wendigo Debug/Play from interior" )]
	public static void PlayFromInterior() {
		if (EditorApplication.SaveCurrentSceneIfUserWantsTo()) {
			EditorPrefs.SetString( "previousScene", EditorApplication.currentScene );

			if (EditorApplication.currentScene != "Assets/Scenes/Interior.unity")
				EditorApplication.OpenScene( "Assets/Scenes/Interior.unity" );

			EditorApplication.isPlaying = true;
		}
	}

	//[MenuItem( "Wendigo Debug/Stop and go back to previous scene" )]
	public static void GoBackToPreviousScene() {
		if (!EditorPrefs.HasKey( "previousScene" )) return;

		string previousScene = EditorPrefs.GetString( "previousScene" );

		if (!EditorApplication.isPlaying) return;
		EditorApplication.isPlaying = false;

		if (EditorApplication.currentScene != previousScene) {
			EditorApplication.OpenScene( previousScene );
			EditorPrefs.SetString( "previousScene", string.Empty );
		}
	}


	[MenuItem( "Window/Wendigo Debug" )]
	public static void ShowWindow() {
		GetWindow( typeof (WendigoDebug) );
	}


	private void OnGUI() {
		// Play/Stop button
		if (!EditorApplication.isPlaying) {
			Rect r = EditorGUILayout.BeginHorizontal( "Button" );
			r.height = 30f;
			if (GUI.Button( r, "Play from interior" ))
				PlayFromInterior();
			EditorGUILayout.EndHorizontal();
		} else {
			Rect r = EditorGUILayout.BeginHorizontal( "Button" );
			r.height = 30f;
			if ( GUI.Button( r, "Stop and load " + EditorPrefs.GetString( "previousScene" ) ) )
				GoBackToPreviousScene();
			EditorGUILayout.EndHorizontal();
		}


		//DrawPlayerStats();
	}


	private void DrawPlayerStats() {
		if (PlayerStats.Instance == null) {
			GUI.Label( new Rect( 5f, 40f, position.width - 10f, 15f ), "No instance of PlayerStats found!", EditorStyles.boldLabel );
			return;
		}

		// Title
		GUI.Label( new Rect( 5f, 40f, position.width - 10f, 15f ), "Player stats", EditorStyles.boldLabel );


		EditorGUI.ProgressBar( new Rect( 5f, 60f, position.width - 10f, 20f ), PlayerStats.Instance.CurrentHunger, "Hunger" );
		EditorGUI.ProgressBar( new Rect( 5f, 85f, position.width - 10f, 20f ), PlayerStats.Instance.CurrentThirst, "Thirst" );
		EditorGUI.ProgressBar( new Rect( 5f, 110f, position.width - 10f, 20f ), PlayerStats.Instance.CurrentSanity, "Sanity" );

		float temperaturePercentage = MathfExtension.RingPercentage( PlayerStats.Instance.CurrentTemperature, 20f, 37f );
		float airTemperaturePercentage = MathfExtension.RingPercentage( PlayerStats.Instance.CurrentAirTemperature, -40f, -20f );
		string tempBarText = "Body temperature: " + PlayerStats.Instance.CurrentTemperature + "°C";
		string airTempBarText = "Air temperature: " + PlayerStats.Instance.CurrentAirTemperature + "°C";

		EditorGUI.ProgressBar( new Rect( 5f, 140f, position.width - 10f, 20f ), temperaturePercentage, tempBarText );
		EditorGUI.ProgressBar( new Rect( 5f, 165f, position.width - 10f, 20f ), airTemperaturePercentage, airTempBarText );
	}

	private void OnInspectorUpdate() {
		Repaint();
	}

}