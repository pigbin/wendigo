Shader "Skybox/CubemapDayTimes" {
Properties {
	_Tint ("Tint Color", Color) = (.5, .5, .5, .5)
	[Gamma] _Exposure ("Exposure", Range(0, 8)) = 1.0
	_Rotation ("Rotation", Range(0, 360)) = 0
	
	[NoScaleOffset] _TexSunrise ("Sunrise Texture (HDR)", Cube) = "grey" {}
	_SunriseBlend ("Sunrise Blend", Range(0,1)) = 0
	[NoScaleOffset] _TexMorning ("Morning Texture (HDR)", Cube) = "grey" {}
	_MorningBlend("Morning Blend", Range(0,1)) = 0
	[NoScaleOffset] _TexNoon ("Noon Texture (HDR)", Cube) = "grey" {}
	_NoonBlend("Noon Blend", Range(0,1)) = 0
	[NoScaleOffset] _TexAfternoon ("Afternoon Texture (HDR)", Cube) = "grey" {}
	_AfternoonBlend("Afternoon Blend", Range(0,1)) = 0
	[NoScaleOffset] _TexSunset ("Sunset Texture (HDR)", Cube) = "grey" {}
	_SunsetBlend("Sunset Blend", Range(0,1)) = 0
	[NoScaleOffset] _TexNight ("Night Texture (HDR)", Cube) = "grey" {}
	_NightBlend("Night Blend", Range(0,1)) = 0
}

SubShader{
	Tags { "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }
	Cull Off ZWrite Off

	Pass {

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		#include "UnityCG.cginc"

		sampler2D _SunTexture;

		samplerCUBE _TexSunrise;
		samplerCUBE _TexMorning;
		samplerCUBE _TexNoon;
		samplerCUBE _TexAfternoon;
		samplerCUBE _TexSunset;
		samplerCUBE _TexNight;
		half4 _TexSunrise_HDR;
		half4 _TexMorning_HDR;
		half4 _TexNoon_HDR;
		half4 _TexAfternoon_HDR;
		half4 _TexSunset_HDR;
		half4 _TexNight_HDR;

		half _SunriseBlend;
		half _MorningBlend;
		half _NoonBlend;
		half _AfternoonBlend;
		half _SunsetBlend;
		half _NightBlend;

		half4 _Tint;
		half _Exposure;
		float _Rotation;


		float4 RotateAroundYInDegrees (float4 vertex, float degrees) {
			float alpha = degrees * UNITY_PI / 180.0;
			float sina, cosa;
			sincos(alpha, sina, cosa);
			float2x2 m = float2x2(cosa, -sina, sina, cosa);
			return float4(mul(m, vertex.xz), vertex.yw).xzyw;
		}


		struct appdata_t {
			float4 vertex : POSITION;
		};

		struct v2f {
			float4 vertex : SV_POSITION;
			float3 texcoord : TEXCOORD0;
		};

		v2f vert (appdata_t v) {
			v2f o;
			o.vertex = mul(UNITY_MATRIX_MVP, RotateAroundYInDegrees(v.vertex, _Rotation));
			o.texcoord = v.vertex.xyz;
			return o;
		}

		fixed4 frag (v2f i) : SV_Target	{
			half4 texSunrise = texCUBE(_TexSunrise, i.texcoord);
			half4 texMorning = texCUBE(_TexMorning, i.texcoord);
			half4 texNoon = texCUBE(_TexNoon, i.texcoord);
			half4 texAfternoon = texCUBE(_TexAfternoon, i.texcoord);
			half4 texSunset = texCUBE(_TexSunset, i.texcoord);
			half4 texNight = texCUBE(_TexNight, i.texcoord);

			half4 textSun = tex2D(_SunTexture, i.texcoord);

			half3 cSunrise = DecodeHDR(texSunrise, _TexSunrise_HDR);
			half3 cMorning = DecodeHDR(texMorning, _TexMorning_HDR);
			half3 cNoon = DecodeHDR(texNoon, _TexNoon_HDR);
			half3 cAfternoon = DecodeHDR(texAfternoon, _TexAfternoon_HDR);
			half3 cSunset = DecodeHDR(texSunset, _TexSunset_HDR);
			half3 cNight = DecodeHDR(texNight, _TexNight_HDR);

			// Blend between textures
			half3 c = cNight;
			c = lerp(c, cSunrise, _SunriseBlend);
			c = lerp(c, cMorning, _MorningBlend);
			c = lerp(c, cNoon, _NoonBlend);
			c = lerp(c, cAfternoon, _AfternoonBlend);
			c = lerp(c, cSunset, _SunsetBlend);
			c = lerp(c, cNight, _NightBlend);

			// Apply tint and exposure
			c = c * _Tint.rgb * unity_ColorSpaceDouble.rgb;
			c *= _Exposure;
			return half4(c, 1);
		}

		ENDCG 
	}
} 	


Fallback Off

}
