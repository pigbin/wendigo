﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class SceneChanger : MonoBehaviourExtended {

	// Singleton
	private static SceneChanger _instance;
	public static SceneChanger Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<SceneChanger>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}

	// Loading parameters and options
	public AsyncOperation AsyncOperation;

	public static bool HasSpawnPoint;
	public static Vector3 SpawnPoint;
	public static float YRotation;

	// UI Stuff
	[SerializeField] private GameObject _canvas;
	[SerializeField] private Image[] _bloodSpatters = new Image[7];


	private void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}


	protected override void Start() {
		base.Start();

		// Get references
		_canvas = transform.GetChild( 0 ).gameObject;
		Transform bloodDrops = _canvas.transform.FindChild( "LoadingScreen/BloodDrops" );
		for (int i = 0; i < _bloodSpatters.Length; i++)
			_bloodSpatters[i] = bloodDrops.GetChild( i ).GetComponent<Image>();
		_canvas.SetActive( false );
	}


	private void OnLevelWasLoaded( int id ) {
		AsyncOperation = null;

		// Get references
		_canvas = transform.GetChild( 0 ).gameObject;
		Transform bloodDrops = _canvas.transform.FindChild( "LoadingScreen/BloodDrops" );
		for ( int i = 0; i < _bloodSpatters.Length; i++ )
			_bloodSpatters[i] = bloodDrops.GetChild( i ).GetComponent<Image>();
		_canvas.SetActive( false );
	}


	private void Update() {
		if (AsyncOperation != null) {
			float progress = AsyncOperation.progress;

			for (int i = 0; i < _bloodSpatters.Length; i++) {
				float bloodProgress = Mathf.Clamp01( progress / ((i + 1) / (float) _bloodSpatters.Length) );
				_bloodSpatters[i].color = new Color(1f, 1f, 1f, bloodProgress);
			}
		}
	}


	/// <summary>
	/// Loads a scene asynchronously and teleports the player to the given spawn point
	/// </summary>
	public void LoadSceneAtLocation( int id, SpawnPointData pointData ) {
		HasSpawnPoint = true;
		SpawnPoint = pointData.SpawnLocation;
		YRotation = pointData.YRotation;
		LoadAsync( id );
	}

	
	/// <summary>
	/// Use this for scenes that do not contain spawn locations
	/// </summary>
	public void LoadAsync( int id ) {
		// Check if the level is available
		if (id > 0 && id > Application.levelCount - 1)
			throw new Exception("The level with the id " + id + " does not exist.");
		
		if(id == 1 && SceneLoadSounds.Instance != null)
			SceneLoadSounds.Instance.EnterPlaneSound();
		if (id == 2)
			GlobalVariables.NeedsSleep = true;
		// TODO: This shoudl NOT be here. Move this to exterior scene manager

		// If it is, load it.
		_canvas.SetActive( true );
		StartCoroutine( LoadLevelAsync( id ) );
	}
	

	private IEnumerator LoadLevelAsync( int id ) {
		AsyncOperation = Application.LoadLevelAsync( id );
		yield return AsyncOperation;
	}
}