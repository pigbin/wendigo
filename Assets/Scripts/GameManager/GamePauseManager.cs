﻿using UnityEngine;

public class GamePauseManager : MonoBehaviour {

	// Singleton pattern
	private static GamePauseManager _instance;
	public static GamePauseManager Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<GamePauseManager>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}

	// Internal event stuff
	public delegate void PauseEnter(bool openPauseMenu);
	public delegate void PauseExit();
	public static event PauseEnter OnPause;
	public static event PauseExit OnResume;

	private bool _paused;

	public static int InputBlockers;



	void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}

	private void Update() {
		//Debug.Log( InputBlockers + " " + !PlayerStats.Instance.IsDead + " " + SceneData.Instance.CanPause );

		if (InputBlockers == 0 && Input.GetKeyDown( KeyCode.Escape ) && !PlayerStats.Instance.IsDead && SceneData.Instance.CanPause)
			if (_paused)
				Unpause();
			else
				Pause( true );
	}


	public void Pause( bool openPauseMenu ) {
		//if (PauseBlockers > 0) return;

		_paused = true;

		if ( OnPause != null )
			OnPause( openPauseMenu );
	}

	public void Unpause() {
		_paused = false;

		if ( OnResume != null )
			OnResume();
	}

}