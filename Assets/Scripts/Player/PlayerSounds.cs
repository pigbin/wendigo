﻿using UnityEngine;

[RequireComponent( typeof (AudioSource) )]
public class PlayerSounds : MonoBehaviour {

	private AudioSource _audioSource;

	[Header( "Sounds" )] public AudioClip[] FootstepSounds;

	[SerializeField] private AudioClip _hungerLow;
	[SerializeField] private AudioClip _hungerMid;
	[SerializeField] private AudioClip _hungerHigh;

	[SerializeField] private AudioClip _thirstLow;
	[SerializeField] private AudioClip _thirstMid;
	[SerializeField] private AudioClip _thirstHigh;

	[SerializeField] private AudioClip _frostStateSound;

	[SerializeField] private AudioClip _gameOverBeep;

	private void Start() {
		// Get references
		_audioSource = GetComponent<AudioSource>();

		// Subscribe to events
		PlayerController.OnStep += Footstep;
		PlayerStats.OnHungerLevelChange += OnHungerChange;
		PlayerStats.OnThirstLevelChange += OnThirstChange;
		PlayerStats.OnBodyTemperatureStateChange += OnTempStateChange;
		PlayerStats.OnPlayerDeath += Death;
	}

	private void OnDestroy() {
		// Unsubscribe from events
		PlayerController.OnStep -= Footstep;
		PlayerStats.OnHungerLevelChange -= OnHungerChange;
		PlayerStats.OnThirstLevelChange -= OnThirstChange;
		PlayerStats.OnBodyTemperatureStateChange -= OnTempStateChange;
		PlayerStats.OnPlayerDeath -= Death;
	}


	// Footsteps
	private void Footstep( int foot ) {
		int randomStep = Random.Range( 0, FootstepSounds.Length );
		_audioSource.PlayOneShot( FootstepSounds[randomStep] );
	}


	// Hunger and Thirst
	private void OnHungerChange( float hunger ) {
		if (hunger >= 0.25f)
			_audioSource.PlayOneShot( _hungerLow );
		else if(hunger >= 0.5f)
			_audioSource.PlayOneShot( _hungerMid );
		else if (hunger >= 0.75f)
			_audioSource.PlayOneShot( _hungerHigh );
	}

	private void OnThirstChange( float thirst ) {
		if (thirst >= 0.25f)
			_audioSource.PlayOneShot( _thirstLow );
		else if (thirst >= 0.5f)
			_audioSource.PlayOneShot( _thirstMid );
		else if (thirst >= 0.75f)
			_audioSource.PlayOneShot( _thirstHigh );
	}


	// Temperatures
	private void OnTempStateChange( TemperatureStates state ) {
		_audioSource.PlayOneShot( _frostStateSound );
	}


	// Death
	private void Death(DeathCause cause) {
		_audioSource.PlayOneShot( _gameOverBeep );
	}

}