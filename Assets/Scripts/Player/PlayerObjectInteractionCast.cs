﻿using UnityEngine;

public class PlayerObjectInteractionCast : MonoBehaviourExtended {

	// Dependencies
	private Camera _playerCamera;

	// Ray
	private Vector3 _lookingDirection = Vector3.zero;
	private RaycastHit _hit;
	public float RayMaxLength;

	// Target script
	public GameObject CurrentlyTargetting { get; private set; }
	private GameObject _targetGameObject;
	private IInteractable _targetScript;


	protected override void Start() {
		base.Start();

		// Get references
		_playerCamera = Camera.main;
	}


	private void Update() {
		if (IsPaused) return;

		if (LookingAtInteractable()) {
			if(CurrentlyTargetting != _hit.transform.gameObject)
				_hit.transform.GetComponent<IInteractable>().OnLookAt();

			CurrentlyTargetting = _hit.transform.gameObject;

			if (Input.GetKeyDown( KeyCode.E )) {
				// Get script
				// Only, when a new object is selected
				if (_hit.transform.gameObject != _targetGameObject) {
					_targetGameObject = _hit.transform.gameObject;
					_targetScript = _targetGameObject.GetComponent<IInteractable>();
				}

				if(_targetScript != null)
					_targetScript.Interact( DeltaState.Enter, _hit );
				else
					Debug.LogError( "Object is tagged as " + gameObject.tag + " but does not contain a script of type " + typeof(IInteractable) + "!" );
			}

			if (Input.GetKey( KeyCode.E )) {
				if ( _targetScript != null )
					_targetScript.Interact( DeltaState.Stay, _hit );
			}

			if (Input.GetKeyUp( KeyCode.E )) {
				if ( _targetScript != null )
					_targetScript.Interact( DeltaState.Exit, _hit );
				else
					Debug.LogError( "Object is tagged as " + gameObject.tag + " but does not contain a script of type " + typeof( IInteractable ) + "!" );
			}

		} else {
			CurrentlyTargetting = null;
			_targetScript = null;
			_targetGameObject = null;
		}
	}


	bool LookingAtInteractable() {
		_lookingDirection = _playerCamera.transform.forward;
		if (Physics.Raycast( _playerCamera.transform.position, _lookingDirection, out _hit, RayMaxLength ))
			if (_hit.transform.tag == "Interactable")
				return true;

		return false;
	}

}