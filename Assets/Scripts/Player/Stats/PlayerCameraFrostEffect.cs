﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class PlayerCameraFrostEffect : MonoBehaviourExtended {

	private static PlayerCameraFrostEffect _instance;
	public static PlayerCameraFrostEffect Instance {
		get {
			if (_instance == null)
				_instance = FindObjectOfType<PlayerCameraFrostEffect>();
			return _instance;
		}
	}

	// Dependencies
	private FrostEffect _camEffect;

	// Values
	public float NormalIntensity;
	public float HypothermiaIntensity;
	public float UnconsciousIntensity;
	public float DeathIntensity;	// \m/

	protected override void Start() {
 		base.Start();

		// Get references
		_camEffect = GetComponent<FrostEffect>();

		// Subscribe
		PlayerStats.OnBodyTemperatureChange += OnTempChange;

		OnTempChange();
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unsubscribe
		PlayerStats.OnBodyTemperatureChange -= OnTempChange;
	}


	private void OnTempChange() {
		StopCoroutine(FadeFrostEffect(0));

		if(PlayerStats.Instance.TemperatureState == TemperatureStates.Normal)
			StartCoroutine(FadeFrostEffect(NormalIntensity));
		else if(PlayerStats.Instance.TemperatureState == TemperatureStates.Hypothermia)
			StartCoroutine(FadeFrostEffect(HypothermiaIntensity));
		else if (PlayerStats.Instance.TemperatureState == TemperatureStates.Unconscious)
			StartCoroutine(FadeFrostEffect(UnconsciousIntensity));
		else if ( PlayerStats.Instance.TemperatureState == TemperatureStates.Dead )
			StartCoroutine( FadeFrostEffect( DeathIntensity ) );
	}

	private IEnumerator FadeFrostEffect(float target) {
		while (Mathf.Abs(_camEffect.FrostAmount - target) >= 0.001) {
			_camEffect.FrostAmount = Mathf.Lerp(_camEffect.FrostAmount, target, 0.001f);
			yield return null;
		}
	}
}