﻿using UnityEngine;

public class SuicideAnimationEvents : MonoBehaviourExtended {

	[Header("Spurt objects")]
	[SerializeField] private GameObject[] _bloodSpurts;
	private Animator[] _bloodSpurtAnimators;

	[Header( "Wrist" )]
	[SerializeField] private GameObject _wrist;
	private Animator _wristAnimator;

	[Header("Animation timers")]
	[SerializeField] private float _spurtTotalTime;


	protected override void Start() {
		base.Start();

		_bloodSpurtAnimators = new Animator[_bloodSpurts.Length];
		for (int b = 0; b < _bloodSpurts.Length; b++ ) {
			_bloodSpurtAnimators[b] = _bloodSpurts[b].GetComponent<Animator>();
			_bloodSpurts[b].SetActive( false );
		}

		_wristAnimator = _wrist.GetComponent<Animator>();
		_wrist.SetActive( false );
	}


	public void DoWristAnimation() {
		_wrist.SetActive( true );
		_wristAnimator.SetTrigger( "Slit" );
	}


	public void DoBloodAnimation() {
		for (int i = 0; i < _bloodSpurts.Length; i++) {
			_bloodSpurts[i].SetActive( true );
			_bloodSpurtAnimators[i].SetTrigger( "Spurt" );
		}

		foreach ( Animator animator in _bloodSpurtAnimators )
			animator.SetTrigger( "Spurt" );
	}


	public void EndOfAnimation() {
		PlayerStats.Instance.Die( DeathCause.Insanity );
	}
}