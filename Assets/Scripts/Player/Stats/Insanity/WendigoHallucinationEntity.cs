﻿using UnityEngine;

public class WendigoHallucinationEntity : MonoBehaviourExtended {

	[Header( "Vector stuff" )]
	[SerializeField] private float _maxRange;
	[SerializeField] private float _minRange;
	[SerializeField] private float _approachRange;
	private float _currentRange;
	[SerializeField] private float _maxHeightCheckLength;
	[SerializeField] private float _rayStepDistance;
	[SerializeField] LayerMask _groundLayers;

	private Renderer _renderer;

	[Header( "Animation" )]
	[SerializeField] private float _maxAlpha;
	[SerializeField] private float _fadeTime;
	[SerializeField] private float _approachTime;
	private float _currentFadeTimeLeft;
	private float _currentFadeTimeRight;
	[SerializeField] private float _targetStayTime;

	[SerializeField] [Range( 0f, 1f )] private float _currentAlpha;
	private float _targetAlpha {
		get {
			if (_targetAlphaTimer <= 0)
				return 0f;

			return _targetAlphaTarget;
		}
	}
	private float _targetAlphaTarget;
	private float _targetAlphaTimer;

	public static bool ApproachesPlayer;


	protected override void Start() {
		base.Start();

		// Get references
		_renderer = GetComponent<Renderer>();

		_currentRange = _maxRange;
	}

	private void Update() {
		if ( IsPaused ) return;

		if (Input.GetKeyDown( KeyCode.Q )) ApproachesPlayer = true;

		// Detect movement
		if (PlayerStats.Instance.CurrentSanity <= 0.15f || ApproachesPlayer) {
			if (ApproachesPlayer)
				_currentRange = Mathf.Lerp( _currentRange, _approachRange, Time.deltaTime * _approachTime );
			else
                _currentRange = _maxRange;

			Ray screenEdgeRay = Camera.main.ViewportPointToRay( new Vector3( 0.5f, 0.5f ) );
			screenEdgeRay.direction = new Vector3( screenEdgeRay.direction.x, 0f, screenEdgeRay.direction.z );

			Vector3 groundPoint = Vector3.zero;
			for (float l = _currentRange; l > 0; l -= _rayStepDistance) {
				Vector3 maxLengthPoint = screenEdgeRay.origin + screenEdgeRay.direction * l;

				// Look for ground, either up or down
				RaycastHit hit;
				if (Physics.Linecast( maxLengthPoint, maxLengthPoint + Vector3.down * _maxHeightCheckLength, out hit, _groundLayers )
					|| Physics.Linecast( maxLengthPoint, maxLengthPoint + Vector3.up * _maxHeightCheckLength, out hit, _groundLayers )) {
					groundPoint = hit.point;
					break;
				}
			}

			if (groundPoint == Vector3.zero) {
				_targetAlphaTarget = 0f;
				return;
			}

			Vector3 newPos = groundPoint;
			newPos.y += transform.localScale.z * 3f;
			transform.position = newPos;

			// Get alpha or smth
			if (!ApproachesPlayer) {
				float distanceToWendigo = (groundPoint - Camera.main.transform.position).magnitude;
				_targetAlphaTarget = _maxAlpha * MathfExtension.RingPercentage( distanceToWendigo, _minRange, _maxRange );

			} else {
				_targetAlphaTarget = _maxAlpha;
			}

		} else {
			_targetAlphaTarget = 0f;
		}

		// Target timer values
		if (_targetAlphaTarget <= 0f)
			_targetAlphaTimer -= Time.deltaTime;
		else
			_targetAlphaTimer = _targetStayTime;

		// Fade
		_currentAlpha = DoTimeFade( _targetAlpha, _currentAlpha, ref _currentFadeTimeLeft );
		_renderer.material.SetColor( "_TintColor", new Color( 0.5f, 0.5f, 0.5f, _currentAlpha ) );
	}


	private float DoTimeFade( float targetAlpha, float currentAlpha, ref float currentFadeTime ) {
		int sign = 0;
		if (targetAlpha > currentAlpha)
			sign = 1;
		else
			sign = -1;

		currentFadeTime = Mathf.Clamp( currentFadeTime + Time.deltaTime * sign, 0f, 1f );
		currentAlpha = currentFadeTime / _fadeTime;

		return currentAlpha;
	}

}