﻿using UnityEngine;

public class HallucinationManager : MonoBehaviourExtended {

	[Header( "Vector stuff" )]
	[SerializeField] private float _maxRange;
	[SerializeField] private float _maxHeightCheckLength;
	[SerializeField] private float _areaTestRadius;
	[SerializeField] private float _rayStepDistance;
	[SerializeField] LayerMask _groundLayers;

	[Header( "Hallucinations" )]
	[SerializeField] private GameObject _hallucinationObjectLeft;
	[SerializeField] private GameObject _hallucinationObjectRight;
	private Renderer _objRendererLeft;
	private Renderer _objRendererRight;

	[Header( "Animation" )]
	[SerializeField] private float _fadeTime;
	private float _currentFadeTimeLeft;
	private float _currentFadeTimeRight;
	[SerializeField] private float _targetStayTime;

	// Left
	[SerializeField] [Range(0f, 1f)] private float _currentAlphaLeft;
	private float _targetAlphaLeft {
		get {
			if (_targetAlphaLeftTimer <= 0) return 0f;
			else return 1f;
		}
	}
	private float _targetAlphaLeftTarget;
	private float _targetAlphaLeftTimer;

	// Right
	[SerializeField] [Range( 0f, 1f )] private float _currentAlphaRight;
	private float _targetAlphaRight {
		get {
			if (_targetAlphaRightTimer <= 0) return 0f;
			else return 1f;
		}
	}
	private float _targetAlphaRightTarget;
	private float _targetAlphaRightTimer;


	protected override void Start() {
		base.Start();

		// Get references
		_objRendererLeft = _hallucinationObjectLeft.GetComponent<Renderer>();
		_objRendererRight = _hallucinationObjectRight.GetComponent<Renderer>();
	}		   
			   
			   
	private void Update() {
		// Detect movement
		Direction dir = PlayerController.Instance.CameraTurnDirection;
		if (dir != Direction.None && PlayerStats.Instance.CurrentSanity <= 0.25f){
			Ray screenEdgeRay = GetMovingScreenEdge( dir );

			Vector3 groundPoint = Vector3.zero;
			for (float l = _maxRange; l > 0; l -= _rayStepDistance) {
				Vector3 maxLengthPoint = screenEdgeRay.origin + screenEdgeRay.direction * l;

				// Look for ground, either up or down
				RaycastHit hit;
				if (Physics.Linecast( maxLengthPoint, maxLengthPoint + Vector3.down * _maxHeightCheckLength, out hit, _groundLayers )
					|| Physics.Linecast( maxLengthPoint, maxLengthPoint + Vector3.up * _maxHeightCheckLength, out hit, _groundLayers )) {
					groundPoint = hit.point;
					break;
				}
			}

			if (groundPoint == Vector3.zero) {
				if (dir == Direction.Left)
					_targetAlphaLeftTarget = 0f;
				else if (dir == Direction.Right)
					_targetAlphaRightTarget = 0f;
				return;
			}

			if (dir == Direction.Left) {
				Vector3 newPos = groundPoint;
				newPos.y += _hallucinationObjectRight.transform.localScale.z * 4f;
				_hallucinationObjectRight.transform.position = newPos;
				_targetAlphaRightTarget = 1f;
			} else if (dir == Direction.Right) {
				Vector3 newPos = groundPoint;
				newPos.y += _hallucinationObjectLeft.transform.localScale.z * 4f;
				_hallucinationObjectLeft.transform.position = newPos;
				_targetAlphaLeftTarget = 1f;
			}
		} else {
			_targetAlphaLeftTarget = 0f;
			_targetAlphaRightTarget = 0f;
		}

		// Target timer values
		if (_targetAlphaLeftTarget <= 0f) _targetAlphaLeftTimer -= Time.deltaTime;
		else _targetAlphaLeftTimer = _targetStayTime;

		if (_targetAlphaRightTarget <= 0f) _targetAlphaRightTimer -= Time.deltaTime;
		else _targetAlphaRightTimer = _targetStayTime;

		// Fade
		_currentAlphaLeft = DoTimeFade( _targetAlphaLeft, _currentAlphaLeft, ref _currentFadeTimeLeft );
		_currentAlphaRight = DoTimeFade( _targetAlphaRight, _currentAlphaRight, ref _currentFadeTimeRight );

		_hallucinationObjectLeft.SetActive( _currentAlphaLeft > 0.01f );
		_hallucinationObjectRight.SetActive( _currentAlphaRight > 0.01f );

		_objRendererLeft.material.SetColor( "_TintColor", new Color( 1f, 1f, 1f, _currentAlphaLeft ) );
		_objRendererRight.material.SetColor( "_TintColor", new Color( 1f, 1f, 1f, _currentAlphaRight ) );
	}		   
			   
			   
	private Ray GetMovingScreenEdge(Direction direction) {
		float point = 0f;
		if (direction == Direction.Left)
			point = 1;
		else if (direction == Direction.Right)
			point = 0;

		Ray newEdgeRay = Camera.main.ViewportPointToRay( new Vector3( point, 0.5f ) );
		newEdgeRay.direction = new Vector3( newEdgeRay.direction.x, 0f, newEdgeRay.direction.z );
		return newEdgeRay;
	}

	private float DoTimeFade(float targetAlpha, float currentAlpha, ref float currentFadeTime) {
		int sign = 0;
		if (targetAlpha > currentAlpha) sign = 1;
		else sign = -1;

		currentFadeTime = Mathf.Clamp(currentFadeTime + Time.deltaTime * sign, 0f, 1f);
		currentAlpha = currentFadeTime / _fadeTime;

		return currentAlpha;
	}

}