﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class PlayerCameraInsanity : MonoBehaviourExtended {

	private static PlayerCameraInsanity _instance;
	public static PlayerCameraInsanity Insanity {
		get {
			if (_instance == null)
				_instance = FindObjectOfType<PlayerCameraInsanity>();
			return _instance;
		}
	}

	// Cam effects
	private VignetteAndChromaticAberration _vignetteEffect;
	private BlurOptimized _blurEffect;

	// Pulse
	[Header( "Pulsating" )]
	public float DefaultFoV;
	public float InsaneFoV;
	private float CurrentFoV;
	public float PulseStrength;
	public float PulseStrengthMultiplier;
	private float _pulseBase;
	private float _currentPulseFlicker;
	private PingPongValue _currentPulseFlickerTime = new PingPongValue( 0f, 100f );
	public float PulseFlickerSpeed;

	// Vignette details
	[Header("Vignette")]
	public float VignetteStrength;
	public float VignetteStrengthMultiplier;
	private float _vignetteBase;
	private float _currentVignetteFlicker;
	private PingPongValue _currentVignetteFlickerTime = new PingPongValue( 0f, 100f);
	public float VignetteFlickerSpeed;

	// Aberration details
	[Header("Aberration")]
	public float AberrationStrength;
	public float AberrationStrengthMultiplier;
	private float _aberrationBase;
	private float _currentAberrationFlicker;
	private PingPongValue _currentAberrationFlickerTime = new PingPongValue( 0f, 100f );
	public float AberrationFlickerSpeed;

	// Blur
	[Header("Blur")]
	public float BlurStrength;
	public float BlurStrengthMultiplier;
	private float _blurBase = 1f;


	protected override void Start() {
		base.Start();

		// Get references
		_vignetteEffect = GetComponent<VignetteAndChromaticAberration>();
		_blurEffect = GetComponent<BlurOptimized>();

		CurrentFoV = DefaultFoV;
	}

	private void Update() {
		if (!IsPaused) {
			Pulse();
			VignetteAndAberration();
			//BlurScreen();
		}
	}


	private void Pulse() {
		if ( PlayerStats.Instance.CurrentSanity <= 0.75f ) {
			PulseStrengthMultiplier = 1f;
		} else {
			PulseStrengthMultiplier = 0f;
		}

		if (PlayerStats.Instance.CurrentSanity <= 0.25f)
			CurrentFoV = Mathf.Lerp( CurrentFoV, InsaneFoV, Time.deltaTime * 0.25f );
		else
			CurrentFoV = Mathf.Lerp( CurrentFoV, DefaultFoV, Time.deltaTime * 0.25f );

		// Lerp base strength
		_pulseBase = Mathf.Lerp( _pulseBase, PulseStrengthMultiplier, Time.deltaTime );

		// Flicker
		_currentPulseFlicker = Mathf.PerlinNoise( _currentPulseFlickerTime.Next( Time.deltaTime * PulseFlickerSpeed ), 0 );

		// Apply effects
		Camera.main.fieldOfView = CurrentFoV - _pulseBase * _currentPulseFlicker * PulseStrength;
	}


	private void VignetteAndAberration() {
		if (PlayerStats.Instance.CurrentSanity <= .5f) {
			AberrationStrengthMultiplier = 1f;
			VignetteStrengthMultiplier = 1f;
		} else {
			AberrationStrengthMultiplier = 0f;
			VignetteStrengthMultiplier = 0f;
		}

		// Lerp base strength
		_vignetteBase = Mathf.Lerp(_vignetteBase, VignetteStrengthMultiplier, Time.deltaTime);
		_aberrationBase = Mathf.Lerp(_aberrationBase, AberrationStrengthMultiplier, Time.deltaTime);

		// Flicker vignette
		_currentVignetteFlicker = Mathf.PerlinNoise(_currentVignetteFlickerTime.Next( Time.deltaTime * VignetteFlickerSpeed ), 0 );
		_currentAberrationFlicker = Mathf.PerlinNoise(_currentAberrationFlickerTime.Next( Time.deltaTime * AberrationFlickerSpeed ), 5f);

		// Apply effects
		_vignetteEffect.intensity = _vignetteBase * _currentVignetteFlicker * VignetteStrength;
		_vignetteEffect.chromaticAberration = _aberrationBase * _currentAberrationFlicker * AberrationStrength;
	}


	private void BlurScreen() {
		if (PlayerStats.Instance.CurrentSanity <= .25f) {
			BlurStrengthMultiplier = 1f;
		} else {
			BlurStrengthMultiplier = 0f;
		}

		// Lerp base strength
		_blurBase = Mathf.Lerp(_blurBase, BlurStrengthMultiplier, Time.deltaTime);

		// Apply effects
		_blurEffect.blurSize = _blurBase * BlurStrength;
	}
}