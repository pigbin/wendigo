﻿using UnityEngine;

[RequireComponent( typeof( SphereCollider ) )]
public class SanityImpedimentZone : MonoBehaviourExtended {

	// Dependencies
	private SphereCollider _collider;
	private Transform _player;

	// Constants
	public const float ValueMinimum = -1f;
	public const float ValueMaximum = 0f;
	public static readonly Color MinColour = new Color( 255f / 255f, 77f / 255f, 77f / 255f, 1f );
	public static readonly Color MaxColour = new Color( 255f / 255f, 223f / 255f, 13f / 255f, 1f );

	// Temperature
	[Range( -1f, 0f )]
	public float Impediment;

	// Range
	public float ZoneRange = 15f;
	public float MinimumZoneRange = 10f;


	protected override void Start() {
		base.Start();

		// Get references
		_collider = GetComponent<SphereCollider>();
		_player = GameObject.FindGameObjectWithTag( "Player" ).transform;

		// Apply stats to collider
		_collider.isTrigger = true;
		_collider.radius = ZoneRange / 2f;
	}


	private void OnTriggerEnter( Collider other ) {
		if ( other.gameObject.tag != "Player" ) return;

		EnableZone();
	}

	private void OnTriggerExit( Collider other ) {
		if ( other.gameObject.tag != "Player" ) return;

		DisableZone();
	}


	protected void OnEnable() {
		DisableZone();
	}

	protected void OnDisable() {
		DisableZone();
	}


	protected override void OnDestroy() {
		base.OnDestroy();

		DisableZone();
	}


	public float GetStrength() {
		Vector3 dir = _player.position - transform.position;

		if ( dir.magnitude <= MinimumZoneRange / 2f )
			return 1f;

		if ( dir.magnitude <= ZoneRange / 2f )
			return 1 - (dir.magnitude - MinimumZoneRange / 2f) / ((ZoneRange - MinimumZoneRange) / 2f);

		return 0;
	}


	private void EnableZone() {
		PlayerStats.Instance.SanityImpedimentZones.Add( this );
		PlayerStats.Instance.UpdateSanity();
	}

	private void DisableZone() {
		PlayerStats.Instance.SanityImpedimentZones.Remove( this );
		PlayerStats.Instance.UpdateSanity();
	}

}