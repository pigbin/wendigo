﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerStats : MonoBehaviourExtended {

	// Singleton
	private static PlayerStats _instance;
	public static PlayerStats Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<PlayerStats>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}

	// Walking Speed
	public Dictionary<string, float> SpeedMultipliers = new Dictionary<string, float>();

	public delegate void StatChange( float val );

	// Hunger
	[Header("Hunger")]
	[SerializeField] [Range( 0f, 1f )] private float _currentHunger;
	public float CurrentHunger {
		get { return _currentHunger; }
		private set {
			if (!SceneData.Instance.StatsArePaused) {
				// Check if the state changes
				// How: Check if the value is between the state min and max value
				// ... and if the value was in that range before.
				// If so: The state has changed!
				if (OnHungerLevelChange != null && CurrentHunger != value) {
					if (MathfExtension.IsBetween( value, 0f, 0.25f, CompareFlags.IncludeMin ) 
						&& !MathfExtension.IsBetween( CurrentHunger, 0f, 0.25f, CompareFlags.IncludeMin ))
						OnHungerLevelChange( value );
					else if ( MathfExtension.IsBetween( value, 0.25f, 0.5f, CompareFlags.IncludeMin )
						&& !MathfExtension.IsBetween( CurrentHunger, 0.25f, 0.5f, CompareFlags.IncludeMin ) )
						OnHungerLevelChange( value );
					else if ( MathfExtension.IsBetween( value, 0.5f, 0.75f, CompareFlags.IncludeMin ) 
						&& !MathfExtension.IsBetween( CurrentHunger, 0.5f, 0.75f, CompareFlags.IncludeMin ) )
						OnHungerLevelChange( value );
					else if ( MathfExtension.IsBetween( value, 0.75f, 1f )
						&& !MathfExtension.IsBetween( CurrentHunger, 0.75f, 1f ) )
						OnHungerLevelChange( value );
				}

				_currentHunger = value;
				UpdateSanity();
			}
		}
	}
	public float HungerPerTick;
	public float HungerPerTickVeryHungry;
	public static event StatChange OnHungerLevelChange;

	// Thirst
	[Header( "Thirst" )]
	[SerializeField] [Range( 0f, 1f )] private float _currentThirst;
	public float CurrentThirst {
		get { return _currentThirst; }
		private set {
			if (!SceneData.Instance.StatsArePaused) {
				// Check if the state changes
				// How: Check if the value is between the state min and max value
				// ... and if the value was in that range before.
				// If so: The state has changed!
				if ( OnThirstLevelChange != null && CurrentThirst != value ) {
					if ( MathfExtension.IsBetween( value, 0f, 0.25f, CompareFlags.IncludeMin )
						&& !MathfExtension.IsBetween( CurrentThirst, 0f, 0.25f, CompareFlags.IncludeMin ) )
						OnThirstLevelChange( value );
					else if ( MathfExtension.IsBetween( value, 0.25f, 0.5f, CompareFlags.IncludeMin )
						&& !MathfExtension.IsBetween( CurrentThirst, 0.25f, 0.5f, CompareFlags.IncludeMin ) )
						OnThirstLevelChange( value );
					else if ( MathfExtension.IsBetween( value, 0.5f, 0.75f, CompareFlags.IncludeMin )
						&& !MathfExtension.IsBetween( CurrentThirst, 0.5f, 0.75f, CompareFlags.IncludeMin ) )
						OnThirstLevelChange( value );
					else if ( MathfExtension.IsBetween( value, 0.75f, 1f, CompareFlags.IncludeMin | CompareFlags.IncludeMax )
						&& !MathfExtension.IsBetween( CurrentThirst, 0.75f, 1f, CompareFlags.IncludeMin | CompareFlags.IncludeMax ) )
						OnThirstLevelChange( value );
				}

				_currentThirst = value;
				UpdateSanity();
			}
		}
	}
	public float ThirstPerTick;
	[SerializeField] private float ThirstMovementSpeedReduction;
	public static event StatChange OnThirstLevelChange;

	// Sanity
	[Header( "Sanity" )]
	[SerializeField] [Range( 0f, 1f )] public float CurrentSanity = 1f;

	public List<KeyValuePair<float, float>> SanityImpediments = new List<KeyValuePair<float, float>>();
	public List<SanityImpedimentZone> SanityImpedimentZones = new List<SanityImpedimentZone>();


	// Temperature
	[Header( "Temperature" )]
	[Range( -40f, -20f )] public float CurrentAirTemperature;
	[SerializeField] [Range( 20f, 37f )] private float _currentTemperature = 37f;
	public float CurrentTemperature {
		get { return _currentTemperature; }
		set {
			if (!SceneData.Instance.StatsArePaused || true) {
				_currentTemperature = value;
				UpdateSanity();

				OnTemperatureChange();

				if ( OnBodyTemperatureChange != null )
					OnBodyTemperatureChange();
			}
		}
	}
	[SerializeField] private float _temperatureLossPerMinute;
	[SerializeField] private TemperatureStates _temperatureState;
	public TemperatureStates TemperatureState {
		get { return _temperatureState; }
		set {
			if (_temperatureState != value && OnBodyTemperatureStateChange != null) OnBodyTemperatureStateChange( value );
			_temperatureState = value;
		}
	}

	public float TemperatureTickTime;
	private float _currentLossTime;
	public List<TemperatureZone> TemperatureZones = new List<TemperatureZone>();
	public float HypothermiaTemperature;
	public float UnconsciousTemperature;
	public float DeathTempereature;
	public delegate void TempChange();
	public static event TempChange OnBodyTemperatureChange;
	public delegate void TempStateChange(TemperatureStates state);
	public static event TempStateChange OnBodyTemperatureStateChange;

	// Death
	[Header("Death")]
	public bool IsDead;
	public delegate void Death(DeathCause cause);
	public static event Death OnPlayerDeath;

	// Suicide
	[Header( "SEWE-WHEE-SIDE" )]
	private Animator _animator;
	[SerializeField] private AudioMixer _audioMixer;
	[SerializeField] private float _soundFadeOutTime;


	void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}

	protected override void Start() {
		base.Start();

		// Subscribe
		WorldTime.Instance.OnHourTick += HungerTick;
		WorldTime.Instance.OnHourTick += ThirstTick;

		_animator = Camera.main.transform.parent.GetComponent<Animator>();
	}

	private void OnLevelWasLoaded( int id ) {
		_animator = Camera.main.transform.parent.GetComponent<Animator>();
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unsubscribe
		WorldTime.Instance.OnHourTick -= HungerTick;
		WorldTime.Instance.OnHourTick -= ThirstTick;
	}


	private void Update() {
		if (!IsPaused && !SceneData.Instance.StatsArePaused) {
			// Temperature
			CurrentAirTemperature = GetAirTemperature();
			_temperatureLossPerMinute = -0.1415f * CurrentAirTemperature + 1.42f;
			CalculateBodyTemperature();

			// Sanity impediment timers
			for (int i = 0; i < SanityImpediments.Count; i++) {
				float time = SanityImpediments[i].Value - Time.deltaTime;
				if (time <= 0f) {
					SanityImpediments.RemoveAt( i );
					UpdateSanity();
				} else {
					SanityImpediments[i] = new KeyValuePair<float, float>( SanityImpediments[i].Key, time );
				}
			}

			// Sanity impediment zones
			if(SanityImpedimentZones.Count > 0) UpdateSanity();
		}

		if (Input.GetKeyDown( KeyCode.U )) {
			Debug.Log( "LOL" );
			CurrentSanity = 0f;
			StartCoroutine( CommitSuicide() );
		}
	}


	protected override void OnPauseEnter( bool openPauseMenu ) {
		if(_animator != null)
			_animator.speed = 0f;
	}

	protected override void OnPauseExit() {
		if (_animator != null)
			_animator.speed = 1f;
	}


	// DID I EVER TELL YOU THE DEFINITION OF SANITY?
	// ==================================================
	[ContextMenu("Update Sanity")]
	public void UpdateSanity() {
		if (SceneData.Instance.StatsArePaused)
			return;
		
		var newSanity = 1f;

		// Hunger
		if (CurrentHunger >= 0.75f)
			newSanity -= 0.25f;
		else if(CurrentHunger >= 0.5f)
			newSanity += 0.5f;

		// Thirst
		if (CurrentThirst >= 0.75f)
			newSanity -= 0.25f;

		// Temperature
		if (CurrentTemperature <= HypothermiaTemperature)
			newSanity -= 0.25f;

		// Impediments
		foreach (KeyValuePair<float, float> impediment in SanityImpediments)
			newSanity += impediment.Key;

		// Zones
		foreach (SanityImpedimentZone zone in SanityImpedimentZones)
			newSanity += (zone.Impediment * zone.GetStrength());

		// THE CLAMPS!
		CurrentSanity = Mathf.Clamp( newSanity, 0f, 1f );

		if (CurrentSanity <= 0) {
			StartCoroutine( CommitSuicide() );
		}
	}

	public void AlterSanity( float impediment, float time ) {
		SanityImpediments.Add( new KeyValuePair<float, float>(impediment, time) );
		UpdateSanity();
	}


	// NEEDS
	// ==================================================
	private void HungerTick() {
		float newHunger = CurrentHunger;

		if (CurrentHunger < 0.5f)
			newHunger += HungerPerTick;
		else
			newHunger += HungerPerTickVeryHungry;

		CurrentHunger = Mathf.Clamp( newHunger, 0f, 1f );

		if(CurrentHunger >= 1f) Die( DeathCause.Hunger );
	}

	private void ThirstTick() {
		float newThirst = CurrentThirst;
		newThirst += ThirstPerTick;
		CurrentThirst = Mathf.Clamp( newThirst, 0f, 1f );

		// Movement effects
		if ( !SpeedMultipliers.ContainsKey( "Thirst" ) )
			SpeedMultipliers.Add( "Thirst", 1f );
		if (CurrentThirst >= .25f)
			SpeedMultipliers["Thirst"] = ThirstMovementSpeedReduction;
		else
			SpeedMultipliers["Thirst"] = 1f;

		// Death
		if ( CurrentThirst >= 1f ) Die( DeathCause.Thirst );
	}

	public void AlterHunger( float value ) {
		CurrentHunger = Mathf.Clamp( CurrentHunger += value, 0f, 1f );
	}

	public void AlterThirst( float value ) {
		CurrentThirst = Mathf.Clamp( CurrentThirst += value, 0f, 1f );
	}


	// TEMPERATURE
	// ==================================================
	private float GetAirTemperature() {
		float weatherTemp = WorldWeather.Instance.CurrentWeather.TimedAmbientTemperature;
		float totalTemp = weatherTemp;

		List<TemperatureZone> sortedTemperatureZoneses = TemperatureZones;
		foreach (TemperatureZone zone in sortedTemperatureZoneses.OrderByDescending( z => z.GetStrength() )) {
			totalTemp = Mathf.Lerp( totalTemp, zone.Temperature, zone.GetStrength() );

			if (Math.Abs( zone.GetStrength() - 1f ) < 0.001f) break;
		}

		return totalTemp;
	}

	private void CalculateBodyTemperature() {
		if ( SceneData.Instance.TimeIsPaused )
			return;

		_currentLossTime += Time.deltaTime;

		if (_currentLossTime >= TemperatureTickTime) {
			_currentLossTime = 0f;

			float _newLossPerMinute = _temperatureLossPerMinute;

			foreach (EquipmentPiece piece in Inventory.Instance.Equipment)
				_newLossPerMinute *= piece.TemperatureMultiplier;

			CurrentTemperature -= _newLossPerMinute;
			//OnTemperatureChange();

			//if (OnBodyTemperatureChange != null)
			//	OnBodyTemperatureChange();
		}
	}

	private void OnTemperatureChange() {
		if (!SpeedMultipliers.ContainsKey("Temperature"))
			SpeedMultipliers.Add("Temperature", 1f);

		if (CurrentTemperature <= DeathTempereature) {
			TemperatureState = TemperatureStates.Dead;
			SpeedMultipliers["Temperature"] = 0f;
			Die( DeathCause.Temperature );

		} else if (CurrentTemperature <= UnconsciousTemperature) {
			TemperatureState = TemperatureStates.Unconscious;
			SpeedMultipliers["Temperature"] = 0.25f;

		} else if (CurrentTemperature <= HypothermiaTemperature) {
			TemperatureState = TemperatureStates.Hypothermia;
			SpeedMultipliers["Temperature"] = 0.5f;

		} else {
			TemperatureState = TemperatureStates.Normal;
			SpeedMultipliers["Temperature"] = 1f;
		}
	}

	public void AlterTemperature( float value ) {
		CurrentTemperature = Mathf.Clamp( CurrentTemperature += value, 20f, 37f );
	}


	// DETH
	// ==================================================
	public void Die( DeathCause cause ) {
		IsDead = true;
		WendigoHallucinationEntity.ApproachesPlayer = false;

		GamePauseManager.Instance.Pause( false );
		if (OnPlayerDeath != null) OnPlayerDeath(cause);
	}

	private IEnumerator CommitSuicide() {
		// Turn down sound
		float currentSoundFadeOuttime = _soundFadeOutTime;
		while (currentSoundFadeOuttime > 0) {
			currentSoundFadeOuttime -= Time.deltaTime;
			_audioMixer.SetFloat( "SuicideVolume", -80 * (1 - currentSoundFadeOuttime / _soundFadeOutTime) );
			yield return null;
		}

		// Have wendigo come closer
		WendigoHallucinationEntity.ApproachesPlayer = true;

		yield return new WaitForSeconds( 3f );

		// Block player input and animate camera
		PlayerController.InputBlockers++;
		BlockInterface( true );
		if (_animator != null)
			_animator.SetTrigger( "Suicide" );

		yield return null;
	}


	// RESETTI
	// ==================================================
	private void Reset() {
		IsDead = false;

		CurrentHunger = 0f;
		CurrentThirst = 0f;

		CurrentSanity = 0f;

		CurrentTemperature = 37f;
	}

}

public enum TemperatureStates {

	Normal,
	Hypothermia,
	Unconscious,
	Dead

}

public enum DeathCause {

	Temperature,
	Hunger,
	Thirst,
	Insanity

}