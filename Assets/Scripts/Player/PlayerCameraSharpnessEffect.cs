﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class PlayerCameraSharpnessEffect : MonoBehaviourExtended {

	[Header("Component")]
	[SerializeField] private ContrastEnhance _contrastEffect;

	// Sharpness details
	[Header( "Effect" )]
	public float SharpnessStrength;
	public float SharpnessStrengthMultiplier;
	private float _SharpnessBase;
	private float _currentSharpnessFlicker;
	private PingPongValue _currentSharpnessFlickerTime = new PingPongValue( 0f, 100f );
	public float SharpnessFlickerSpeed;


	protected override void Start() {
		base.Start();

		// Get references
		_contrastEffect = GetComponent<ContrastEnhance>();
	}


	private void Update() {
		if (IsPaused) return;

		if ( PlayerStats.Instance.CurrentThirst >= 0.5f )
			SharpnessStrengthMultiplier = 1f;
		else
			SharpnessStrengthMultiplier = 0f;


		// Lerp base strength
		_SharpnessBase = Mathf.Lerp( _SharpnessBase, SharpnessStrengthMultiplier, Time.deltaTime );

		// Flicker vignette
		_currentSharpnessFlicker = Mathf.PerlinNoise( _currentSharpnessFlickerTime.Next( Time.deltaTime * SharpnessFlickerSpeed ), 5f );

		// Apply effects
		_contrastEffect.intensity = _SharpnessBase * _currentSharpnessFlicker * SharpnessStrength;
	}

}