﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class PlayerCameraGreyscaleEffect : MonoBehaviourExtended {

	// Effect scripts
	private ColorCorrectionCurves _cameraEffect;

	[Header( "Values" )]
	[SerializeField] private float _insaneSaturation;
	[SerializeField] private float _pausedSaturation;
	[SerializeField] private float _lerpTime;
	private float _currentSaturation;
	private float _targetSaturation;


	protected override void Start() {
		base.Start();

		// Get references
		_cameraEffect = GetComponent<ColorCorrectionCurves>();
	}


	protected override void OnPauseEnter( bool openPauseMenu ) {
		_currentSaturation = _pausedSaturation;
		_cameraEffect.saturation = _pausedSaturation;
	}

	
	private void Update() {
		if (IsPaused) return;

		if (PlayerStats.Instance.CurrentSanity <= 0.5f)
			_targetSaturation = _insaneSaturation;
		else
			_targetSaturation = 1f;

		_currentSaturation = Mathf.Lerp( _currentSaturation, _targetSaturation, Time.deltaTime * _lerpTime );
		_cameraEffect.saturation = _currentSaturation;
	}

}