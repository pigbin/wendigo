﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerController : MonoBehaviourExtended {

	// Singleton
	private static PlayerController _instance;
	public static PlayerController Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<PlayerController>();
			}
			return _instance;
		}
	}

	// Dependencies
	private Camera _playerCamera;
	private Camera _skyCamera;
	private NavMeshAgent _navAgent;

	// Character properties
	private float _headHeight;

	// Movement
	[Header( "Movement and speed" )] public float BaseMovementSpeed;
	public float BackpedallingMultiplier;
	public float RunningSpeedMultiplier;
	//public Dictionary<string, float> OtherSpeedMultipliers = new Dictionary<string, float>();
	
	[SerializeField] private float _finalMovementSpeed;
	private bool _isSprinting;
	public Vector3 Movement;
	private Vector3 _movementPrevious;

	public bool IsMoving {
		get { return Movement.magnitude > 0; }
	}

	private bool IsMovingPrevious {
		get { return _movementPrevious.magnitude > 0; }
	}

	private DeltaState IsMovingDelta {
		get {
			if(!IsMovingPrevious && IsMoving) return DeltaState.Enter;
			if(IsMovingPrevious && IsMoving) return DeltaState.Stay;
			if(IsMovingPrevious && !IsMoving) return DeltaState.Exit;
			return DeltaState.None;
		}
	}

	// Walking
	[Header( "Steppin'" )]
	[SerializeField] private float _currentStepDistance;
	[SerializeField] private float _currentBobDistance;
	public float StepDistanceWalking;
	public float StepDistanceRunning;
	public float StepDistanceBackpedalling;
	private float _stepDistance;
	private int _foot = 1; //1 = right; -1 = left

	public delegate void Step(int currentFoot);
	public static event Step OnStep;

	public delegate void StopMovement();
	public static event StopMovement OnStopMoving;

	// Head bobbing
	[Header( "Bobbin'" )]
	public float HeadBobStrengthWalking;
	public float HeadBobStrengthRunning;
	private float _headBobStrength;
	public float HeadBobReversalTime;

	// Input
	[Header( "Input keys" )] public KeyCode[] MovementForward;
	public KeyCode[] MovementBackward;
	public KeyCode[] MovementStrafeLeft;
	public KeyCode[] MovementStrafeRight;
	[Space( 5f )] public KeyCode[] MovementSprint;
	public static int InputBlockers;
	private Direction _cameraTurnDirection;
	public Direction CameraTurnDirection {
		get { return _cameraTurnDirection; }
		set {
			//if (_cameraTurnDirection != value)
				if (OnCameraTurnDirectionChange != null)
					OnCameraTurnDirectionChange( value );

			_cameraTurnDirection = value;
		}
	}

	public delegate void DirectionChange( Direction newDirection );
	public static event DirectionChange OnCameraTurnDirectionChange;

	// Camera
	[Header( "Camera" )] private float _cameraYRotation;
	public float MaxCameraAngle;
	public float MinCameraAngle;


	protected override void Start() {
		base.Start();

		// Get references
		_playerCamera = Camera.main;
		_skyCamera = GameObject.FindGameObjectWithTag( "SkyCamera" ).GetComponent<Camera>();
		_navAgent = GetComponent<NavMeshAgent>();

		// Get default values
		_headHeight = _playerCamera.transform.localPosition.y;
		_stepDistance = StepDistanceWalking;
		_headBobStrength = HeadBobStrengthWalking;

		// Lock cursor on screen
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	private void Update() {
		if (!IsPaused && InputBlockers == 0) {
			// Update movement speed
			_finalMovementSpeed = BaseMovementSpeed;
			foreach (KeyValuePair<string, float> multiplier in PlayerStats.Instance.SpeedMultipliers)
				_finalMovementSpeed *= multiplier.Value;

			// Start sprinting if player presses shift
			if ( !_isSprinting && InputExtensions.KeyInArrayPressed( MovementSprint ) 
				&& PlayerStats.Instance.TemperatureState == TemperatureStates.Normal )
				_isSprinting = true;
			if ( _isSprinting )
				_finalMovementSpeed *= RunningSpeedMultiplier;
			
			// Stop sprinting if the player stops moving
			// Also stop sprinting if the player walks backwards (See MovePlayer)
			if ( !IsMoving 
				|| PlayerStats.Instance.TemperatureState != TemperatureStates.Normal)
				_isSprinting = false;

			MoveCamera();
			MovePlayer();
			CalculateStepDistance();
			BobHead();

			if (IsMovingDelta == DeltaState.Exit) {
				if (OnStep != null) OnStep( _foot );
				if (OnStopMoving != null) OnStopMoving();
			}
		}
	}

	protected override void OnPauseEnter(bool openPauseMenu ) {
		// Release cursor
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	protected override void OnPauseExit() {
		// Lock cursor
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}


	protected override void OnDestroy() {
		base.OnDestroy();

		// Release cursor
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}


	private void MovePlayer() {
		_movementPrevious = Movement;
		Movement = Vector3.zero;

		// Forward / Backward
		if (InputExtensions.KeyInArrayPressed( MovementForward ))
			Movement += transform.forward;
		else if (InputExtensions.KeyInArrayPressed( MovementBackward )) {
			Movement -= transform.forward;
			if (_isSprinting) {
				_finalMovementSpeed *= 1 / RunningSpeedMultiplier;
				_isSprinting = false;
			}
			_finalMovementSpeed *= BackpedallingMultiplier;
		}

		// Left / Right
		if (InputExtensions.KeyInArrayPressed( MovementStrafeLeft ))
			Movement -= transform.right;
		else if (InputExtensions.KeyInArrayPressed( MovementStrafeRight ))
			Movement += transform.right;

		// Normalize vector and apply movement speed
		// Otherwise the player would be faster by strafing and walking
		Movement.Normalize();
		Movement = Movement * _finalMovementSpeed * Time.deltaTime;

		_navAgent.Move( Movement );
	}

	private void MoveCamera() {
		var mouseMovement = new Vector2( Input.GetAxis( "Mouse X" ), Input.GetAxis( "Mouse Y" ) );

		// X: Rotate player
		transform.Rotate( new Vector3( 0f, mouseMovement.x, 0f ) * PlayerConfig.Instance.MouseLookingSensitivity.Value ); // Well that was easy!
		if (mouseMovement.x == 0)
			CameraTurnDirection = Direction.None;
		else if (mouseMovement.x > 0)
			CameraTurnDirection = Direction.Right;
		else if (mouseMovement.x < 0)
			CameraTurnDirection = Direction.Left;

		// Y: Rotate camera
		_cameraYRotation += mouseMovement.y * PlayerConfig.Instance.MouseLookingSensitivity.Value;
		_cameraYRotation = Mathf.Clamp( _cameraYRotation, MinCameraAngle, MaxCameraAngle );
		_playerCamera.transform.localEulerAngles = new Vector3( -_cameraYRotation, 0f, 0f );
		_skyCamera.transform.rotation = _playerCamera.transform.rotation;
	}


	private void CalculateStepDistance() {
		if (IsMoving) {
			if ( _isSprinting )
				_stepDistance = StepDistanceRunning;
			else if (InputExtensions.KeyInArrayPressed( MovementBackward ))
				_stepDistance = StepDistanceBackpedalling;
			else
				_stepDistance = StepDistanceWalking;

			_currentStepDistance += Movement.magnitude;
			if (_currentStepDistance >= _stepDistance) {
				_currentStepDistance = 0f;
				Footstep();
			}
		} else _currentStepDistance = 0f;
	}

	private void Footstep() {
		// Change foot, yay!
		_foot *= -1;

		// Send event
		if(OnStep != null)
			OnStep(_foot);
	}

	private void BobHead() {
		// Calculate bob distance
		if (PlayerConfig.Instance.DoHeadBob.Value && IsMoving) {
			_currentBobDistance += Movement.magnitude;

			// Smoothly change the strength of the bob depending on speed
			if (_isSprinting)
				_headBobStrength = Mathf.Lerp( _headBobStrength, HeadBobStrengthRunning, HeadBobReversalTime * Time.deltaTime );
			else
				_headBobStrength = Mathf.Lerp( _headBobStrength, HeadBobStrengthWalking, HeadBobReversalTime * Time.deltaTime );

		} else {
			// When not moving, move head back to nearest default state (0 or step distance)
			if (_currentBobDistance <= _stepDistance / 2f)
				_currentBobDistance = Mathf.Lerp( _currentBobDistance, 0f, HeadBobReversalTime * Time.deltaTime );
			else {
				_currentBobDistance = Mathf.Lerp( _currentBobDistance, _stepDistance, HeadBobReversalTime * Time.deltaTime );
			}
		}

		// Convert bob distance to radial in relation to the step distance
		// Also cap it to prevent big stupid numbers
		float currentBobRad = (_currentBobDistance / _stepDistance) * (Mathf.PI * 2f);
		if ( currentBobRad >= Mathf.PI * 2f )
			_currentBobDistance = 0f;

		// Calculate and apply new height
		float currentBobHeight = -Mathf.Cos( currentBobRad ) * _headBobStrength + _headBobStrength;
		float newHeadHeight = _headHeight + currentBobHeight;
		var newHeadPosition = new Vector3( _playerCamera.transform.localPosition.x, newHeadHeight, _playerCamera.transform.localPosition.z );

		_playerCamera.transform.localPosition = newHeadPosition;
	}

}