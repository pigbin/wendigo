﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Inventory : MonoBehaviour {

	// Singleton
	private static Inventory _instance;
	public static Inventory Instance {
		get {
			if (_instance == null)
				_instance = FindObjectOfType<Inventory>();
			return _instance;
		}
	}

	// Events
	public delegate void ItemEvent( Item item, int amount );
	public static event ItemEvent OnItemGet;
	public static event ItemEvent OnItemLose;

	// Inventory
	public List<InventorySlot> Items = new List<InventorySlot>();

	// Equipment
	public List<EquipmentPiece> Equipment = new List<EquipmentPiece>();


	void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}


	public Item AddItem( Item item, int amount) {
		// Before adding an item, check if it's already there. If so, add to it.
		foreach (InventorySlot slot in Items)
			if (slot.Item.Name == item.Name) {
				slot.Quantity += amount;
				item.OnObtain();

				UIMessageSystem.EnqueueMessage( "+ " + item.Name + " (" + amount + ")" );
				if (OnItemGet != null) OnItemGet( item, amount );

				return item;
			}

		// If it's not there, make it be there.
		Items.Add( new InventorySlot( item, amount ) );
		item.OnObtain();
		UIMessageSystem.EnqueueMessage( "+ " + item.Name + " (" + amount + ")" );
		if (OnItemGet != null) OnItemGet( item, amount );
		return item;
	}

	public void RemoveItem( string itemName, int amount ) {
		foreach (InventorySlot slot in Items)
			if (slot.Item.Name == itemName ) {

				// If amount of items in slot is greater than 'amount', simply reduce number
				if (slot.Quantity > amount) {
					slot.Quantity -= amount;
					UIMessageSystem.EnqueueMessage( "- " + slot.Item.Name + " (" + amount + ")" );
					if (OnItemLose != null) OnItemLose( slot.Item, amount );

				// If not, remove it.
				} else {
					UIMessageSystem.EnqueueMessage( "- " + slot.Item.Name + " (" + slot.Quantity + ")" );
					if (OnItemLose != null) OnItemLose( slot.Item, slot.Quantity );
					Items.Remove( slot );
				}

				break;
			}
	}


	public void Equip( EquipmentPiece eqPiece ) {
		UIMessageSystem.EnqueueMessage( "Equipped: " + eqPiece.Name);
		Equipment.Add( eqPiece );
		eqPiece.OnEquip();
	}

	public void Unequip( string equipmentName ) {
		foreach (EquipmentPiece piece in Equipment)
			if (piece.Name == equipmentName) {
				piece.OnUnEquip();
				Equipment.Remove( piece );
			}
	}


	public bool HasItem( string itemName ) {
		return Items.Any( s => s.Item.Name == itemName );
	}

	public bool HasItem( Item item ) {
		return Items.Any( s => s.Item == item );
	}

	public bool HasEquipped( string itemName ) {
		return Equipment.Any( e => e.Name == itemName );
	}

	public bool HasEquipped( EquipmentPiece item ) {
		return Equipment.Contains( item );
	}

}

[System.Serializable]
public sealed class InventorySlot {

	public InventorySlot(Item newItem, int newQuantity) {
		Item = newItem;
		Quantity = newQuantity;
	}

	public Item Item;
	public int Quantity;

}