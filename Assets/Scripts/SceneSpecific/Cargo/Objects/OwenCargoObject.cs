﻿public class OwenCargoObject : CargoObjectInteract {

	public static bool HasGreeted = false;

	protected override void Interact() {

		if (Inventory.Instance.HasItem( "Piece of cloth" ))
			MessageBoxManager.StartDialogue( new TalkToOwenAboutEmma01() );
		else if (Inventory.Instance.HasItem( "Photo" ))
			MessageBoxManager.StartDialogue( new TalkToOwenAboutEmmasPicture01() );
		else if(HasGreeted)
			MessageBoxManager.StartDialogue( new OwenIdle01() );
		else {
			MessageBoxManager.StartDialogue( new TalkToOwen01() );
			HasGreeted = true;
		}

	}

}