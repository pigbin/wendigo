﻿public class WendyCargoObject : CargoObjectInteract {

	protected override void Interact() {
		if (Inventory.Instance.HasItem( "Piece of cloth" ))
			MessageBoxManager.StartDialogue( new TalkToWendyAboutOwen01() );
		else if (Inventory.Instance.HasItem( "Photo" ))
			MessageBoxManager.StartDialogue( new EmmasPictureWendy01() );
		else
			MessageBoxManager.StartDialogue( new TalkToWendy01() );
	}

}