﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SleepingBagCargoObject : CargoObjectInteract {

	[SerializeField] private Image _screenOverlay;

	[SerializeField] private Text[] _sleepTexts;

	[SerializeField] private float _fadeTime;

	[SerializeField] private float _textFadeTime;
	[SerializeField] private float _textStayTime;

	protected override void Interact() {
		if (GlobalVariables.NeedsSleep) {
			StartCoroutine( Sleep() );
			ItemSounds.Instance.Play( ItemSounds.Instance.Sleep );
		} else {
			MessageBoxManager.StartDialogue( new NeedNoSleep01() );
		}
	}


	private IEnumerator Sleep() {
		// Block input
		BlockInterface( true );
		MessageBoxManager.MessageIsInSession = true;
		yield return null;

		// Fade
		float alpha = 0f;
		while(alpha < 1f) {
			alpha += Time.deltaTime / _fadeTime;
			_screenOverlay.color = new Color( 0f, 0f, 0f, alpha );
			yield return new WaitForEndOfFrame();
		}

		WorldTime.Instance.CurrentDay++;
		WorldTime.Instance.CurrentHour = 0;
		GlobalVariables.NeedsSleep = false;

		// Show random text
		foreach (Text st in _sleepTexts)
			st.color = new Color( 1f, 1f, 1f, 0f );

		Text fadeText = ListUtils.RandomObjectFromArray( _sleepTexts );

		// Fade text
		alpha = 0f;
		while (alpha < 1f) {
			alpha += Time.deltaTime / _textFadeTime;
			fadeText.color = new Color( 1f, 1f, 1f, alpha );
			yield return new WaitForEndOfFrame();
		}

		yield return new WaitForSeconds( _textStayTime );

		// Fade text
		alpha = 1f;
		while (alpha > 0f) {
			alpha -= Time.deltaTime / _textFadeTime;
			fadeText.color = new Color( 1f, 1f, 1f, alpha );
			yield return new WaitForEndOfFrame();
		}


		// Fade
		alpha = 1f;
		while (alpha > 0f) {
			alpha -= Time.deltaTime / _fadeTime;
			_screenOverlay.color = new Color( 0f, 0f, 0f, alpha );
			yield return new WaitForEndOfFrame();
		}

		// Unblock
		BlockInterface( false );
		MessageBoxManager.MessageIsInSession = false;
	}

}