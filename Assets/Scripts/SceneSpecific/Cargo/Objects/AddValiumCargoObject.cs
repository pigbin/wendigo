﻿using UnityEngine;

public class AddValiumCargoObject : CargoObjectInteract {

	private static bool HasFound;

	private void OnEnable() {
		enabled = !HasFound;
	}

	protected override void Interact() {
		Inventory.Instance.AddItem( new DiazepamItem(), 1 );

		HasFound = true;

		_spriteRenderer.color = Color.white;
		enabled = false;
	}

}