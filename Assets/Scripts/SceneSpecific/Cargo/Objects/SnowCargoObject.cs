﻿using UnityEngine;

public class SnowCargoObject : CargoObjectInteract {

	[SerializeField] private int _sceneToLoad;

	protected override void Interact() {
		if(GlobalVariables.NeedsSleep) {
			MessageBoxManager.StartDialogue( new NeedSleep01() );
			return;
		}

		if (!Inventory.Instance.HasEquipped( "Backpack" ) && !Inventory.Instance.HasEquipped( "Warm Jacket" )) {
			MessageBoxManager.StartDialogue( new PlaneExitReqBoth() );
		} else if (Inventory.Instance.HasEquipped( "Backpack" ) && !Inventory.Instance.HasEquipped( "Warm Jacket" )) {
			MessageBoxManager.StartDialogue( new PlaneExitReqJacket() );
		} else if (!Inventory.Instance.HasEquipped( "Backpack" ) && Inventory.Instance.HasEquipped( "Warm Jacket" )) {
			MessageBoxManager.StartDialogue( new PlaneExitReqBackpack() );
		} else {
			MessageBoxManager.StartDialogue( new PlaneExitHasBoth01() );
		}
	}

}