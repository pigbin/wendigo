﻿using UnityEngine;

public class BackPackCargoObject : CargoObjectInteract {

	protected override void Start() {
		base.Start();

		gameObject.SetActive( !Inventory.Instance.HasEquipped( "Backpack" ) );
	}

	protected override void Interact() {
		Inventory.Instance.Equip( new EqBackpack() );
		gameObject.SetActive( false );
	}

}