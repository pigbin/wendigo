﻿using UnityEngine;

public class AddPhotoCargoObject : CargoObjectInteract {

	private void OnEnable() {
		if ( !Inventory.Instance.HasItem( "Piece of cloth" )
			&& !GlobalVariables.OwenHasCloth
			&& !GlobalVariables.WendyHasCloth )
			enabled = false;
	}

	protected override void Interact() {
		Inventory.Instance.AddItem( new PhotoItem(), 1 );

		_spriteRenderer.color = Color.white;
		enabled = false;
	}

}