﻿using UnityEngine;

public class AddJacketCargoObject : CargoObjectInteract {

	private void OnEnable() {
		if ( Inventory.Instance.HasEquipped( "Warm Jacket" ) )
			enabled = false;
	}

	protected override void Interact() {
		Inventory.Instance.Equip( new EqWarmJacket() );

		_spriteRenderer.color = Color.white;
		enabled = false;
	}

}