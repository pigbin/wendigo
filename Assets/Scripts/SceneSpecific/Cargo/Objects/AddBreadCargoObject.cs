﻿using UnityEngine;

public class AddBreadCargoObject : CargoObjectInteract {

	private static bool HasFound;

	private void OnEnable() {
		enabled = !HasFound;
	}

	protected override void Interact() {
		Inventory.Instance.AddItem( new BreadItem(), 1 );

		HasFound = true;

		_spriteRenderer.color = Color.white;
		enabled = false;
	}

}