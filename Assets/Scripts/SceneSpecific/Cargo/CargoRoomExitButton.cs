﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent( typeof( Button ) )]
public class CargoRoomExitButton : MonoBehaviourExtended {

	public int SceneID;

	private Button _button;


	protected override void Start() {
		base.Start();

		// Get references
		_button = GetComponent<Button>();
	}


	private void Update() {
		if ( MessageBoxManager.MessageIsInSession
			|| IsPaused ) {
			_button.interactable = false;
			return;
		}

		_button.interactable = true;
	}


	public void ClickButton() {
		if ( GlobalVariables.NeedsSleep ) {
			MessageBoxManager.StartDialogue( new NeedSleep01() );
			return;
		}

		if ( !Inventory.Instance.HasEquipped( "Backpack" ) && !Inventory.Instance.HasEquipped( "Warm Jacket" ) ) {
			MessageBoxManager.StartDialogue( new PlaneExitReqBoth() );
		} else if ( Inventory.Instance.HasEquipped( "Backpack" ) && !Inventory.Instance.HasEquipped( "Warm Jacket" ) ) {
			MessageBoxManager.StartDialogue( new PlaneExitReqJacket() );
		} else if ( !Inventory.Instance.HasEquipped( "Backpack" ) && Inventory.Instance.HasEquipped( "Warm Jacket" ) ) {
			MessageBoxManager.StartDialogue( new PlaneExitReqBackpack() );
		} else {
			MessageBoxManager.StartDialogue( new PlaneExitHasBoth01() );
		}
	}

}