﻿using UnityEngine;

[DisallowMultipleComponent]
public class CargoObjectInteract : MonoBehaviourExtended {

	protected SpriteRenderer _spriteRenderer;
	private bool _isHovering;

	[Header( "Colors" )]
	protected Color _hoverColor = new Color( 67f / 255f, 81f / 255f, 125f / 255f );
	protected Color _interactColor = new Color( 117f / 255f, 0f / 255f, 0f / 255f );


	protected override void Start() {
		base.Start();

		// Get references
		_spriteRenderer = GetComponent<SpriteRenderer>();
	}


	public void OnMouseEnter() {
		_isHovering = true;
	}

	public void OnMouseDown() {
		if (IsPaused || MessageBoxManager.MessageIsInSession || !isActiveAndEnabled)
			return;

		_spriteRenderer.color = _interactColor;
		Interact();
	}

	public void OnMouseExit() {
		_isHovering = false;
	}


	private void Update() {
		if ( IsPaused || MessageBoxManager.MessageIsInSession || !isActiveAndEnabled )
			return;

		if (_isHovering)
			_spriteRenderer.color = _hoverColor;
		else
			_spriteRenderer.color = Color.white;
	}


	protected virtual void Interact() {

	}

}