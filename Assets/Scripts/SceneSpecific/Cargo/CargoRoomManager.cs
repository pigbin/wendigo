﻿using UnityEngine;
using System.Collections.Generic;

public class CargoRoomManager : MonoBehaviourExtended {

	public static bool MayLeavePlane {
		get {
			return Inventory.Instance.HasEquipped( "Backpack" ) && Inventory.Instance.HasEquipped( "Warm Jacket" );
		}
	}

	private static bool FirstEntry = true;

	[SerializeField] private float _timeToCalmDown;
	private float _currentCalmTime;
	private float _enterSanity;


	protected override void Start() {
		base.Start();

		// Clear inventory and let player know
		List<InventorySlot> slots = new List<InventorySlot>();
		foreach (InventorySlot slot in Inventory.Instance.Items)
			if(slot.Item.RemoveOnEnterCargoArea)
				slots.Add( new InventorySlot( slot.Item, slot.Quantity ) );
		foreach (InventorySlot slot in slots)
			Inventory.Instance.RemoveItem( slot.Item.Name, slot.Quantity );


		_currentCalmTime = _timeToCalmDown;
		_enterSanity = PlayerStats.Instance.CurrentSanity;

		OnEnterDialogue();
	}


	private void Update() {
		if ( IsPaused )	return;

		CalmDownWarmUp();
	}


	private void OnEnterDialogue() {
		if (FirstEntry) {
			MessageBoxManager.StartDialogue( new WendyIntroP01() );
			FirstEntry = false;
		}

		if(Inventory.Instance.HasEquipped("Backpack"))
			MessageBoxManager.StartDialogue( new ClearInventoryPage() );
	}


	private void CalmDownWarmUp() {
		if ( _enterSanity < 0.75f ) {
			PlayerStats.Instance.AlterSanity( 0.75f - _enterSanity, 0.25f );
			GlobalVariables.SanityBuff = 0.75f - _enterSanity;
		}

		if ( _currentCalmTime <= 0 ) return;

		_currentCalmTime -= Time.deltaTime;
		float calmPercentage = 1 - (_currentCalmTime / _timeToCalmDown);

		PlayerStats.Instance.CurrentTemperature = Mathf.Lerp( PlayerStats.Instance.CurrentTemperature, 37f, calmPercentage );
	}
}