﻿using UnityEngine;

public class CargoRoomParallaxObject : MonoBehaviourExtended {

	[Header( "Movement" )]
	[SerializeField] private Vector2 _parallaxStrength;
	private Vector2 _defaultPosition = Vector2.one;


	protected override void Start() {
		base.Start();

		// Get default values
		_defaultPosition = transform.localPosition;
	}


	private void Update() {
		if (IsPaused || MessageBoxManager.MessageIsInSession) return;

		Vector2 normPosition = GetNormalizedCursorPosition();
		Vector2 parallaxPosition = new Vector2( normPosition.x * _parallaxStrength.x, normPosition.y * _parallaxStrength.y );
		transform.localPosition = _defaultPosition - parallaxPosition;
	}


	Vector2 GetNormalizedCursorPosition() {
		float mouseRatioX = Input.mousePosition.x / Screen.width;
		float mouseRatioY = Input.mousePosition.y / Screen.height;

		var mousePos = new Vector2( Mathf.Clamp( 2 * (mouseRatioX - 0.5f), -1f, 1f ), Mathf.Clamp( 2 * (mouseRatioY - 0.5f), -1f, 1f ) );

		return mousePos;
	}

}