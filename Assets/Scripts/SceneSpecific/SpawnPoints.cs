﻿using System.Collections.Generic;
using UnityEngine;

public static class SpawnPoints {

	public static readonly Dictionary<string, SpawnPointData> Points = new Dictionary<string, SpawnPointData> {
		{"extCargoExit", new SpawnPointData( new Vector3( -2f, 21.9f, -40.4f ), 85f )},
		{"extCockpitExit", new SpawnPointData( new Vector3( 20f, 18.855f, -59f ), -37f )}
	};

}

public struct SpawnPointData {

	public Vector3 SpawnLocation;
	public float YRotation;

	public SpawnPointData( Vector3 loc, float yRot ) {
		SpawnLocation = loc;
		YRotation = yRot;
	}

}