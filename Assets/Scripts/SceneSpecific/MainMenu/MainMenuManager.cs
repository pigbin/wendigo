﻿using UnityEngine;
using System.Collections;

public class MainMenuManager : MonoBehaviour {

	public int SceneID;
	public GameObject Canvas;

	public GameObject MovieObject;
	private MovieTexture _movieTexture;

	private AudioSource _audioSource;


	private void Start() {
		_movieTexture = ((MovieTexture) MovieObject.GetComponent<Renderer>().material.mainTexture);
		MovieObject.SetActive( false );

		_audioSource = MovieObject.GetComponent<AudioSource>();
	}


	private void Update () {
		if (Input.GetKeyDown( KeyCode.Return ) && Canvas.activeSelf) {
			Canvas.SetActive( false );
			MovieObject.SetActive( true );

			_movieTexture.Play();
			_audioSource.Play();

			StartCoroutine( LoadSceneAtEndOfMovie() );
		}
    }


	private IEnumerator LoadSceneAtEndOfMovie() {
		yield return new WaitForSeconds( _movieTexture.duration );
		SceneChanger.Instance.LoadAsync( SceneID );
	}

}