﻿using UnityEngine;

public class SceneData : MonoBehaviour {

	// Singleton
	private static SceneData _instance;
	public static SceneData Instance {
		get {
			if (_instance == null)
				_instance = FindObjectOfType<SceneData>();
			return _instance;
		}
	}

	// Data
	[Header("Data stuff")]
	public bool CanPause;
	public bool TimeIsPaused;
	public bool StatsArePaused;
	public bool IsExterior;

	// Player
	[Header( "Player stuff" )]
	public Transform SpawnPoint;


	private void Start() {
		if (SpawnPoint != null) {
			GameObject player = GameObject.FindGameObjectWithTag( "Player" );
			NavMeshAgent navMeshAgent = player.GetComponent<NavMeshAgent>();
			navMeshAgent.enabled = false;

			if (SceneChanger.HasSpawnPoint) {
				player.transform.position = SceneChanger.SpawnPoint;
				player.transform.eulerAngles = new Vector3(player.transform.eulerAngles.x, SceneChanger.YRotation, player.transform.eulerAngles.z);
				SceneChanger.HasSpawnPoint = false;
			} else
				player.transform.position = SpawnPoint.position;

			navMeshAgent.enabled = true;
			Destroy( SpawnPoint.gameObject );
		}
	}

}