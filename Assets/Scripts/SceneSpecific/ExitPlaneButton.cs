﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ExitPlaneButton : MonoBehaviourExtended {

	public int SceneID;
	public string SpawnPointData;

	private Button _button;


	protected override void Start() {
		base.Start();

		// Get references
		_button = GetComponent<Button>();
	}


	private void Update() {
		if(MessageBoxManager.MessageIsInSession
			|| IsPaused) {
			_button.interactable = false;
			return;
		}

		_button.interactable = true;
	}


	public void ClickButton() {
		if(SceneChanger.Instance != null)
			SceneChanger.Instance.LoadSceneAtLocation( SceneID, SpawnPoints.Points[SpawnPointData] );
	}

}