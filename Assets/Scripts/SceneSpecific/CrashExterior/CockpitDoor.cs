﻿using System;
using UnityEngine;

public class CockpitDoor : MonoBehaviourExtended, IInteractable {

	public int SceneID;

	public void Interact( DeltaState deltaState, RaycastHit hitInfo ) {
		if (GlobalVariables.HasFledPlane) return;

		SceneChanger.Instance.LoadSceneAtLocation( SceneID, SpawnPoints.Points["extCockpitExit"] );
	}

	public void OnLookAt() {
	}

}