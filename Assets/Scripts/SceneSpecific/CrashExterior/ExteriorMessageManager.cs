﻿using UnityEngine;

public class ExteriorMessageManager : MonoBehaviourExtended {

	private static bool IsFirstVisit = true;


	protected override void Start() {
		base.Start();

		//MessageBoxManager.Instance.CloseBox();

		if (IsFirstVisit) {
			IsFirstVisit = false;
			MessageBoxManager.StartDialogue( new FirstTimeOutside01() );
		}

		if( GlobalVariables.HasFledPlane )
			MessageBoxManager.StartDialogue( new EatThePilot01() );
	}

}