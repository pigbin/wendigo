﻿using UnityEngine;

public class SkyboxManager : MonoBehaviourExtended {

	private Material _skyboxMaterial;

	[Header("Daytimes limits")]
	[Header("start (h,m) end(h,m)")]
	[SerializeField] private Vector4 _sunriseTimes;
	[SerializeField] private Vector4 _morningTimes;
	[SerializeField] private Vector4 _noonTimes;
	[SerializeField] private Vector4 _afternoonTimes;
	[SerializeField] private Vector4 _sunsetTimes;
	[SerializeField] private Vector4 _nightTimes;


	protected override void Start() {
		Destroy( gameObject );

		base.Start();

		// Get references
		_skyboxMaterial = RenderSettings.skybox;
	}


	private void Update () {
		if (IsPaused) return;
		if (_skyboxMaterial == null) {
			_skyboxMaterial = RenderSettings.skybox;
			return;
		}

		SetSkyboxBlend( "_SunriseBlend", _sunriseTimes );
		SetSkyboxBlend( "_MorningBlend", _morningTimes );
		SetSkyboxBlend( "_NoonBlend", _noonTimes );
		SetSkyboxBlend( "_AfternoonBlend", _afternoonTimes );
		SetSkyboxBlend( "_SunsetBlend", _sunsetTimes );
		SetSkyboxBlend( "_NightBlend", _nightTimes );
	}


	private float TimeToDayPercentage(float h, float m) {
		float minutesOfDay = m + h * 60f;
		return minutesOfDay / (24f * 60f);
	}

	private void SetSkyboxBlend(string blendName, Vector4 times) {
		float timeStart = TimeToDayPercentage( times.x, times.y );
		float timeEnd = TimeToDayPercentage( times.z, times.w );
		_skyboxMaterial.SetFloat( blendName, MathfExtension.RingPercentage( WorldTime.Instance.PercentageOfWholeDay, timeStart, timeEnd ) );
	}

}