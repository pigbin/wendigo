﻿using UnityEngine;

public class ExteriorManager : MonoBehaviourExtended {

	private static bool IsFirstVisit = true;


	protected override void Start() {
		base.Start();

		//WorldTime.Instance.CurrentMinute = 1;

		if (IsFirstVisit) {
			IsFirstVisit = false;
			MessageBoxManager.StartDialogue( new FirstTimeOutside01() );
		}

		if( GlobalVariables.HasFledPlane )
			MessageBoxManager.StartDialogue( new EatThePilot01() );

		if ( GlobalVariables.SanityBuff != null ) {
			PlayerStats.Instance.AlterSanity( GlobalVariables.SanityBuff.GetValueOrDefault(), 3 * 60f );
		}
	}

}