﻿using UnityEngine;

public class Snowstorm : MonoBehaviourExtended {

	public ParticleSystem Particles;

	protected override void OnPauseEnter(bool openPauseMenu ) {
		Particles.Pause();
	}

	protected override void OnPauseExit() {
		Particles.Play();
	}

}
