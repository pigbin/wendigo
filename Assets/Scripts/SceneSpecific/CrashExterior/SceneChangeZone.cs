﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class SceneChangeZone : MonoBehaviour {

	public int SceneID;

	private void OnTriggerEnter( Collider other ) {
		if (other.tag != "Player"
			|| GlobalVariables.HasFledPlane ) return;

		SceneChanger.Instance.LoadAsync( SceneID );
	}

}