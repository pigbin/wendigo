﻿using System;
using UnityEngine;

public class dbgSceneChanger : MonoBehaviour, IInteractable {

	public int SceneID;

	public void OnLookAt() {
		// Nothing
	}

	public void Interact( DeltaState deltaState, RaycastHit hitInfo ) {
		if(deltaState == DeltaState.Enter)
			ApplicationExtensions.LoadLevelReleaseCursor( SceneID );
	}

}
