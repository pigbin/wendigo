﻿using UnityEngine;

public class BerryBush : MonoBehaviourExtended, IInteractable {

	private GameObject[] _berries = new GameObject[5];
	[SerializeField] [Range(3, 5)] private int _amount = 3;


	protected override void Start() {
		base.Start();

		// Find berries
		for (int i = 0; i < _berries.Length; i++) {
			_berries[i] = transform.GetChild( 0 ).GetChild( i ).gameObject;
			_berries[i].SetActive( i < _amount );
		}

		gameObject.name = string.Format( "Bush of {0} berries", _amount );
	}


	public void OnLookAt() {
		
	}

	public void Interact( DeltaState deltaState, RaycastHit hitInfo ) {
		if (deltaState != DeltaState.Enter) return;

		switch ( _amount ) {
			case 3:
				Inventory.Instance.AddItem( new TwigBerries_3(), 1 );
				break;
			case 4:
				Inventory.Instance.AddItem( new TwigBerries_4(), 1 );
				break;
			case 5:
				Inventory.Instance.AddItem( new TwigBerries_5(), 1 );
				break;
		}
		

		// Remove interactivity from object or something. You know what I mean.
		foreach (GameObject berry in _berries)
			berry.SetActive( false );

		gameObject.tag = "Untagged";
	}
}