﻿using System;
using UnityEngine;

public class ClothObject : MonoBehaviourExtended, IInteractable {

	public void OnLookAt() {}

	public void Interact( DeltaState deltaState, RaycastHit hitInfo ) {
		if ( deltaState != DeltaState.Enter )
			return;

		if (!gameObject.activeSelf) return;

		MessageBoxManager.StartDialogue( new Traces2_01() );

		Inventory.Instance.AddItem( new ClothItem(), 1 );

		// Remove interactivity from object or something. You know what I mean.
		gameObject.SetActive( false );
		gameObject.tag = "Untagged";
	}
}