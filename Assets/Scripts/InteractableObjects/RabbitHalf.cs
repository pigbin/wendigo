﻿using System;
using UnityEngine;

public class RabbitHalf : MonoBehaviourExtended, IInteractable {

	private static bool HasSeenRabbit;

	public void OnLookAt() {
		if (HasSeenRabbit) return;

		HasSeenRabbit = true;
		MessageBoxManager.StartDialogue( new DeadRabbitFound01() );
	}

	public void Interact( DeltaState deltaState, RaycastHit hitInfo ) {
		if ( deltaState != DeltaState.Enter )
			return;
		if (!gameObject.activeSelf) return;

		MessageBoxManager.StartDialogue( new DeadRabbitCollected01() );

		Inventory.Instance.AddItem( new DeadRabbitItem(), 1 );

		// Remove interactivity from object or something. You know what I mean.
		gameObject.SetActive( false );
		gameObject.tag = "Untagged";
	}
}