﻿using UnityEngine;

public interface IInteractable {

	void OnLookAt();

	void Interact( DeltaState deltaState, RaycastHit hitInfo );

}

public enum DeltaState {

	None,
    Enter,
	Stay,
	Exit

}