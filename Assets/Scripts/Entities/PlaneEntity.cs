﻿using UnityEngine;

[ExecuteInEditMode]
public class PlaneEntity : MonoBehaviourExtended {

	private Transform _player;

	// Rotation
	[Header( "Rotation" )]
	private Vector3 _worldRotation;
	private Vector3 _worldLooking;
	private readonly Vector3[] _fixedRotationsWorld = {
		new Vector3( 0, 0, 1 ).normalized,
		new Vector3( 1, 0, 1 ).normalized,
		new Vector3( 1, 0, 0 ).normalized,
		new Vector3( 1, 0, -1 ).normalized,
		new Vector3( 0, 0, -1 ).normalized,
		new Vector3( -1, 0, -1 ).normalized,
		new Vector3( -1, 0, 0 ).normalized,
		new Vector3( -1, 0, 1 ).normalized,
	};
	private Vector3[] _fixedRotationsLocal = new Vector3[8];
	public Vector3 LookingDirection;
	[Range( 0, 7 )] [SerializeField] private int _lookingDirectionIndex;
	public bool LockLookingDirection;
	public Vector3 LookingDirectionLockAt;

	[Header( "Lock rotation axes" )]
	public bool DoLockX;
	public float LockX = 90;
	public bool DoLockZ;
	public float LockZ = 0;

	[Header( "Add to rotation" )]
	[SerializeField] private Vector3 _addToRotation;


	protected override void Start() {
		base.Start();

		// Get references
		_player = GameObject.FindGameObjectWithTag( "Player" ).transform;

		LookAt( _player );
	}


	private void Update() {
		if (IsPaused) return;

		RotateToPlayer();
		transform.eulerAngles += _addToRotation;

		// Transform fixed directions to world space
		for (var i = 0; i < _fixedRotationsWorld.Length; i++)
			_fixedRotationsLocal[i] = (Quaternion.AngleAxis( _worldRotation.y, Vector3.up ) * _fixedRotationsWorld[i]).normalized;
		_lookingDirectionIndex = GetTextureDirection();

		//DrawDebug();
	}


	// Utilities
	//===========================================================================
	/// <summary>
	/// Sets the LookingDirection to the target.
	/// </summary>
	public void LookAt( Vector3 target ) {
		if (!LockLookingDirection)
			LookingDirection = (target - transform.position).normalized;
		else
			LookingDirection = LookingDirectionLockAt.normalized;
	}

	/// <summary>
	/// Sets the LookingDirection to the target.
	/// </summary>
	public void LookAt( GameObject target ) {
		LookAt( target.transform.position );
	}

	/// <summary>
	/// Sets the LookingDirection to the target.
	/// </summary>
	public void LookAt( Transform target ) {
		LookAt( target.position );
	}


	private void RotateToPlayer() {
		_worldRotation = Quaternion.LookRotation( (_player.position - transform.position), Vector3.up ).eulerAngles;
		if (DoLockX) _worldRotation.x = LockX;
		if (DoLockZ) _worldRotation.z = LockZ;

		transform.eulerAngles = _worldRotation;
	}

	private int GetTextureDirection() {
		return Vector3Extensions.GetClosestVectorIndexFromArray( LookingDirection, _fixedRotationsLocal );
	}


	// DEBUG
	//===========================================================================
	void DrawDebug() {
		foreach (Vector3 rot in _fixedRotationsLocal) {
			if(rot == Vector3Extensions.GetClosestVectorFromArray( LookingDirection, _fixedRotationsLocal ))
                Debug.DrawRay( transform.position, rot * 1.5f, Color.blue );
			else
				Debug.DrawRay( transform.position, rot * 1.5f, Color.green );
		}

		Debug.DrawRay( transform.position, LookingDirection * 2.5f, Color.black );
	}

}