﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class PlayerDepthOfField : MonoBehaviourExtended {

	// Dependencies
	private DepthOfField _cameraEffect;

	// Ray
	[Header( "Raycast" )]
	public float MaxRayDistance;
	public LayerMask LayerMask;

	// Effect
	[Header( "Effect" )]
	public float FocalSize;
	public float Aperture;
	[SerializeField] private float _targetFocalDistance;
	[SerializeField] private float _targetFocalSize;
	[SerializeField] private float _targetAperture;


	protected override void Start() {
		base.Start();

		// Get references and all that you know the drill
		_cameraEffect = GetComponent<DepthOfField>();
	}


	private void Update() {
		if (!IsPaused) {
			RaycastHit hit;
			if (Physics.Raycast( transform.position, transform.forward, out hit, MaxRayDistance, LayerMask )) {
				_targetFocalDistance = hit.distance;
				_targetAperture = Aperture;
				_targetFocalSize = FocalSize - 0.25f;
			} else {
				_targetAperture = 0f;
			}

			_cameraEffect.focalLength = Mathf.Lerp( _cameraEffect.focalLength, _targetFocalDistance, Time.deltaTime / 0.5f );
			_cameraEffect.focalSize = Mathf.Lerp( _cameraEffect.focalSize, _targetFocalSize, Time.deltaTime / 0.5f );
			_cameraEffect.aperture = Mathf.Lerp( _cameraEffect.aperture, _targetAperture, Time.deltaTime / 0.5f );

		} else {
			_cameraEffect.focalLength = 0f;
			_cameraEffect.focalSize = 0f;
			_cameraEffect.aperture = 12f;
		}
	}

}