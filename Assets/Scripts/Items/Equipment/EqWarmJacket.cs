﻿using UnityEngine;

public class EqWarmJacket : EquipmentPiece {

	public EqWarmJacket() {
		Name = "Warm Jacket";
		Description = "Jackets like to hug people until they are all comfy.";

		TemperatureMultiplier = 0.5f;
	}

	public override void OnEquip() {
		MessageBoxManager.StartDialogue( new OnJacketObtain() );
	}


	class OnJacketObtain : MessageBoxPage {
		public OnJacketObtain() {
			Speaker = "";

			LineOne = new Line1();
			LineTwo = new Line2();
			LineThree = new Line3();

			NextPage = null;
		}

		class Line1 : MessageBoxLine {
			public Line1() {
				Text = "Oh, cool, a jacket!";
				LineType = MessageBoxLineType.Text;
			}

			public override void Select() {
			}
		}

		class Line2 : MessageBoxLine {
			public Line2() {
				Text = "It'll help me keep warm.";
				LineType = MessageBoxLineType.Text;
			}

			public override void Select() {
			}
		}

		class Line3 : MessageBoxLine {
			public Line3() {
				Text = "";
				LineType = MessageBoxLineType.Text;
			}

			public override void Select() {
			}
		}
	}
}