﻿using UnityEngine;

[System.Serializable]
public class EquipmentPiece {

	public string Name;
	public string Description;
	public Item Item;

	public float TemperatureMultiplier = 1f;


	public virtual void OnEquip() {}

	public virtual void OnUnEquip() {
		Inventory.Instance.AddItem( Item, 1 );
	}
}