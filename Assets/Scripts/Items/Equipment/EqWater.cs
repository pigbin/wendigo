﻿using UnityEngine;

public class EqWater : EquipmentPiece {

	public EqWater() {
		Name = "Water";
	}

}

public class EqWaterWarm : EquipmentPiece {

	public EqWaterWarm() {
		Name = "Warm water";
	}

	public override void OnEquip() {
		Inventory.Instance.Unequip( "Water" );
	}

}