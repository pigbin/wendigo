﻿public class ClothItem : Item {

	// Stats
	private const float HungerReduction = -1f;

	public ClothItem() {
		Name = "Piece of cloth";
		Description = "Maybe Wendy and Owen know what to do with this.";
		CanUse = false;
		RemoveOnEnterCargoArea = false;
    }

	public override void Use() {
		AlterHunger( HungerReduction );
		PlaySound( ListUtils.RandomObjectFromArray( ItemSounds.Instance.EatingSounds ) );
		RemoveFromInventory( 1 );
	}

}