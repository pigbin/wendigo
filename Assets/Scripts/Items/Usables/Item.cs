﻿using UnityEngine;

[System.Serializable]
public class Item {

	public string Name;
	public string Description;

	public bool CanUse { get; protected set; }

	public bool RemoveOnEnterCargoArea { get; protected set; }

	public virtual void Use() {}

	public virtual void OnObtain() { }

	protected void RemoveFromInventory(int amount) {
		Inventory.Instance.RemoveItem( Name, amount );
	}


	// Stats
	protected void AlterHunger( float value ) {
		PlayerStats.Instance.AlterHunger( value );
	}

	protected void AlterThirst( float value ) {
		PlayerStats.Instance.AlterThirst( value );
	}

	protected void AlterTemperature( float value ) {
		PlayerStats.Instance.AlterTemperature( value );
	}

	protected void AlterSanity( float value, float time ) {
		PlayerStats.Instance.AlterSanity( value, time );
	}


	// Misc
	protected void PlaySound(AudioClip sound) {
		if (ItemSounds.Instance == null) throw new System.Exception( "No instance of ItemSounds is available in the scene!" );

		ItemSounds.Instance.Play( sound );
	}
}
