﻿public class PhotoItem : Item {

	public PhotoItem() {
		Name = "Photo";
		Description = "Interesting, but just who is this woman?";
		CanUse = false;
		RemoveOnEnterCargoArea = false;
	}

	public override void OnObtain() {
		MessageBoxManager.StartDialogue( new OnPhotoObtain() );
	}

	class OnPhotoObtain : MessageBoxPage {
		public OnPhotoObtain() {
			Speaker = "";

			LineOne = new Line1();
			LineTwo = new Line2();
			LineThree = new Line3();

			NextPage = null;
		}

		class Line1 : MessageBoxLine {
			public Line1() {
				Text = "Who is this woman?";
				LineType = MessageBoxLineType.Text;
			}

			public override void Select() {
			}
		}

		class Line2 : MessageBoxLine {
			public Line2() {
				Text = "";
				LineType = MessageBoxLineType.Text;
			}

			public override void Select() {
			}
		}

		class Line3 : MessageBoxLine {
			public Line3() {
				Text = "";
				LineType = MessageBoxLineType.Text;
			}

			public override void Select() {
			}
		}
	}

}