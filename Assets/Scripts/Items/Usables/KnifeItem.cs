﻿public class KnifeItem : Item {

	public KnifeItem() {
		Name = "Knife";
		Description = "Comes in handy to cut anything down (mind you!).";
		CanUse = true;
		RemoveOnEnterCargoArea = false;
    }

	public override void Use() {
		AlterSanity( -1f, 5f );
		InventoryUIMain.Instance.CloseInventory();
	}

}