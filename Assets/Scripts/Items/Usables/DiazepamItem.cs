﻿public class DiazepamItem : Item {

	// Stats
	private const float SanityEffect = 0.75f;

	public DiazepamItem() {
		Name = "Valium";
		Description = "Not fond of psycho pills, but maybe these can help.";
		CanUse = true;

		RemoveOnEnterCargoArea = true;
    }

	public override void Use() {
		AlterSanity( SanityEffect, 120f );
		PlaySound( ListUtils.RandomObjectFromArray( ItemSounds.Instance.EatingSounds ) );
		RemoveFromInventory( 1 );
	}

}