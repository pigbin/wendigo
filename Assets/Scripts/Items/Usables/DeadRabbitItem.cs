﻿public class DeadRabbitItem : Item {

	// Stats
	private const float HungerReduction = -1f;

	public DeadRabbitItem() {
		Name = "½ rabbit";
		Description = "Poor creature. But life is life and feeds on itself.";
		CanUse = true;

		RemoveOnEnterCargoArea = true;
    }

	public override void Use() {
		AlterHunger( HungerReduction );
		PlaySound( ListUtils.RandomObjectFromArray( ItemSounds.Instance.EatingSounds ) );
		RemoveFromInventory( 1 );
	}

}