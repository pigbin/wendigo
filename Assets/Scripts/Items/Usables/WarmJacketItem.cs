﻿public class WarmJacketItem : Item {

	public WarmJacketItem() {
		Name = "Warm Jacket";
		Description = "It's waaaaaarm! <3";
	}

	public override void Use() {
		Inventory.Instance.Equip( new EqWarmJacket() );
		RemoveFromInventory( 1 );
	}

}