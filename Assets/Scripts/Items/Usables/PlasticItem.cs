﻿public class PlasticItem : Item {

	public PlasticItem() {
		Name = "Plastic";
		Description = "Feels strong. I bet you can make something out of it.";
		CanUse = false;
		RemoveOnEnterCargoArea = false;
	}

}