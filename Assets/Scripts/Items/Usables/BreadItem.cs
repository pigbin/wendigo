﻿public class BreadItem : Item {

	// Stats
	private const float HungerReduction = -1f;

	public BreadItem() {
		Name = "Old Bread";
		Description = "Hard to chew, but nutrimental.";
		CanUse = true;
		RemoveOnEnterCargoArea = true;
    }

	public override void Use() {
		AlterHunger( HungerReduction );
		PlaySound( ListUtils.RandomObjectFromArray( ItemSounds.Instance.EatingSounds ) );
		RemoveFromInventory( 1 );
	}

}