﻿public class TwigBerries_3 : Item {

	// Stats
	private const float BerryAmount = 3;
	private const float HungerReduction = -0.05f;

	public TwigBerries_3() {
		Name = "Twig of 3 berries";
		Description = "Not much, but hopefully diminish hunger anyway.";
		CanUse = true;
		RemoveOnEnterCargoArea = true;
    }

	public override void Use() {
		AlterHunger( HungerReduction * BerryAmount );
		PlaySound( ListUtils.RandomObjectFromArray( ItemSounds.Instance.EatingSounds ) );
		RemoveFromInventory( 1 );
	}

}

public class TwigBerries_4 : Item {

	// Stats
	private const float BerryAmount = 4;
	private const float HungerReduction = -0.05f;

	public TwigBerries_4() {
		Name = "Twig of 4 berries";
		Description = "Not much, but hopefully diminish hunger anyway.";
		CanUse = true;
		RemoveOnEnterCargoArea = true;
	}

	public override void Use() {
		AlterHunger( HungerReduction * BerryAmount );
		PlaySound( ListUtils.RandomObjectFromArray( ItemSounds.Instance.EatingSounds ) );
		RemoveFromInventory( 1 );
	}

}

public class TwigBerries_5 : Item {

	// Stats
	private const float BerryAmount = 5;
	private const float HungerReduction = -0.05f;

	public TwigBerries_5() {
		Name = "Twig of 5 berries";
		Description = "Not much, but hopefully diminish hunger anyway.";
		CanUse = true;
		RemoveOnEnterCargoArea = true;
	}

	public override void Use() {
		AlterHunger( HungerReduction * BerryAmount );
		PlaySound( ListUtils.RandomObjectFromArray( ItemSounds.Instance.EatingSounds ) );
		RemoveFromInventory( 1 );
	}

}