﻿using UnityEngine;
using System.Collections.Generic;

public static class UIMessageSystem {

	// The queue *thunder*
    private static Queue<string> _messageQueue = new Queue<string>();

	
	public static void EnqueueMessage( string message ) {
		_messageQueue.Enqueue( message );
	}

	public static string RequestMessage() {
		if(_messageQueue.Count > 0)
			return _messageQueue.Dequeue();

		return null;
	}

}