﻿using UnityEngine;
using UnityEngine.UI;

public class CurrentlyHighlightedTextUI : MonoBehaviourExtended {

	// Dependencies
	[SerializeField] private PlayerObjectInteractionCast _objectInteractionCast;
	[SerializeField] private Text _screenText;


	protected override void Start() {
		base.Start();

		// Get references
		_screenText = GetComponent<Text>();
		_objectInteractionCast = FindObjectOfType<PlayerObjectInteractionCast>();


		// If no caster script can be found: Disable for now.
		if (_objectInteractionCast == null)
			gameObject.SetActive( false );
	}


	private void Update() {
		if (!IsPaused)
			if (_objectInteractionCast != null)
				if (_objectInteractionCast.CurrentlyTargetting == null)
					_screenText.text = "";
				else
					_screenText.text = "[E] " + _objectInteractionCast.CurrentlyTargetting.name;
	}

}