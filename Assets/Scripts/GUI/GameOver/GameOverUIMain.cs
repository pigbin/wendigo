﻿using UnityEngine;

public class GameOverUIMain : MonoBehaviourExtended {

	// The main UI object
	[SerializeField] private GameObject _hungerPaper;
	[SerializeField] private GameObject _thirstPaper;
	[SerializeField] private GameObject _suicidePaper;
	[SerializeField] private GameObject _frozenPaper;

	private Animation _hungerAnimation;
	private Animation _thirstAnimation;
	private Animation _suicideAnimation;
	private Animation _frozenAnimation;


	protected override void Start() {
		base.Start();

		// Get references
		_hungerAnimation = _hungerPaper.GetComponent<Animation>();
		_thirstAnimation = _thirstPaper.GetComponent<Animation>();
		_suicideAnimation = _suicidePaper.GetComponent<Animation>();
		_frozenAnimation = _frozenPaper.GetComponent<Animation>();

		// Subscribe
		PlayerStats.OnPlayerDeath += OpenUI;
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unscubscribe
		PlayerStats.OnPlayerDeath -= OpenUI;
	}


	private void OpenUI(DeathCause cause) {
		if (cause == DeathCause.Hunger) {
			_hungerPaper.SetActive( true );
			_hungerAnimation.Play();
		} else if (cause == DeathCause.Insanity) {
			_suicidePaper.SetActive( true );
			_suicideAnimation.Play();
		} else if (cause == DeathCause.Temperature) {
			_frozenPaper.SetActive( true );
			_frozenAnimation.Play();
		} else if (cause == DeathCause.Thirst) {
			_thirstPaper.SetActive( true );
			_thirstAnimation.Play();
		}
	}

}