﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIMessage : MonoBehaviourExtended {

	// The text element
	public Text TextElement;

	// Display stuffs
	[Header( "Display data" )]
	[SerializeField] private float _displayTime;
	[SerializeField] private float _fadeInTime;
	[SerializeField] private float _fadeOutTime;
	[SerializeField] private float _messageCooldown;
	[SerializeField] private bool _canDisplayMessage;


	protected override void Start() {
		base.Start();
		StartCoroutine( InitialCooldown() );
	}

	private void Update() {
		if (!IsPaused && _canDisplayMessage) {
			string messageToDisplay = UIMessageSystem.RequestMessage();
			if(messageToDisplay != null)
				StartCoroutine(ShowMessage(messageToDisplay));
		}
	}


	private IEnumerator InitialCooldown() {
		yield return new WaitForSeconds( _messageCooldown );
		_canDisplayMessage = true;
	}

	private IEnumerator ShowMessage(string msg) {
		// Stop messages from showing up
		_canDisplayMessage = false;

		// Set string I guess
		TextElement.text = msg;

		// Fade in
		Color newColor = Color.white;
		newColor.a = 0f;
		TextElement.color = newColor;
		while (TextElement.color.a < 1f) {
			if (!IsPaused) {
				newColor.a += Time.deltaTime / _fadeInTime;
				TextElement.color = newColor;
			}
			yield return null;
		}

		// Stay
		yield return new WaitForSeconds( _displayTime );

		// Fade out
		newColor = Color.white;
		TextElement.color = newColor;
		while (TextElement.color.a > 0f) {
			if (!IsPaused) {
				newColor.a -= Time.deltaTime / _fadeOutTime;
				TextElement.color = newColor;
			}
			yield return null;
		}

		yield return new WaitForSeconds( _messageCooldown );
		_canDisplayMessage = true;
	}

}