﻿using UnityEngine;
using System.Collections.Generic;

public static class HighlightWindowManager {

	// The stack itself.
	public readonly static List<GameObject> ObjectHierarchy = new List<GameObject>();

	// The event when the stack changes
	public delegate void FocusChange();
	public static event FocusChange OnFocusChange;


	public static void FocusOnObject( GameObject objectToFocus ) {
		// Check if the object is already in the list. If it is, bring it to the front.
		foreach (GameObject o in ObjectHierarchy)
			if (o == objectToFocus) {
				ObjectHierarchy.Remove( o );
				ObjectHierarchy.Add( objectToFocus );
				return;
			}

		// If not, add it.
		ObjectHierarchy.Add( objectToFocus );
	}

	public static void UnFocusCurrent() {
		if(ObjectHierarchy.Count > 0)
			ObjectHierarchy.Remove( ObjectHierarchy[ObjectHierarchy.Count - 1] );
	}

	public static bool IsFocused( GameObject objectToCheck ) {
		if (ObjectHierarchy.Count == 0) return false;
		return ObjectHierarchy[ObjectHierarchy.Count - 1] == objectToCheck;
	}

}
