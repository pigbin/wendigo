﻿using UnityEngine;
using UnityEngine.UI;

public class HeadBobConfigBox : MonoBehaviour {

	public Toggle Checkbox;

	void Start() {
		UpdateValue();

		PlayerConfig.Instance.DoHeadBob.OnValuechange += UpdateValue;
	}

	void OnDestroy() {
		PlayerConfig.Instance.DoHeadBob.OnValuechange -= UpdateValue;
	}

	void OnEnable() {
		UpdateValue();
	}

	public void OnChange( bool newValue ) {
		UISounds.Instance.Play( UISounds.Instance.UiClick );

		PlayerConfig.Instance.DoHeadBob.Value = newValue;
	}

	private void UpdateValue() {
		Checkbox.isOn = PlayerConfig.Instance.DoHeadBob.Value;
	}

}