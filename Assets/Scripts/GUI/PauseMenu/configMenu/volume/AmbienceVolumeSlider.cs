﻿using UnityEngine;
using UnityEngine.UI;

public class AmbienceVolumeSlider : UISlider {

	void OnEnable() {
		if(Slider == null)
			Slider = GetComponent<Slider>();

		Slider.value = PlayerConfig.Instance.VolumeAmbient.Value;
	}

	public void OnChange() {
		PlayerConfig.Instance.VolumeAmbient.Value = Slider.value;
		AudioControl.Instance.SetVolume( "ambienceVolume", Slider.value );
	}

	protected override void OnReleaseSlider() {
		UISounds.Instance.Play( UISounds.Instance.UiClick );
	}

	private void OnMouseDown() {
		Debug.Log( "LOL" );
	}

}
