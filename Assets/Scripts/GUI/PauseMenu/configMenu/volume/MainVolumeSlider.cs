﻿using UnityEngine;
using UnityEngine.UI;

public class MainVolumeSlider : MonoBehaviour {

	public Slider Slider;

	void OnEnable() {
		Slider.value = PlayerConfig.Instance.VolumeMain.Value;
	}

	public void OnChange() {
		PlayerConfig.Instance.VolumeMain.Value = Slider.value;
		AudioControl.Instance.SetVolume( "mainVolume", Slider.value );
	}


	private void OnMouseUpAsButton() {
		UISounds.Instance.Play( UISounds.Instance.UiClick );
	}

}
