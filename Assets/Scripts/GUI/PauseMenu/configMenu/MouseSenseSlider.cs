﻿using UnityEngine;
using UnityEngine.UI;

public class MouseSenseSlider : MonoBehaviour {

	public Slider Slider;

	void OnEnable() {
		Slider.value = PlayerConfig.Instance.MouseLookingSensitivity.Value;
	}

	public void OnChange() {
		PlayerConfig.Instance.MouseLookingSensitivity.Value = Slider.value;
	}


	private void OnMouseUpAsButton() {
		UISounds.Instance.Play( UISounds.Instance.UiClick );
	}
}	