﻿using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviourExtended {

	// References
	[Header("Objects")]
	[SerializeField] private GameObject _canvas;
	[SerializeField] private GameObject _mainScreen;
	[SerializeField] private GameObject _configScreen;
	[SerializeField] private GameObject _serverWarning;
	[SerializeField] private GameObject _exitConfirmation;

	// Textures
	[Header( "Textures" )]
	[SerializeField] private Image _gpsImage;
	[SerializeField] private Sprite _normalGpsSprite;
	[SerializeField] private Sprite _configGpsSprite;


	// Pausing
	protected override void OnPauseEnter(bool openPauseMenu) {
		_gpsImage.sprite = _normalGpsSprite;

		_canvas.SetActive( openPauseMenu );
		_mainScreen.SetActive( openPauseMenu );
	}

	protected override void OnPauseExit() {
		ConfigParser.Instance.LoadConfigFile();
		ConfigParser.Instance.ParseXmlToConfig();

		_mainScreen.SetActive( false );
		_configScreen.SetActive( false );
		_canvas.SetActive( false );
	}


	// Buttons
	public void ShowServerError( bool show ) {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );
		_serverWarning.SetActive( show );
	}


	public void ExitGamePreConfirm() {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );
		_exitConfirmation.SetActive( true );
	}

	public void DenyExit() {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );
		_exitConfirmation.SetActive( false );
	}

	public void ExitGame() {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );

#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}


	public void UnpauseGame() {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );
		GamePauseManager.Instance.Unpause();
	}

	public void ToggleOptions(bool openOptions) {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );
		_mainScreen.SetActive( !openOptions );
		_configScreen.SetActive( openOptions );

		_gpsImage.sprite = openOptions ? _configGpsSprite : _normalGpsSprite;
	}


	public void DismissChanges() {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );
		ConfigParser.Instance.ParseXmlToConfig();
		ToggleOptions( false );
	}

	public void SaveChanges() {
		UISounds.Instance.Play( UISounds.Instance.UiSelect );
		ConfigParser.Instance.SaveToFile();
		ToggleOptions( false );
	}

}