﻿using UnityEngine;
using UnityEngine.UI;

public class UISlider : MonoBehaviourExtended {

	[SerializeField] protected Slider Slider;

	private bool _hasMouseDownAsButton;


	protected override void Start() {
		base.Start();

		// Get references
		Slider = GetComponent<Slider>();
	}

	private void OnMouseDown() {
		Debug.Log( "D" );
		_hasMouseDownAsButton = true;
	}

	private void OnMouseUp() {
		Debug.Log( "U" );
		if(_hasMouseDownAsButton)
			OnReleaseSlider();
		_hasMouseDownAsButton = false;
	}

	protected virtual void OnReleaseSlider() {}

}