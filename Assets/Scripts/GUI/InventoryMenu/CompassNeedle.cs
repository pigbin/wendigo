﻿using UnityEngine;
using UnityEngine.UI;

public class CompassNeedle : MonoBehaviourExtended {

	[Header("Directions")]
	[SerializeField] private Vector3 WorldNorthVector;
	private Vector3 WorldWestVector;

	[Header( "Sprite Settings" )]
	[SerializeField] private Image _uiImage;
	[SerializeField] private Sprite[] _needleSprites;
	private Vector3[] _directionVectors;


	//private void Update() {
	//	Debug.DrawRay( PlayerController.Instance.transform.position, WorldNorthVector * 5f, Color.red );
	//	Debug.DrawRay( PlayerController.Instance.transform.position, WorldNorthVector * -5f, Color.blue );

	//	Debug.DrawRay( PlayerController.Instance.transform.position, PlayerController.Instance.transform.forward * 3f, Color.green );

	//	Vector3 closest = Vector3Extensions.GetClosestVectorFromArray( PlayerController.Instance.transform.forward, _directionVectors );
	//	Debug.DrawRay( PlayerController.Instance.transform.position, closest * 3f, Color.blue );


	//	int closestInt = Vector3Extensions.GetClosestVectorIndexFromArray( PlayerController.Instance.transform.forward, _directionVectors );
	//	closestInt = Mathf.Abs( _needleSprites.Length - closestInt  ) ;
	//	closestInt = closestInt % 16;
	//	if (_uiImage != null)
	//		_uiImage.sprite = _needleSprites[closestInt];
	//}


	private void OnEnable() {
		if(_uiImage == null) _uiImage = GetComponent<Image>();
		if(_directionVectors == null) _directionVectors = GetConstantDirectionVectors();

		int closestInt = PlayerController.Instance != null ? Vector3Extensions.GetClosestVectorIndexFromArray( PlayerController.Instance.transform.forward, _directionVectors ) : 0;
		closestInt = Mathf.Abs( _needleSprites.Length - closestInt );
		closestInt = closestInt % 16; // Keep it in range of 0 to 15

		if (_uiImage == null) {
			Debug.LogError( "This object has no Image component attached to it!" );
			return;
		}

		_uiImage.sprite = _needleSprites[closestInt];
	}


	private Vector3[] GetConstantDirectionVectors() {
		var output = new Vector3[_needleSprites.Length];
		float rotationAngle = 360f / output.Length;

		for (var i = 0; i < output.Length; i++) {
			Vector3 newDirection = Quaternion.AngleAxis( rotationAngle * i, Vector3.up ) * WorldNorthVector;
			output[i] = newDirection;
		}

		return output;
	}


	private void PointNorthDeprecated() {
		if ( WorldWestVector == Vector3.zero ) {
			WorldNorthVector.Normalize();
			WorldWestVector = Vector3.Cross( WorldNorthVector, Vector3.up ).normalized;
		}

		float dotNorth = Vector3.Dot( WorldNorthVector, PlayerController.Instance.transform.forward );
		float dotWest = Vector3.Dot( WorldWestVector, PlayerController.Instance.transform.forward );

		float needleRotation = dotWest * -90f;
		if ( dotNorth < 0 ) needleRotation = 180 - needleRotation;

		transform.localEulerAngles = new Vector3( 0f, 0f, needleRotation );
	}

}