﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIMain : MonoBehaviourExtended {

	// Singleton
	private static InventoryUIMain _instance;
	public static InventoryUIMain Instance {
		get {
			if (_instance == null)
				_instance = FindObjectOfType<InventoryUIMain>();
			return _instance;
		}
	}

	// Dependencies
	public GameObject MenuContent;

	[Header("Item UI")]
	public Text ItemInfoName;
	public Text ItemInfoDescription;

	// Input
	[Header("Input")]
	public KeyCode[] OpenCloseKeys;
	public KeyCode[] SelectNextItemKeys;
	public KeyCode[] SelectPreviousItemKeys;
	public static int InputBlockers;

	// Event
	public delegate void Open();
	public delegate void Close();

	public static event Open OnInventoryOpen;
	public static event Close OnInventoryClose;

	// Item selection
	[SerializeField] private int _currentItemIndex;
	[SerializeField] private GameObject _currentlyShownImage;
	private GameObject[] _itemImageObjects;
	private GameObject _imageMissingObject;
	private GameObject _bagEmptyObject;

	// Animations
	[Header( "Animations" )]
	public float MoveOutOfBagTime;
	private float _currentMoveOutOfBagTime;
	public float MoveIntoBagTime;
	private float _currentMoveIntoBagTime;
	public float TimeBetweenAnimations;
	public Vector3 AnimStartPosition;
	public Vector3 AnimEndPosition;
	private bool _itemPutAwayAnimationIsRunning;

	// Addons
	[Header("Addons")]
	[SerializeField] private GameObject _mapAddon;
	[SerializeField] private GameObject _compassAddon;
	[SerializeField] private GameObject _waterBottleAddon;
	[SerializeField] private GameObject _waterColdEmpty;
	[SerializeField] private GameObject _waterColdFull;
	[SerializeField] private GameObject _waterWarmEmpty;
	[SerializeField] private GameObject _waterWarmFull;

	protected override void Start() {
		base.Start();

		// Find all item images
		Transform itemFolder = transform.GetChild( 0 ).FindChild( "Bag" ).FindChild( "Items" );
        _itemImageObjects = new GameObject[itemFolder.childCount];
		for (int c = 0; c < itemFolder.childCount; c++) {
			_itemImageObjects[c] = itemFolder.GetChild( c ).gameObject;
			_itemImageObjects[c].SetActive( false );
		}

		_bagEmptyObject = itemFolder.FindChild( "empty" ).gameObject;
		_imageMissingObject = itemFolder.FindChild( "imageMissing" ).gameObject;
	}


	private void Update() {
		// Check for open/close input
		if(InputBlockers == 0 && !PlayerStats.Instance.IsDead) OpenOrCloseMenu();

		if (MenuContent.activeSelf) {
			// Items
			if (Inventory.Instance.Items.Count > 0) {
				_bagEmptyObject.SetActive( false );

				if (InputBlockers == 0) {
					// Scrolling and animating
					if (ScrollIndex()) {
						ItemSounds.Instance.Play( ItemSounds.Instance.ShuffleBag, 0.5f );
						UpdateItemInfo();
						if (!_itemPutAwayAnimationIsRunning) {
							ResetAnimation();
							StartCoroutine( PutAwayItem( true ) );
						}
					}

					// Use item
					if (Inventory.Instance.Items.Count > 0 
						&& (Input.GetKeyDown( KeyCode.E ) || Input.GetMouseButtonDown( 0 ))
						&& Inventory.Instance.Items[_currentItemIndex].Item.CanUse) {
						int previousDifferentItemAmount = Inventory.Instance.Items.Count;
						Inventory.Instance.Items[_currentItemIndex].Item.Use();

						if (Inventory.Instance.Items.Count != previousDifferentItemAmount) {
							_currentItemIndex--;
							if (_currentItemIndex < 0) _currentItemIndex = 0;

							if (!_itemPutAwayAnimationIsRunning) {
								// Check if the inventory is empty
								if (Inventory.Instance.Items.Count > 0) {
									ResetAnimation();
									StartCoroutine( PullOutItem() );
								} else {
									ResetAnimation();
									_bagEmptyObject.SetActive( true );
								}
							}
						}

						UpdateItemInfo();
					}
				}
			} else {
				_bagEmptyObject.SetActive( true );
			}


			// Water
			if(Input.GetKeyDown(KeyCode.R)) {
				if (Inventory.Instance.HasEquipped( "Water" )) {
					PlayerStats.Instance.AlterThirst( -0.25f );
					ItemSounds.Instance.Play( ListUtils.RandomObjectFromArray(ItemSounds.Instance.DrinkingSounds) );
				} else if (Inventory.Instance.HasEquipped( "Warm water" )) {
					PlayerStats.Instance.AlterThirst( -0.25f );
					PlayerStats.Instance.AlterTemperature( 2 );
					ItemSounds.Instance.Play( ListUtils.RandomObjectFromArray( ItemSounds.Instance.DrinkingSounds ) );
				}
			}
		}
	}


	private void OpenOrCloseMenu() {
		// Open
		if ( InputExtensions.KeyInArrayDown( OpenCloseKeys ) && MenuContent.activeInHierarchy == false && Inventory.Instance.HasEquipped( "Backpack" )) {
			ItemSounds.Instance.Play(ItemSounds.Instance.OpenBag);
			OpenInventory();
			return;
		}

		// Close
		if ( (Input.GetKeyDown( KeyCode.Escape ) || InputExtensions.KeyInArrayDown( OpenCloseKeys )) && MenuContent.activeSelf )
			CloseInventory();
	}

	public void OpenInventory() {
		GamePauseManager.Instance.Pause( false );
		MenuContent.SetActive( true );

		if ( OnInventoryOpen != null )
			OnInventoryOpen();

		UpdateItemInfo();
		StartCoroutine( PullOutItem() );
	}

	public void CloseInventory() {
		if ( OnInventoryClose != null )
			OnInventoryClose();

		ResetAnimation();

		MenuContent.SetActive( false );
		GamePauseManager.Instance.Unpause();
	}


	// ITEM STUFF
	// ========================================================
	private bool ScrollIndex() {
		int prevIndex = _currentItemIndex;

		// Scroll next
		if (InputExtensions.KeyInArrayDown( SelectNextItemKeys ) || Input.GetAxis( "Mouse ScrollWheel" ) > 0)
			_currentItemIndex++;
		else if (InputExtensions.KeyInArrayDown( SelectPreviousItemKeys ) || Input.GetAxis( "Mouse ScrollWheel" ) < 0 )
			_currentItemIndex--;

		_currentItemIndex = Mathf.Clamp( _currentItemIndex, 0, Inventory.Instance.Items.Count - 1 );

		return (prevIndex != _currentItemIndex);
	}

	private void UpdateItemInfo() {
		if (Inventory.Instance.Items.Count > 0) {
			ItemInfoName.text = Inventory.Instance.Items[_currentItemIndex].Item.Name + " (" + Inventory.Instance.Items[_currentItemIndex].Quantity + ")";
			ItemInfoDescription.text = Inventory.Instance.Items[_currentItemIndex].Item.Description;
		} else {
			ItemInfoName.text = string.Empty;
			ItemInfoDescription.text = string.Empty;
		}
	}

	private GameObject GetCurrentlySelectedImage() {
		foreach (GameObject imageObject in _itemImageObjects)
			if (Inventory.Instance.Items.Count > 0) {
				if (imageObject.name == Inventory.Instance.Items[_currentItemIndex].Item.Name)
					return imageObject;
			} else return _bagEmptyObject;

		return _imageMissingObject;
	}


	// ANIMATION STUFF
	// ========================================================
	private void ResetAnimation() {
		StopCoroutine( "PullOutItem" );
		StopCoroutine( "PutAwayItem" );
		_itemPutAwayAnimationIsRunning = false;

		foreach (GameObject imageObject in _itemImageObjects) {
			imageObject.SetActive( false );
			_currentMoveIntoBagTime = 0f;
			_currentMoveOutOfBagTime = 0f;
		}
	}

	IEnumerator PullOutItem() {
		// Get and initialize image
		_currentlyShownImage = GetCurrentlySelectedImage();
		_currentlyShownImage.transform.localPosition = AnimStartPosition;
		_currentlyShownImage.SetActive( true );

		// Initialize time
		_currentMoveOutOfBagTime = 0f;

		while (_currentMoveOutOfBagTime < MoveOutOfBagTime) {
			_currentMoveOutOfBagTime += Time.deltaTime;
			_currentlyShownImage.transform.localPosition = Vector3.Lerp( AnimStartPosition, AnimEndPosition, _currentMoveOutOfBagTime / MoveOutOfBagTime );
			yield return null;
		}
	}

	IEnumerator PutAwayItem(bool PullOutAfterAnimation) {
		_itemPutAwayAnimationIsRunning = true;

		// And initialize image
		_currentlyShownImage.transform.localPosition = AnimEndPosition;
		_currentlyShownImage.SetActive( true );

		// Initialize time
		_currentMoveIntoBagTime = 0f;

		while ( _currentMoveIntoBagTime < MoveIntoBagTime ) {
			_currentMoveIntoBagTime += Time.deltaTime;
			_currentlyShownImage.transform.localPosition = Vector3.Lerp( AnimEndPosition, AnimStartPosition, _currentMoveIntoBagTime / MoveIntoBagTime );
			yield return null;
		}

		_currentlyShownImage.SetActive( false );

		if (PullOutAfterAnimation) {
			yield return new WaitForSeconds( TimeBetweenAnimations );
			_itemPutAwayAnimationIsRunning = false;
			StartCoroutine( PullOutItem() );
		} else _itemPutAwayAnimationIsRunning = false;
	}
}