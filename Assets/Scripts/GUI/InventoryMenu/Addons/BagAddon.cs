﻿using UnityEngine;

public class BagAddon : MonoBehaviourExtended {

	[SerializeField] private string _requiredItemName;
	[SerializeField] private KeyCode[] _interactionKeys;


	protected override void Start() {
		base.Start();

		// Subscribe
		InventoryUIMain.OnInventoryOpen += OnOpen;

		OnOpen();
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unsubscribe
		InventoryUIMain.OnInventoryOpen -= OnOpen;
	}


	private void Update() {
		if (_interactionKeys.Length == 0) return;

		if(InputExtensions.KeyInArrayDown( _interactionKeys ))
			OnUse();
	}


	protected virtual void OnOpen() {
		gameObject.SetActive( Inventory.Instance.HasEquipped( _requiredItemName ) );
	}

	protected virtual void OnUse() {
		
	}

}