﻿using UnityEngine;

public class PlayerConfig : MonoBehaviourExtended {

	// Singleton
	private static PlayerConfig _instance;

	public static PlayerConfig Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<PlayerConfig>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}


	// SFX
	//==============================================
	[Header( "SFX" )]
	// Main volume
	public PlayerConfiguration<float> VolumeMain = new PlayerConfiguration<float>( 0.75f );

	// Main volume
	public PlayerConfiguration<float> VolumeAmbient = new PlayerConfiguration<float>( 0.75f );


	// Camera
	//==============================================
	[Header( "Camera" )]
	// Bobs the head when walking
	public PlayerConfiguration<bool> DoHeadBob = new PlayerConfiguration<bool>( true );

	// Controlls the speed at which the player looks around
	public PlayerConfiguration<float> MouseLookingSensitivity = new PlayerConfiguration<float>( 5f );


	void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}
}

public class PlayerConfiguration<T> {

	// OnValueChange event
	public delegate void Change();
	public event Change OnValuechange;

	// Value field / prop
	private T _value;
	public T Value {
		get { return _value; }

		set {
			_value = value;
			if (OnValuechange != null) OnValuechange();
		}
	}

	// Default value prop
	public T DefaultValue { private set; get; }

	// Constructor
	public PlayerConfiguration( T value ) {
		Value = value;
		DefaultValue = value;
	}

}