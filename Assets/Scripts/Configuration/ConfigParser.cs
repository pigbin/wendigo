﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;

public class ConfigParser : MonoBehaviour {

	// Singleton
	private static ConfigParser _instance;
	public static ConfigParser Instance {
		get {
			if ( _instance == null )
				_instance = FindObjectOfType<ConfigParser>();
			return _instance;
		}
	}

	// Config file path
	private string _configPath;

	// File data
	private readonly XmlDocument _file = new XmlDocument();
	private XmlNodeList _fileData;


	void Awake() {
		_configPath = Application.dataPath + "/config.xml";
		LoadConfigFile();
		ParseXmlToConfig();
	}


	public void LoadConfigFile() {
		if ( !File.Exists( _configPath ) ) {
			Debug.LogError( "Config file not found!" );
			SaveDefaultFile();
		}
		_file.Load( _configPath );

		_fileData = _file.GetElementsByTagName( "playerConfig" );
	}


	public void ParseXmlToConfig() {
		var notFounds = new List<string>();

		XmlNode configNodes = _fileData[0];
		foreach (XmlNode node in configNodes) {
			notFounds.Add( node.Name );

			switch (node.Name) {
				case "mainVolume":
					PlayerConfig.Instance.VolumeMain.Value = ConvertToType<float>( node.InnerText, PlayerConfig.Instance.VolumeMain.DefaultValue );
					notFounds.Remove( node.Name );
					break;

				case "ambientVolume":
					PlayerConfig.Instance.VolumeAmbient.Value = ConvertToType<float>( node.InnerText, PlayerConfig.Instance.VolumeAmbient.DefaultValue );
					notFounds.Remove( node.Name );
					break;

				case "headBob":
					PlayerConfig.Instance.DoHeadBob.Value = ConvertToType<bool>( node.InnerText, PlayerConfig.Instance.DoHeadBob.DefaultValue );
					notFounds.Remove( node.Name );
					break;

				case "mouseLookingSensitivity":
					PlayerConfig.Instance.MouseLookingSensitivity.Value = ConvertToType<float>( node.InnerText, PlayerConfig.Instance.MouseLookingSensitivity.DefaultValue );
					notFounds.Remove( node.Name );
					break;
			}
		}

		if (notFounds.Count > 0) {
			var notFoundOutput = "The following items in the playerConfig file were not used by the parser: ";

			foreach (string element in notFounds) {
				notFoundOutput += element + ", ";
			}

			Debug.LogWarning( notFoundOutput );
		}
	}

	private T ConvertToType<T>( object value, object fallBackValue ) {
		T newValue;
		try {
			newValue = (T) Convert.ChangeType( value, typeof (T) );
		} catch {
			newValue = (T) Convert.ChangeType( fallBackValue, typeof( T ) );
		}
		return newValue;
	}


	[ContextMenu("Save")]
	public void SaveToFile() {
		var newFile = new XmlDocument();
		XmlNode head = newFile.CreateXmlDeclaration( "1.0", "utf-8", null );
		newFile.AppendChild( head );

		XmlNode config = newFile.CreateElement( "playerConfig" );
		newFile.AppendChild( config );

		// Volume
		//==============================
		AddNodeToXmlDocument( newFile, ref config, "mainVolume", PlayerConfig.Instance.VolumeMain.Value );
		AddNodeToXmlDocument( newFile, ref config, "ambientVolume", PlayerConfig.Instance.VolumeAmbient.Value );

		// Camera
		//==============================
		AddNodeToXmlDocument( newFile, ref config, "headBob", PlayerConfig.Instance.DoHeadBob.Value );
		AddNodeToXmlDocument( newFile, ref config, "mouseLookingSensitivity", PlayerConfig.Instance.MouseLookingSensitivity.Value );

		newFile.Save( _configPath );
	}

	[ContextMenu( "Save Default" )]
	private void SaveDefaultFile() {
		var newFile = new XmlDocument();
		XmlNode head = newFile.CreateXmlDeclaration( "1.0", "utf-8", null );
		newFile.AppendChild( head );

		XmlNode config = newFile.CreateElement( "playerConfig" );
		newFile.AppendChild( config );

		// Volume
		//==============================
		AddNodeToXmlDocument( newFile, ref config, "mainVolume", PlayerConfig.Instance.VolumeMain.DefaultValue );
		AddNodeToXmlDocument( newFile, ref config, "ambientVolume", PlayerConfig.Instance.VolumeAmbient.DefaultValue );

		// Camera
		//==============================
		AddNodeToXmlDocument( newFile, ref config, "headBob", PlayerConfig.Instance.DoHeadBob.DefaultValue );
		AddNodeToXmlDocument( newFile, ref config, "mouseLookingSensitivity", PlayerConfig.Instance.MouseLookingSensitivity.DefaultValue );

		newFile.Save( _configPath );
	}

	private void AddNodeToXmlDocument( XmlDocument document, ref XmlNode parentElement, string nodeName, object value ) {
		XmlNode newNode = document.CreateElement( nodeName );
		newNode.AppendChild( document.CreateTextNode( value.ToString() ) );
		parentElement.AppendChild( newNode );
	}

}
