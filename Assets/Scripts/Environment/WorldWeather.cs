﻿using UnityEngine;

public class WorldWeather : MonoBehaviourExtended {

	// Singleton
	private static WorldWeather _instance;
	public static WorldWeather Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<WorldWeather>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}

	// Camera
	private Camera _skyCamera;

	// Current Weather
	[Header( "Current weather" )]
	public Weather CurrentWeather;

	// Weather fields
	[Header( "Presets" )]
	public Weather SunnyWeather;
	public Weather SnowyWeather;
	public Weather SnowstormWeather;

	// Light
	private Light _light;

	private Material _skyboxMaterial;

	// Weather event
	public delegate void WeatherChange(Weather weather);
	public static event WeatherChange OnWeatherChange;


	void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}


	private void OnLevelWasLoaded( int id ) {
		switch ( WorldTime.Instance.CurrentDay ) {
			case 0:
				ChangeWeather( SnowyWeather );
				break;
			case 1:
				ChangeWeather( SunnyWeather );
				break;
			default:
				Weather newWeather = RandomObjectFromArray( new[] { SunnyWeather, SnowyWeather, SnowstormWeather } );
				ChangeWeather( newWeather );
				break;
		}

		_light = GameObject.FindGameObjectWithTag( "MainLight" ).GetComponent<Light>();
		_skyCamera = GameObject.FindGameObjectWithTag( "SkyCamera" ).GetComponent<Camera>();
	}


	protected override void Start() {
		base.Start();

		OnLevelWasLoaded( Application.loadedLevel );
	}

	private void Update() {
		if(Input.GetKeyDown( KeyCode.Alpha1 ))
			ChangeWeather( SunnyWeather );
		else if ( Input.GetKeyDown( KeyCode.Alpha2 ) )
			ChangeWeather( SnowyWeather );
		else if ( Input.GetKeyDown( KeyCode.Alpha3 ) )
			ChangeWeather( SnowstormWeather );


		if ( IsPaused )
			return;

		if (SceneData.Instance.IsExterior) {
			_skyboxMaterial = RenderSettings.skybox;
			ApplyCurrentWeather();
		}
	}



	public void ChangeWeather( Weather targetWeather ) {
		_skyboxMaterial = RenderSettings.skybox;

		CurrentWeather = targetWeather;
		if(SceneData.Instance.IsExterior) ApplyCurrentWeather();

		if (OnWeatherChange != null) OnWeatherChange (targetWeather);
	}

	private void ApplyCurrentWeather() {
		// Camera settings
		if (_skyCamera == null)
			_skyCamera = GameObject.FindGameObjectWithTag( "SkyCamera" ).GetComponent<Camera>();
		else {
			_skyCamera.clearFlags = CurrentWeather.CameraClearFlag;
			_skyCamera.backgroundColor = Color.Lerp( _skyCamera.backgroundColor, CurrentWeather.Fog.NightColor, WorldTime.RingedTimePercentage( -Vector4.one ) );
			_skyCamera.backgroundColor = Color.Lerp( _skyCamera.backgroundColor, CurrentWeather.Fog.SunriseColor, WorldTime.RingedTimePercentage( WorldTime.Instance.SunriseTimes ) );
			_skyCamera.backgroundColor = Color.Lerp( _skyCamera.backgroundColor, CurrentWeather.Fog.MorningColor, WorldTime.RingedTimePercentage( WorldTime.Instance.MorningTimes ) );
			_skyCamera.backgroundColor = Color.Lerp( _skyCamera.backgroundColor, CurrentWeather.Fog.NoonColor, WorldTime.RingedTimePercentage( WorldTime.Instance.NoonTimes ) );
			_skyCamera.backgroundColor = Color.Lerp( _skyCamera.backgroundColor, CurrentWeather.Fog.AfternoonColor, WorldTime.RingedTimePercentage( WorldTime.Instance.AfternoonTimes ) );
			_skyCamera.backgroundColor = Color.Lerp( _skyCamera.backgroundColor, CurrentWeather.Fog.SunsetColor, WorldTime.RingedTimePercentage( WorldTime.Instance.SunsetTimes ) );
			_skyCamera.backgroundColor = Color.Lerp( _skyCamera.backgroundColor, CurrentWeather.Fog.NightColor, WorldTime.RingedTimePercentage( WorldTime.Instance.NightTimes ) );
		}
		RenderSettings.skybox = CurrentWeather.Skybox;

		// Fog settings
		LerpFog( CurrentWeather.Fog.NightColor, CurrentWeather.Fog.NightDensity, -Vector4.one );
		LerpFog( CurrentWeather.Fog.SunriseColor, CurrentWeather.Fog.SunriseDensity, WorldTime.Instance.SunriseTimes );
		LerpFog( CurrentWeather.Fog.MorningColor, CurrentWeather.Fog.MorningDensity, WorldTime.Instance.MorningTimes );
		LerpFog( CurrentWeather.Fog.NoonColor, CurrentWeather.Fog.NoonDensity, WorldTime.Instance.NoonTimes );
		LerpFog( CurrentWeather.Fog.AfternoonColor, CurrentWeather.Fog.AfternoonDensity, WorldTime.Instance.AfternoonTimes );
		LerpFog( CurrentWeather.Fog.SunsetColor, CurrentWeather.Fog.SunsetDensity, WorldTime.Instance.SunsetTimes );
		LerpFog( CurrentWeather.Fog.NightColor, CurrentWeather.Fog.NightDensity, WorldTime.Instance.NightTimes );

		// Light settings
		LerpLight( CurrentWeather.Lighting.NightLight, -Vector4.one );
		LerpLight( CurrentWeather.Lighting.SunriseLight, WorldTime.Instance.SunriseTimes );
		LerpLight( CurrentWeather.Lighting.MorningLight, WorldTime.Instance.MorningTimes );
		LerpLight( CurrentWeather.Lighting.NoonLight, WorldTime.Instance.NoonTimes );
		LerpLight( CurrentWeather.Lighting.AfternoonLight, WorldTime.Instance.AfternoonTimes );
		LerpLight( CurrentWeather.Lighting.SunsetLight, WorldTime.Instance.SunsetTimes );
		LerpLight( CurrentWeather.Lighting.NightLight, WorldTime.Instance.NightTimes );

		// Skybox
		if ( _skyboxMaterial == null ) {
			_skyboxMaterial = RenderSettings.skybox;
			return;
		}
		SetSkyboxBlend( "_SunriseBlend", WorldTime.Instance.SunriseTimes );
		SetSkyboxBlend( "_MorningBlend", WorldTime.Instance.MorningTimes );
		SetSkyboxBlend( "_NoonBlend", WorldTime.Instance.NoonTimes );
		SetSkyboxBlend( "_AfternoonBlend", WorldTime.Instance.AfternoonTimes );
		SetSkyboxBlend( "_SunsetBlend", WorldTime.Instance.SunsetTimes );
		SetSkyboxBlend( "_NightBlend", WorldTime.Instance.NightTimes );
	}


	private void LerpFog(Color color, float density, Vector4 times) {
		float timeStart = WorldTime.TimeToDayPercentage( times.x, times.y );
		float timeEnd = WorldTime.TimeToDayPercentage( times.z, times.w );
		float timePercent = MathfExtension.RingPercentage( WorldTime.Instance.PercentageOfWholeDay, timeStart, timeEnd );

		RenderSettings.fogColor = Color.Lerp( RenderSettings.fogColor, color, timePercent );
		RenderSettings.fogDensity = Mathf.Lerp( RenderSettings.fogDensity, density, timePercent );
	}

	private void LerpLight( LightSettings ls, Vector4 times) {
		if ( _light == null )
			return;

		float timeStart = WorldTime.TimeToDayPercentage( times.x, times.y );
		float timeEnd = WorldTime.TimeToDayPercentage( times.z, times.w );
		float timePercent = MathfExtension.RingPercentage( WorldTime.Instance.PercentageOfWholeDay, timeStart, timeEnd );

		_light.color = Color.Lerp( _light.color, ls.Color, timePercent );
		_light.intensity = Mathf.Lerp( _light.intensity, ls.Intensity, timePercent );
		_light.transform.eulerAngles = Vector3.Lerp( _light.transform.eulerAngles, ls.Rotation, timePercent );
	}

	private void SetSkyboxBlend( string blendName, Vector4 times ) {
		float timeStart = WorldTime.TimeToDayPercentage( times.x, times.y );
		float timeEnd = WorldTime.TimeToDayPercentage( times.z, times.w );
		_skyboxMaterial.SetFloat( blendName, MathfExtension.RingPercentage( WorldTime.Instance.PercentageOfWholeDay, timeStart, timeEnd ) );
	}

}