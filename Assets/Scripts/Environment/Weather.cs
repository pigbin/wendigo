﻿using System;
using UnityEngine;

[Serializable]
public class Weather {

	[Header( "Sky" )]
	public CameraClearFlags CameraClearFlag;
	public Color SkyColour;
	public Material Skybox;

	[Header( "Fog" )]
	//public Color FogColour;
	//public float FogDensity;
	public DayTimeFog Fog;

	[Header( "Light" )]
	public DayTimeLight Lighting;

	[Header( "Snow" )]
	public GameObject SnowObject;
	public float EmissionRate;

	public float TimedAmbientTemperature {
		get {
			float newTemp = AmbientTemperatures.NightTemperature;
			newTemp = Mathf.Lerp( newTemp, AmbientTemperatures.SunriseTemperature, WorldTime.RingedTimePercentage( WorldTime.Instance.SunriseTimes ) );
			newTemp = Mathf.Lerp( newTemp, AmbientTemperatures.MorningTemperature, WorldTime.RingedTimePercentage( WorldTime.Instance.MorningTimes ) );
			newTemp = Mathf.Lerp( newTemp, AmbientTemperatures.NoonTemperature, WorldTime.RingedTimePercentage( WorldTime.Instance.NoonTimes ) );
			newTemp = Mathf.Lerp( newTemp, AmbientTemperatures.AfternoonTemperature, WorldTime.RingedTimePercentage( WorldTime.Instance.AfternoonTimes ) );
			newTemp = Mathf.Lerp( newTemp, AmbientTemperatures.SunsetTemperature, WorldTime.RingedTimePercentage( WorldTime.Instance.SunsetTimes ) );
			newTemp = Mathf.Lerp( newTemp, AmbientTemperatures.NightTemperature, WorldTime.RingedTimePercentage( WorldTime.Instance.NightTimes ) );
			return newTemp;
		}
	}
	[Header( "Temperature" )]
	public DayTimeTemperature AmbientTemperatures;
}


[Serializable]
public class DayTimeFog {

	public Color SunriseColor;
	public float SunriseDensity;
	[Space(10f)]
	public Color MorningColor;
	public float MorningDensity;
	[Space( 10f )]
	public Color NoonColor;
	public float NoonDensity;
	[Space( 10f )]
	public Color AfternoonColor;
	public float AfternoonDensity;
	[Space( 10f )]
	public Color SunsetColor;
	public float SunsetDensity;
	[Space( 10f )]
	public Color NightColor;
	public float NightDensity;

}

[Serializable]
public class DayTimeTemperature {

	public float SunriseTemperature;
	[Space( 10f )]
	public float MorningTemperature;
	[Space( 10f )]
	public float NoonTemperature;
	[Space( 10f )]
	public float AfternoonTemperature;
	[Space( 10f )]
	public float SunsetTemperature;
	[Space( 10f )]
	public float NightTemperature;

}

[Serializable]
public class DayTimeLight {

	public LightSettings SunriseLight;
	[Space( 10f )]
	public LightSettings MorningLight;
	[Space( 10f )]
	public LightSettings NoonLight;
	[Space( 10f )]
	public LightSettings AfternoonLight;
	[Space( 10f )]
	public LightSettings SunsetLight;
	[Space( 10f )]
	public LightSettings NightLight;

}

[Serializable]
public class LightSettings {

	public Color Color;
	public float Intensity;
	public Vector3 Rotation;

}