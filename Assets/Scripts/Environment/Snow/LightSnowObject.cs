﻿using UnityEngine;

public class LightSnowObject : MonoBehaviourExtended {

	protected override void Start() {
		base.Start();

		// Subscribe
		WorldWeather.OnWeatherChange += OnWeatherChange;

		OnWeatherChange( WorldWeather.Instance.CurrentWeather );
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unsubscribe
		WorldWeather.OnWeatherChange -= OnWeatherChange;
	}


	private void OnWeatherChange( Weather newWeather ) {
		if(newWeather == WorldWeather.Instance.SnowyWeather)
			gameObject.SetActive( true );
		else gameObject.SetActive( false );
	}

}