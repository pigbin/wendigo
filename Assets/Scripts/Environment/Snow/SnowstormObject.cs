﻿using UnityEngine;

public class SnowstormObject : MonoBehaviourExtended {

	protected override void Start() {
		base.Start();

		// Subscribe
		WorldWeather.OnWeatherChange += OnWeatherChange;

		OnWeatherChange( WorldWeather.Instance.CurrentWeather );
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unsubscribe
		WorldWeather.OnWeatherChange -= OnWeatherChange;
	}


	private void OnWeatherChange( Weather newWeather ) {
		if ( newWeather == WorldWeather.Instance.SnowstormWeather )
			gameObject.SetActive( true );
		else gameObject.SetActive( false );
	}

}