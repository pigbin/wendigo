﻿using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class TemperatureZone : MonoBehaviourExtended {

	// Dependencies
	private SphereCollider _collider;
	private Transform _player;

	// Constants
	public const float TemperatureMinimum = -40f;
	public const float TemperatureMaximum = -20f;
	public static readonly Color MinColour = new Color( 95f / 255f, 70f / 255f, 160f / 255f, 1f );
	public static readonly Color MaxColour = new Color( 178f / 255f, 153f / 255f, 242f / 255f, 1f );

	// Temperature
	[Range(-40f, -20f)] public float Temperature;

	// Range
	public float ZoneRange;
	public float MinimumZoneRange;


	protected override void Start() {
		base.Start();

		// Get references
		_collider = GetComponent<SphereCollider>();
		_player = GameObject.FindGameObjectWithTag( "Player" ).transform;

		// Apply stats to collider
		_collider.radius = ZoneRange / 2f;
	}


	private void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag != "Player") return;

		PlayerStats.Instance.TemperatureZones.Add( this );
	}

	private void OnTriggerExit( Collider other ) {
		if ( other.gameObject.tag != "Player" ) return;

		PlayerStats.Instance.TemperatureZones.Remove( this );
	}

	protected override void OnDestroy() {
		base.OnDestroy();
		PlayerStats.Instance.TemperatureZones.Remove( this );
	}


	public float GetStrength() {
		Vector3 dir = _player.position - transform.position;

		if (dir.magnitude <= MinimumZoneRange / 2f)
			return 1f;

		if (dir.magnitude <= ZoneRange / 2f)
			return 1 - (dir.magnitude - MinimumZoneRange / 2f) / ((ZoneRange - MinimumZoneRange) / 2f);

		return 0;
	}

}