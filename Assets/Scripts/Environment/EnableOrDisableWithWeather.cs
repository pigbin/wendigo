﻿using UnityEngine;

public class EnableOrDisableWithWeather : MonoBehaviourExtended {

	[SerializeField] private bool _isVisibleInSunnyWeather;
	[SerializeField] private bool _isVisibleInSnowyWeather;
	[SerializeField] private bool _isVisibleInStormyWeather;


	protected override void Start() {
		base.Start();

		// Sub
		WorldWeather.OnWeatherChange += WeatherChanged;
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unsub
		WorldWeather.OnWeatherChange -= WeatherChanged;
	}


	protected void WeatherChanged( Weather weather ) {
		var isActive = false;

		if (weather == WorldWeather.Instance.SunnyWeather) isActive = _isVisibleInSunnyWeather;
		else if (weather == WorldWeather.Instance.SnowyWeather) isActive = _isVisibleInSnowyWeather;
		else if (weather == WorldWeather.Instance.SnowstormWeather) isActive = _isVisibleInStormyWeather;

		gameObject.SetActive( isActive );
	}

}