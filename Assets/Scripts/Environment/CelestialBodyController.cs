﻿using UnityEngine;

[ExecuteInEditMode]
public class CelestialBodyController : MonoBehaviourExtended {

	private Transform _player;

	[Header("Controls")]
	[SerializeField] private float _distanceToPlayer;
	[SerializeField] [Range( 0f, 360f )] private float _xzRot;
	[SerializeField] [Range( 0f, 360f )] private float _xyRot;

	[Header( "Time positions (xz, xy)" )]
	[SerializeField] private Vector2 _preSunrisePosition;
	[SerializeField] private Vector2 _sunrisePosition;
	[SerializeField] private Vector2 _morningPosition;
	[SerializeField] private Vector2 _noonPosition;
	[SerializeField] private Vector2 _afternoonPosition;
	[SerializeField] private Vector2 _sunsetPosition;
	[SerializeField] private Vector2 _nightPosition;


	protected override void Start() {
		base.Start();

		// Get references
		_player = GameObject.FindGameObjectWithTag( "Player" ).transform;
	}


	private void LateUpdate() {
		if (IsPaused) return;

		var newXZRot = 0f;
		var newXYRot = 0f;
		if ( WorldTime.Instance != null ) {
			// Lerp XZ position
			newXZRot = _nightPosition.x;
			newXZRot = Mathf.Lerp( newXZRot, _preSunrisePosition.x, WorldTime.RingedTimePercentage( WorldTime.Instance.PreSunriseTimes ) );
			newXZRot = Mathf.Lerp( newXZRot, _sunrisePosition.x, WorldTime.RingedTimePercentage( WorldTime.Instance.SunriseTimes ) );
			newXZRot = Mathf.Lerp( newXZRot, _morningPosition.x, WorldTime.RingedTimePercentage( WorldTime.Instance.MorningTimes ) );
			newXZRot = Mathf.Lerp( newXZRot, _noonPosition.x, WorldTime.RingedTimePercentage( WorldTime.Instance.NoonTimes ) );
			newXZRot = Mathf.Lerp( newXZRot, _afternoonPosition.x, WorldTime.RingedTimePercentage( WorldTime.Instance.AfternoonTimes ) );
			newXZRot = Mathf.Lerp( newXZRot, _sunsetPosition.x, WorldTime.RingedTimePercentage( WorldTime.Instance.SunsetTimes ) );
			newXZRot = Mathf.Lerp( newXZRot, _nightPosition.x, WorldTime.RingedTimePercentage( WorldTime.Instance.NightTimes ) );

			newXYRot = _nightPosition.y;
			newXYRot = Mathf.Lerp( newXYRot, _preSunrisePosition.y, WorldTime.RingedTimePercentage( WorldTime.Instance.PreSunriseTimes ) );
			newXYRot = Mathf.Lerp( newXYRot, _sunrisePosition.y, WorldTime.RingedTimePercentage( WorldTime.Instance.SunriseTimes ) );
			newXYRot = Mathf.Lerp( newXYRot, _morningPosition.y, WorldTime.RingedTimePercentage( WorldTime.Instance.MorningTimes ) );
			newXYRot = Mathf.Lerp( newXYRot, _noonPosition.y, WorldTime.RingedTimePercentage( WorldTime.Instance.NoonTimes ) );
			newXYRot = Mathf.Lerp( newXYRot, _afternoonPosition.y, WorldTime.RingedTimePercentage( WorldTime.Instance.AfternoonTimes ) );
			newXYRot = Mathf.Lerp( newXYRot, _sunsetPosition.y, WorldTime.RingedTimePercentage( WorldTime.Instance.SunsetTimes ) );
			newXYRot = Mathf.Lerp( newXYRot, _nightPosition.y, WorldTime.RingedTimePercentage( WorldTime.Instance.NightTimes ) );
		}

		_xzRot = newXZRot;
		_xyRot = newXYRot;


		// Apply rotations
		var xzRot = new Vector3( Mathf.Sin( _xzRot / 360f * 2f * Mathf.PI ), 0f, Mathf.Cos( _xzRot / 360f * 2f * Mathf.PI ) );
		Vector3 xzOrtho = Vector3.Cross( xzRot, Vector3.up );
		xzRot = (Quaternion.AngleAxis( _xyRot, xzOrtho ) * xzRot);

		Vector3 finalRot = (xzRot).normalized * _distanceToPlayer;

		transform.position = _player.position + finalRot;
	}

}