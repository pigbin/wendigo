﻿using UnityEngine;

public class WorldTime : MonoBehaviourExtended {

	// Singleton
	private static WorldTime _instance;
	public static WorldTime Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<WorldTime>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}


	// Minutes and hours
	[Header( "Time" )]
	public float RealMinutesPerDay;
	public float RealMinutesPerNight;
	private float _currentSecondsOfMinute;

	[Range(1, 24)] public int HoursPerDay;
	[Range( 1, 24 )] public int HoursPerNight;
	[Range( 0, 23 )] public int CurrentHour;
	[Range( 0, 60 )] public int CurrentMinute;
	public int CurrentDay;

	[Header( "Daytimes limits" )]
	[Header( "start (h,m) end(h,m)" )]
	public Vector4 PreSunriseTimes;
	public Vector4 SunriseTimes;
	public Vector4 MorningTimes;
	public Vector4 NoonTimes;
	public Vector4 AfternoonTimes;
	public Vector4 SunsetTimes;
	public Vector4 NightTimes;

	public float PercentageOfWholeDay {
		get {
			int minutesOfDay = CurrentMinute + CurrentHour * 60;
			return minutesOfDay / ((HoursPerDay + HoursPerNight) * 60f);
		}
	}

	// Time tick event (Ticks every hour)
	public delegate void TimeTick();
	public event TimeTick OnHourTick;


	void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}

	private void Update() {
		if (!IsPaused && !SceneData.Instance.TimeIsPaused) {
			_currentSecondsOfMinute += Time.deltaTime;

			if(CurrentHour <= HoursPerDay)
				CalculateDayTime();
			else CalculateNightTime();
		}
	}

	private void CalculateDayTime() {
		if ( _currentSecondsOfMinute >= (RealMinutesPerDay / HoursPerDay) ) {
			_currentSecondsOfMinute = 0;
			CurrentMinute++;

			if(CurrentMinute >= 60) {
				CurrentHour++;
				CurrentMinute = 0;

				if ( OnHourTick != null ) OnHourTick();
			}
		}
	}

	private void CalculateNightTime() {
		if ( _currentSecondsOfMinute >= (RealMinutesPerNight / HoursPerNight) ) {
			_currentSecondsOfMinute = 0;
			CurrentMinute++;

			if ( CurrentMinute >= 60 ) {
				CurrentHour++;
				CurrentMinute = 0;
				if ( OnHourTick != null ) OnHourTick();

				// Change day
				if ( CurrentHour >= 24 ) {
					CurrentHour = 0;
					CurrentDay++;
				}
			}
		}
	}


	// Static stuff
	public static float TimeToDayPercentage( float h, float m ) {
		float minutesOfDay = m + h * 60f;
		return minutesOfDay / (24f * 60f);
	}

	public static float RingedTimePercentage( Vector4 times ) {
		float timeStart = TimeToDayPercentage( times.x, times.y );
		float timeEnd = TimeToDayPercentage( times.z, times.w );
		float timePercent = MathfExtension.RingPercentage( Instance.PercentageOfWholeDay, timeStart, timeEnd );

		return timePercent;
	}

}