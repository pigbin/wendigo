﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class StopAudioSourceOnPause : MonoBehaviourExtended {

	private AudioSource _audioSource;

	protected override void Start() {
		base.Start();

		// Get reference
		_audioSource = GetComponent<AudioSource>();
	}


	protected override void OnPauseEnter( bool openPauseMenu ) {
		_audioSource.Pause();
	}

	protected override void OnPauseExit() {
		if (_audioSource.clip != null)
			_audioSource.UnPause();
	}

}