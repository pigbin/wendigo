﻿using UnityEngine;

public static class ApplicationExtensions {

	public static void LoadLevelReleaseCursor(int id) {
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;

		Application.LoadLevel( id );
	}

}