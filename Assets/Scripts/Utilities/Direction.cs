﻿public enum Direction {

	None,
	Up,
	Down,
	Left,
	Right,
	Forward,
	Backward

}