﻿using System;

public static class MathfExtension {

	// Comparisons
	public static bool IsBetween( float value, float min, float max, CompareFlags flags = CompareFlags.IncludeMin | CompareFlags.IncludeMax ) {
		switch (flags) {
			case CompareFlags.IncludeMin:
				return (min <= value) && (value < max);
			case CompareFlags.IncludeMax:
				return (min < value) && (value <= max);
			case (CompareFlags.IncludeMin | CompareFlags.IncludeMax):
				return (min <= value) && (value <= max);
		}

		return (min < value) && (value < max);
	}

	public static bool IsBetween( int value, int min, int max, CompareFlags flags = CompareFlags.IncludeMin | CompareFlags.IncludeMax ) {
		switch ( flags ) {
			case CompareFlags.IncludeMin:
				return (min <= value) && (value < max);
			case CompareFlags.IncludeMax:
				return (min < value) && (value <= max);
			case (CompareFlags.IncludeMin | CompareFlags.IncludeMax):
				return (min <= value) && (value <= max);
		}

		return (min < value) && (value < max);
	}


	// Arrays
	public static int RandomIndexFromArray(object[] array) {
		return UnityEngine.Random.Range( 0, array.Length );
    }


	// Percentage
	public static float RingPercentage(float value, float minVal, float maxVal) {
		if ( value <= minVal ) return 0f;
		if ( value >= maxVal ) return 1f;

		return (value - minVal) / (maxVal - minVal);
	}

}

[Flags]
public enum CompareFlags {

	Exclusive = 0,
	IncludeMin = 1,
	IncludeMax = 2

}