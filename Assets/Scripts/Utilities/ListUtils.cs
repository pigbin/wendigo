﻿public static class ListUtils {

	public static T RandomObjectFromArray<T>( T[] arrayOfObjects ) {
		if (arrayOfObjects.Length == 0)
			return default(T);

		int i = UnityEngine.Random.Range( 0, arrayOfObjects.Length );

		return arrayOfObjects[i];
	}

}