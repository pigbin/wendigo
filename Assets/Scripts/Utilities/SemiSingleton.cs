﻿using UnityEngine;

public class SemiSingleton : MonoBehaviourExtended {

	void Awake() {
		// Protect singleton
		SemiSingleton[] allSingletons = FindObjectsOfType<SemiSingleton>();

		foreach (SemiSingleton singleton in allSingletons)
			if(singleton.gameObject.name == gameObject.name)
				if(singleton != this)
					Destroy( gameObject );
	}

}