﻿using UnityEngine;

public class AnchorToObject : MonoBehaviour {

	public Transform Anchor;
	public bool AnchorPosition;
	public bool AnchorRotation;
	public bool AnchorScale;

	void Update () {
		if (AnchorPosition)
			transform.position = Anchor.position;
		if ( AnchorRotation )
			transform.rotation = Anchor.rotation;
		if ( AnchorPosition )
			transform.localScale = Anchor.localScale;
	}
}
