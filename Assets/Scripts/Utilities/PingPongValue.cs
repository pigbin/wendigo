﻿using UnityEngine;

[System.Serializable]
public class PingPongValue {

	[SerializeField] private float _minValue;
	[SerializeField] private float _maxValue;
	[SerializeField] private float _currentValue;
	[SerializeField] private PingPongDirection _direction;

	public float MinValue { get { return _minValue; } set { _minValue = value; } }
	public float MaxValue { get { return _maxValue; } set { _maxValue = value; } }
	public float CurrentValue { get { return _currentValue; } set { _currentValue = value; } }
	public PingPongDirection Direction { get { return _direction; } set { _direction = value; } }

	// Full constructor
	public PingPongValue( float minValue, float maxValue, float startValue, PingPongDirection startDirection ) {
		_minValue = minValue;
		_maxValue = maxValue;
		_currentValue = startValue;
		_direction = startDirection;
	}

	// Short constructor
	public PingPongValue( float minValue, float maxValue ) {
		_minValue = minValue;
		_maxValue = maxValue;
		_currentValue = minValue;
		_direction = PingPongDirection.Forward;
	}


	public float Next( float t ) {
		if (Direction == PingPongDirection.Forward) {
			CurrentValue += t;

			if (CurrentValue > MaxValue) {
				CurrentValue = MaxValue - (CurrentValue - MaxValue);
				Direction = PingPongDirection.Backward;
			}
		} else {
			CurrentValue -= t;

			if (CurrentValue < MinValue) {
				CurrentValue = MinValue + (MinValue - CurrentValue);
				Direction = PingPongDirection.Forward;
			}
		}

		return CurrentValue;
	}

}

public enum PingPongDirection {

	Forward,
	Backward

}