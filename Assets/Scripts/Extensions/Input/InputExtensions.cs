﻿using System.Linq;
using UnityEngine;

public class InputExtensions {

	public static bool KeyInArrayPressed( KeyCode[] array ) {
		return array.Any( Input.GetKey );
	}

	public static bool KeyInArrayDown( KeyCode[] array ) {
		return array.Any( Input.GetKeyDown );
	}

	public static bool KeyInArrayUp( KeyCode[] array ) {
		return array.Any( Input.GetKeyUp );
	}

}
