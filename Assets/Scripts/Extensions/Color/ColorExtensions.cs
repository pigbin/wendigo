﻿using UnityEngine;

static class ColorExtensions {

	public static bool IsCloseTo( this Color a, Color b, float percentageThreshold ) {
		var rClose = true;
		var gClose = true;
		var bClose = true;

		// Calculate percentages of spectrum
		float rPercent = a.r / b.r;
		float gPercent = a.g / b.g;
		float bPercent = a.b / b.b;

		// Compare
		if ( !float.IsNaN( rPercent ) && !float.IsInfinity( rPercent ) )
			if ( rPercent < percentageThreshold )
				rClose = false;

		if ( !float.IsNaN( gPercent ) && !float.IsInfinity( gPercent ) )
			if ( gPercent < percentageThreshold )
				gClose = false;

		if ( !float.IsNaN( bPercent ) && !float.IsInfinity( bPercent ) )
			if ( bPercent < percentageThreshold )
				bClose = false;

		// Return true if all are close
		return rClose && gClose && bClose;
	}


	public static string ToHex( this Color col ) {
		string output = string.Empty;

		output += ((int) (col.r * 255)).ToString( "X2" );
		output += ((int) (col.g * 255)).ToString( "X2" );
		output += ((int) (col.b * 255)).ToString( "X2" );
		output += ((int) (col.a * 255)).ToString( "X2" );

		return output;
	}

}