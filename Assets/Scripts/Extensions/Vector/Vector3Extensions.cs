﻿using UnityEngine;

public static class Vector3Extensions {

	public static Vector3 GetClosestVectorFromArray(Vector3 vectorToCompare, Vector3[] array ) {
		Vector3 closestRot = Vector3.zero;
		float highest = float.NegativeInfinity;

		foreach ( Vector3 rot in array ) {
			float currentDot = Vector3.Dot( vectorToCompare, rot );
			if ( currentDot > highest ) {
				closestRot = rot;
				highest = currentDot;
			}
		}

		return closestRot;
	}

	public static int GetClosestVectorIndexFromArray( Vector3 vectorToCompare, Vector3[] array ) {
		float highest = float.NegativeInfinity;
		int index = 0;

		for (int i = 0; i < array.Length; i++) {
			float currentDot = Vector3.Dot( vectorToCompare, array[i] );
			if ( currentDot > highest ) {
				highest = currentDot;
				index = i;
			}
		}

		return index;
	}

}
