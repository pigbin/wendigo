﻿using UnityEngine;

public class MonoBehaviourExtended : MonoBehaviour {

	protected virtual void Start() {
		SubscribePauseEvent();
	}

	protected virtual void OnDestroy() {
		UnsubscribePauseEvent();
	}


	// NEW METHODS
	//===========================================================================
	protected virtual void OnPauseEnter(bool openPauseMenu) { }
	protected virtual void OnPauseExit() { }

	protected T RandomObjectFromArray<T>( T[] arrayOfObjects ) {
		if (arrayOfObjects.Length == 0) return default(T);

		int i = Random.Range( 0, arrayOfObjects.Length );

		return arrayOfObjects[i];
	}

	protected void BlockInterface( bool block ) {
		int blockVal = block ? 1 : -1;

		InventoryUIMain.InputBlockers += blockVal;
		GamePauseManager.InputBlockers += blockVal;
	}

	// PAUSE
	//===========================================================================
	protected bool IsPaused;
	private bool _isSubscribed;

	private void SubscribePauseEvent() {
		if (!_isSubscribed) {
			GamePauseManager.OnPause += OnPauseEnter;
			GamePauseManager.OnResume += OnPauseExit;
			GamePauseManager.OnPause += OnPauseEnterInternally;
			GamePauseManager.OnResume += OnPauseExitInternally;
		}

		_isSubscribed = true;
	}

	private void UnsubscribePauseEvent() {
		if (_isSubscribed) {
			GamePauseManager.OnPause -= OnPauseEnter;
			GamePauseManager.OnResume -= OnPauseExit;
			GamePauseManager.OnPause -= OnPauseEnterInternally;
			GamePauseManager.OnResume -= OnPauseExitInternally;
		}

		_isSubscribed = false;
	}

	// These methods are called internally every time the game (un)pauses
	private void OnPauseEnterInternally(bool openPauseMenu) {
		IsPaused = true;
	}

	private void OnPauseExitInternally() {
		IsPaused = false;
	}
}
