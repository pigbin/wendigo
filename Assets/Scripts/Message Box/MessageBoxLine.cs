﻿using UnityEngine;

public class MessageBoxLine {

	public string Text = "";
	public MessageBoxLineType LineType;

	public float DelayPerChar = 0.025f;

	public float DelayBeforeLine = 0f;
	public float DelayAfterLine = 0.25f;

	public MessageBoxLine() {}

	public MessageBoxLine(string text) {
		Text = text;
	}

	public virtual void Select() {
		Debug.Log( "<color=#" + Color.red.ToHex() + ">If you can read this, something went wrong.</color>" );
	}


	protected void QueuePage( MessageBoxPage page ) {
		MessageBoxManager.Instance.QueuePage( page );
	}

	protected void Close() {
		MessageBoxManager.Instance.CloseBox();
	}
}

public enum MessageBoxLineType {

	Text,
	Option

}