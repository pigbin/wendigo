﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MessageBoxManager : MonoBehaviourExtended {

	// Singleton
	private static MessageBoxManager _instance;

	public static MessageBoxManager Instance {
		get {
			if (_instance == null) _instance = FindObjectOfType<MessageBoxManager>();
			return _instance;
		}
	}

	public static bool MessageIsInSession;

	// UI Stuff
	[Header( "UI Elements" )] [SerializeField] private GameObject _content;
	[SerializeField] private Text _textField;
	[SerializeField] private GameObject _messageEndIndicator;
	[SerializeField] private RectTransform _selectionCursor;
	[SerializeField] private float _cursorPositionOne;
	[SerializeField] private float _cursorPositionTwo;
	[SerializeField] private float _cursorPositionThree;

	// Hoomins
	[Header( "Sprites" )]
	[SerializeField] private GameObject _wendyImage;
	[SerializeField] private GameObject _owenImage;
	private GameObject[] _characterObjects;

	// Formatting
	[Header( "Formatting" )]
	[SerializeField] private Color _textColor;
	[SerializeField] private Color _highlightedColor;
	[SerializeField] private Color _chosenColor;
	private const string RichTextColorFormat = "<color=#{1}>{0}</color>";

	// Animation
	[Header( "Animation" )]
	[SerializeField] private float _fadeTimePerChar = 0.25f;
	[SerializeField] private float _selectFadeOutTime;

	// Input
	[Header( "Input" )]
	[SerializeField] private KeyCode[] _skipEndKeys;

	// Debug stuff
	[Header( "Debug stuffzies" )]
	[SerializeField] private MessageBoxPage _debugPage;
	[SerializeField] private MessageBoxPage _debugPageTwo;

	// Control stuff
	private bool _isClosingBox;
	private MessageBoxPage _nextPage;


	protected override void Start() {
		base.Start();

		// Find character images
		Transform itemFolder = transform.GetChild( 0 ).FindChild( "Hoomins" );
		_characterObjects = new GameObject[itemFolder.childCount];
		for (int c = 0; c < itemFolder.childCount; c++) {
			_characterObjects[c] = itemFolder.GetChild( c ).gameObject;
			_characterObjects[c].SetActive( false );
		}
	}


	private void OnLevelWasLoaded( int id ) {
		if (MessageIsInSession) {
			MessageIsInSession = false;
			GamePauseManager.InputBlockers--;
			PlayerController.InputBlockers--;
			InventoryUIMain.InputBlockers--;
		} else if(_content.activeInHierarchy) {
			_isClosingBox = false;
			_content.SetActive( false );
		}
	}


	public static void StartDialogue( MessageBoxPage page ) {
		if (Instance == null)
			throw new Exception( "No instance of MessageBoxManager found!" );
		if (MessageIsInSession)
			throw new Exception( "A message box is already in session!" );

		// Show character
		if(Instance._characterObjects != null)
			foreach (GameObject obj in Instance._characterObjects) {
				obj.SetActive( obj.name == page.Speaker );
			}

		Instance._content.SetActive( true );
		Instance.StartCoroutine( Instance.FadeInMessage( page ) );
	}

	protected IEnumerator FadeInMessage( MessageBoxPage page ) {
		MessageIsInSession = true;

		// Get scenedata so you can revert them later
		bool sdStatsArePaused = SceneData.Instance.StatsArePaused;
		bool sdTimeIsPaused = SceneData.Instance.TimeIsPaused;

		GamePauseManager.InputBlockers++;
		PlayerController.InputBlockers++;
		InventoryUIMain.InputBlockers++;
		SceneData.Instance.StatsArePaused = true;
		SceneData.Instance.TimeIsPaused = true;

		// Reset
		_textField.text = "";

		// Check for linetypes
		var optionsInPage = 0;
		var optionLines = new List<int>();
		if (page.LineOne.LineType == MessageBoxLineType.Option) {
			page.LineOne.Text = "        " + page.LineOne.Text;
			optionLines.Add( 0 );
			optionsInPage++;
		}
		if ( page.LineTwo.LineType == MessageBoxLineType.Option ) {
			page.LineTwo.Text = "        " + page.LineTwo.Text;
			optionLines.Add( 1 );
			optionsInPage++;
		}
		if ( page.LineThree.LineType == MessageBoxLineType.Option ) {
			page.LineThree.Text = "        " + page.LineThree.Text;
			optionLines.Add( 2 );
			optionsInPage++;
		}

		string fullMessage = page.LineOne.Text + "\n" + page.LineTwo.Text + "\n" + page.LineThree.Text;
		int totalMessageLength = fullMessage.Length;

		_messageEndIndicator.SetActive( false );

		// Save every character of the message in an array together with ..
		// .. information on when to show up and the delay to the next character
		var currentMessageInfo = new MessageCharInfo[totalMessageLength];
		for (var c = 0; c < fullMessage.Length; c++) {
			currentMessageInfo[c] = new MessageCharInfo( fullMessage[c], new Color(_textColor.r, _textColor.g, _textColor.b, 0f) );

			if (c <= page.LineOne.Text.Length) {
				// Line 1
				currentMessageInfo[c].FadeTimeStartAt = page.LineOne.DelayPerChar * c + page.LineOne.DelayBeforeLine;

			} else if (c <= page.LineOne.Text.Length + page.LineTwo.Text.Length + 1) {
				// Line 2
				currentMessageInfo[c].FadeTimeStartAt = page.LineTwo.DelayPerChar * (c - page.LineOne.Text.Length) + page.LineOne.DelayBeforeLine + page.LineOne.DelayAfterLine + page.LineTwo.DelayBeforeLine;
				currentMessageInfo[c].FadeTimeStartAt += page.LineOne.Text.Length * page.LineOne.DelayPerChar;

			} else if (c <= page.LineOne.Text.Length + page.LineTwo.Text.Length + page.LineThree.Text.Length + 2) {
				// Line 3
				currentMessageInfo[c].FadeTimeStartAt = page.LineThree.DelayPerChar * (c - page.LineOne.Text.Length - page.LineTwo.Text.Length) + page.LineOne.DelayBeforeLine + page.LineOne.DelayAfterLine + page.LineTwo.DelayBeforeLine + page.LineTwo.DelayAfterLine + page.LineThree.DelayBeforeLine;
				currentMessageInfo[c].FadeTimeStartAt += page.LineOne.Text.Length * page.LineOne.DelayPerChar;
				currentMessageInfo[c].FadeTimeStartAt += page.LineTwo.Text.Length * page.LineTwo.DelayPerChar;
			}
		}

		yield return new WaitForEndOfFrame();

		// Then fade them all until the last one is faded and save them in a string
		var currentTime = 0f;
		while (currentMessageInfo[totalMessageLength - 1].Color.a < 1f) {
			// When pressing enter: Skip shit
			if (InputExtensions.KeyInArrayDown( _skipEndKeys ) && currentTime >= 0.01f) {
				foreach (MessageCharInfo info in currentMessageInfo)
					info.CurrentFadeTime = currentMessageInfo[currentMessageInfo.Length - 1].FadeTimeStartAt + 1f;

				page.LineThree.DelayAfterLine = 0f;
			}

			var finalString = "";
			foreach (MessageCharInfo info in currentMessageInfo) {
				if (currentTime >= info.FadeTimeStartAt) info.CurrentFadeTime += Time.deltaTime;

				float timePercentage = Mathf.Clamp(info.CurrentFadeTime / _fadeTimePerChar, 0f, 1f);
				info.Color.a = timePercentage;

				finalString += ColorCharacterRichText( info.Character, info.Color );
			}

			_textField.text = finalString;
			currentTime += Time.deltaTime;
			yield return null;
		}

		yield return new WaitForSeconds( page.LineThree.DelayAfterLine );

		// Message is now fully displayed. Wait for input to end.
		if (optionsInPage == 0) {
			_messageEndIndicator.SetActive( true );
			while (true) {
				if (InputExtensions.KeyInArrayDown( _skipEndKeys )) {
					_content.SetActive( false );

					if (page.NextPage == null) {
						MessageIsInSession = false;
						break;
					} else {
						QueuePage( page.NextPage );
						break;
					}
				}

				yield return null;
			}

		} else {
			// If there are options: Let the player cycle through them
			_selectionCursor.gameObject.SetActive( true );

			var currentOptionIndex = optionLines.First();
			var firstLoop = true;
			while (true) {
				// Get input - Index
				int newIndex = currentOptionIndex;
				if (Input.GetKeyDown( KeyCode.UpArrow )) {
					if (optionLines.Contains( newIndex - 1 )) newIndex--;
					else if (optionLines.Contains( newIndex - 2 )) newIndex -= 2;
				} else if ( Input.GetKeyDown( KeyCode.DownArrow ) ) {
					if ( optionLines.Contains( newIndex + 1 ) ) newIndex++;
					else if ( optionLines.Contains( newIndex + 2 ) ) newIndex += 2;
				}
				newIndex = Mathf.Clamp( newIndex, 0, 2 );

				bool doRedraw = currentOptionIndex != newIndex;
				currentOptionIndex = newIndex;

				// Display text if index changed
				if (doRedraw || firstLoop) {
					var newString = "";
					for (var c = 0; c < currentMessageInfo.Length; c++) {
						currentMessageInfo[c].Color = _textColor;

						if ( currentOptionIndex == 0 
							&& c <= page.LineOne.Text.Length ) {
							// Line 1
							currentMessageInfo[c].Color = _highlightedColor;

						} else if ( currentOptionIndex == 1 
							&& c > page.LineOne.Text.Length 
							&& c <= page.LineOne.Text.Length + page.LineTwo.Text.Length + 1 ) {
							// Line 2
							currentMessageInfo[c].Color = _highlightedColor;

						} else if ( currentOptionIndex == 2 
							&& c > page.LineTwo.Text.Length + page.LineOne.Text.Length ) {
							// Line 3
							currentMessageInfo[c].Color = _highlightedColor;
						}

						newString += ColorCharacterRichText( currentMessageInfo[c].Character, currentMessageInfo[c].Color );
					}

					_textField.text = newString;

					// Don't forget to move the cursor!
					switch (currentOptionIndex) {
						case 0:
							_selectionCursor.anchoredPosition = new Vector2(0f, _cursorPositionOne);
							break;
						case 1:
							_selectionCursor.anchoredPosition = new Vector2( 0f, _cursorPositionTwo);
							break;
						case 2:
							_selectionCursor.anchoredPosition = new Vector2( 0f, _cursorPositionThree);
							break;
					}
				}

				// Get input - Select
				if (Input.GetKeyDown( KeyCode.Return )) {
					switch (currentOptionIndex) {
						case 0:
							page.LineOne.Select();
							break;
						case 1:
							page.LineTwo.Select();
							break;
						case 2:
							page.LineThree.Select();
							break;
					}

					// Hide cursor
					_selectionCursor.gameObject.SetActive( false );

					// Change color of selection
					for ( var c = 0; c < currentMessageInfo.Length; c++ ) {
						currentMessageInfo[c].Color = _textColor;
						if ( currentOptionIndex == 0 && c <= page.LineOne.Text.Length ) {
							// Line 1
							currentMessageInfo[c].Color = _chosenColor;
						} else if ( currentOptionIndex == 1 && c > page.LineOne.Text.Length && c <= page.LineOne.Text.Length + page.LineTwo.Text.Length + 1 ) {
							// Line 2
							currentMessageInfo[c].Color = _chosenColor;
						} else if ( currentOptionIndex == 2 && c > page.LineTwo.Text.Length + page.LineOne.Text.Length ) {
							// Line 3
							currentMessageInfo[c].Color = _chosenColor;
						}
					}

					// Fade out
					var alpha = 1f;
					while (alpha > 0f) {
						var newString = "";
						foreach (MessageCharInfo info in currentMessageInfo) {
							info.Color.a = alpha;
							newString += ColorCharacterRichText( info.Character, info.Color );
						}
						_textField.text = newString;

						alpha -= Time.deltaTime / _selectFadeOutTime;
						yield return null;
					}
                }

				firstLoop = false;
				if (_isClosingBox || _nextPage != null) break;
				yield return null;
			}
		}

		_isClosingBox = false;
		MessageIsInSession = false;
		_content.SetActive( false );
		GamePauseManager.InputBlockers--;
		PlayerController.InputBlockers--;
		InventoryUIMain.InputBlockers--;
		SceneData.Instance.StatsArePaused = sdStatsArePaused;
		SceneData.Instance.TimeIsPaused = sdTimeIsPaused;

		PlayerStats.Instance.UpdateSanity();

		page.OnFinish();

		if ( _nextPage != null ) {
			page.NextPage = _nextPage;
			_nextPage = null;
			StartDialogue( page.NextPage );
		}
	}


	private string ColorCharacterRichText( char character, Color color ) {
		string hexColor = color.ToHex();
		string output = string.Format( RichTextColorFormat, character, hexColor );
		return output;
	}

	public void CloseBox() {
		_isClosingBox = true;
	}

	public void QueuePage( MessageBoxPage page ) {
		_nextPage = page;
	}

}

[Serializable]
public class MessageCharInfo {

	public char Character;

	public Color Color;

	public float FadeTimeStartAt;
	public float CurrentFadeTime;

	public MessageCharInfo( char c, Color color ) {
		Character = c;
		Color = color;
	}

}