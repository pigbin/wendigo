﻿public class MessageBoxPage {

	public string Speaker;

	public MessageBoxLine LineOne;
	public MessageBoxLine LineTwo;
	public MessageBoxLine LineThree;

	public MessageBoxPage NextPage;

	public virtual void OnFinish() {}

}