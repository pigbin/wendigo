﻿using System;
using UnityEngine;

// ==================================================================
// TalkToWendy01
// ==================================================================
public class TalkToWendy01 : MessageBoxPage {
	public TalkToWendy01() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendy02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I'm tired.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendy02
// ==================================================================
public class TalkToWendy02 : MessageBoxPage {
	public TalkToWendy02() {
		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendy03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I'll leave you alone, then.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendy03
// ==================================================================
public class TalkToWendy03 : MessageBoxPage {
	public TalkToWendy03() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Thank you.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}