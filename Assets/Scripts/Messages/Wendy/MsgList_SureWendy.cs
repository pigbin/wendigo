﻿using System;
using UnityEngine;

// ==================================================================
// Sure01
// ==================================================================
public class Sure01 : MessageBoxPage {
	public Sure01() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new Sure02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "This would make a good bottle sleeve.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Just give me a night and it'll be ready.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}


	public override void OnFinish() {
		GlobalVariables.WendyHasCloth = true;
		Inventory.Instance.RemoveItem( "Piece of cloth", 1 );
    }
}

// ==================================================================
// Sure02
// ==================================================================
public class Sure02 : MessageBoxPage {
	public Sure02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Thank you.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}