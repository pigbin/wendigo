﻿using System;
using UnityEngine;

// ==================================================================
// IPromise01
// ==================================================================
public class IPromise01 : MessageBoxPage {
	public IPromise01() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new IPromise02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Thank you.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I would not want to lose another one.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// IPromise02
// ==================================================================
public class IPromise02 : MessageBoxPage {
	public IPromise02() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Hm, can you give it to me?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Sure.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new Sure01() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "I'd rather keep it.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new IdRatherKeepIt01() );
		}
	}
}