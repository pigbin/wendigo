﻿using System;
using UnityEngine;

// ==================================================================
// IdRatherKeepIt01
// ==================================================================
public class IdRatherKeepIt01 : MessageBoxPage {
	public IdRatherKeepIt01() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh, well. I just thought it would make a good bottle sleeve.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Sure.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new Sure02() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "I don't think I need one.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			Close();
		}
	}
}