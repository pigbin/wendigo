﻿using System;
using UnityEngine;

// ==================================================================
// TalkToWendyAboutOwen01
// ==================================================================
public class TalkToWendyAboutOwen01 : MessageBoxPage {
	public TalkToWendyAboutOwen01() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendyAboutOwen02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I found this at the cliff.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendyAboutOwen02
// ==================================================================
public class TalkToWendyAboutOwen02 : MessageBoxPage {
	public TalkToWendyAboutOwen02() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendyAboutOwen03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh dear, that's a piece of Emma's coat!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendyAboutOwen03
// ==================================================================
public class TalkToWendyAboutOwen03 : MessageBoxPage {
	public TalkToWendyAboutOwen03() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendyAboutOwen04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "It is.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendyAboutOwen04
// ==================================================================
public class TalkToWendyAboutOwen04 : MessageBoxPage {
	public TalkToWendyAboutOwen04() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendyAboutOwen05();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You found this at the cliff?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendyAboutOwen05
// ==================================================================
public class TalkToWendyAboutOwen05 : MessageBoxPage {
	public TalkToWendyAboutOwen05() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendyAboutOwen06();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "It lay in the snow.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendyAboutOwen06
// ==================================================================
public class TalkToWendyAboutOwen06 : MessageBoxPage {
	public TalkToWendyAboutOwen06() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToWendyAboutOwen07();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "That's not good news.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I just hope nothing happened to her.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToWendyAboutOwen07
// ==================================================================
public class TalkToWendyAboutOwen07 : MessageBoxPage {
	public TalkToWendyAboutOwen07() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Please, promise me, you take care of you.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Well ...";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new Well01() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "I promise.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new IPromise01() );
		}
	}
}