﻿using System;
using UnityEngine;

// ==================================================================
// FirstTimeOutside01
// ==================================================================
public class FirstTimeOutside01 : MessageBoxPage {
	public FirstTimeOutside01() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Fuckin' cold.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I better hurry before hunger and thirst kick in.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}