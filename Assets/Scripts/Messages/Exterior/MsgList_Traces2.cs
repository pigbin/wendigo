﻿using System;
using UnityEngine;

// ==================================================================
// Traces2_01
// ==================================================================
public class Traces2_01 : MessageBoxPage {
	public Traces2_01() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I wonder what this is about ...";
			LineType = MessageBoxLineType.Text;
			DelayAfterLine = 0.5f;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Maybe Wendy and Owen know what to do with this.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}