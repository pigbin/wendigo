﻿using System;
using UnityEngine;

// ==================================================================
// EatThePilot01
// ==================================================================
public class EatThePilot01 : MessageBoxPage {
	public EatThePilot01() {
		Speaker = "wendy_m_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new EatThePilot02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I'm hungry.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// EatThePilot02
// ==================================================================
public class EatThePilot02 : MessageBoxPage {
	public EatThePilot02() {
		Speaker = "owen_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new EatThePilot03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I want to eat the pilot.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// EatThePilot03
// ==================================================================
public class EatThePilot03 : MessageBoxPage {
	public EatThePilot03() {
		Speaker = "wendy_m_happy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Me too!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}