﻿public class ClearInventoryPage : MessageBoxPage {

	public ClearInventoryPage() {
		LineOne = new MessageBoxLine("Hey, I'll put the backpack over there.");
		LineTwo = new MessageBoxLine("Just take whatever you need.");
		LineThree = new MessageBoxLine();
	}

}