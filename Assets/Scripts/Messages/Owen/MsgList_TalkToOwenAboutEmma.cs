﻿using System;
using UnityEngine;

// ==================================================================
// TalkToOwenAboutEmma01
// ==================================================================
public class TalkToOwenAboutEmma01 : MessageBoxPage {
	public TalkToOwenAboutEmma01() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmma02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I found something at the cliff.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmma02
// ==================================================================
public class TalkToOwenAboutEmma02 : MessageBoxPage {
	public TalkToOwenAboutEmma02() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmma03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You did?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmma03
// ==================================================================
public class TalkToOwenAboutEmma03 : MessageBoxPage {
	public TalkToOwenAboutEmma03() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmma04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "A piece of cloth.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmma04
// ==================================================================
public class TalkToOwenAboutEmma04 : MessageBoxPage {
	public TalkToOwenAboutEmma04() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmma05();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmma05
// ==================================================================
public class TalkToOwenAboutEmma05 : MessageBoxPage {
	public TalkToOwenAboutEmma05() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmma06();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Give it to me.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmma06
// ==================================================================
public class TalkToOwenAboutEmma06 : MessageBoxPage {
	public TalkToOwenAboutEmma06() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Here.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new HereOwen01() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "Why should I?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WhyShouldI01() );
		}
	}
}