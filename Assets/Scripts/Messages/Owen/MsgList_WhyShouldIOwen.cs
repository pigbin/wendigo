﻿using System;
using UnityEngine;

// ==================================================================
// WhyShouldI01
// ==================================================================
public class WhyShouldI01 : MessageBoxPage {
	public WhyShouldI01() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhyShouldI02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I need something to bandage my arm.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhyShouldI02
// ==================================================================
public class WhyShouldI02 : MessageBoxPage {
	public WhyShouldI02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhyShouldI03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Your arm is already bandaged.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhyShouldI03
// ==================================================================
public class WhyShouldI03 : MessageBoxPage {
	public WhyShouldI03() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhyShouldI04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I need some more ...?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhyShouldI04
// ==================================================================
public class WhyShouldI04 : MessageBoxPage {
	public WhyShouldI04() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhyShouldI05();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Have you hurt yourself?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhyShouldI05
// ==================================================================
public class WhyShouldI05 : MessageBoxPage {
	public WhyShouldI05() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhyShouldI06();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh, look!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhyShouldI06
// ==================================================================
public class WhyShouldI06 : MessageBoxPage {
	public WhyShouldI06() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhyShouldI07();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhyShouldI07
// ==================================================================
public class WhyShouldI07 : MessageBoxPage {
	public WhyShouldI07() {
		Speaker = "owen_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhyShouldI08();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "It's Emma!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhyShouldI08
// ==================================================================
public class WhyShouldI08 : MessageBoxPage {
	public WhyShouldI08() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Jerk.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "What is it with you?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WhatIsItWithYou01() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "So you don't want to talk. I see.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WhatIsItWithYou03() );
		}
	}
}