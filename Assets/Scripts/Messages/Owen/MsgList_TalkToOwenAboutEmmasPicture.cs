﻿using System;
using UnityEngine;

// ==================================================================
// TalkToOwenAboutEmmasPicture01
// ==================================================================
public class TalkToOwenAboutEmmasPicture01 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture01() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I found this in one of the bags.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture02
// ==================================================================
public class TalkToOwenAboutEmmasPicture02 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture02() {
		Speaker = "owen_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What is it?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture03
// ==================================================================
public class TalkToOwenAboutEmmasPicture03 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture03() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "A picture of you.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture04
// ==================================================================
public class TalkToOwenAboutEmmasPicture04 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture04() {
		Speaker = "owen_m_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture05();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Where'd you find it?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture05
// ==================================================================
public class TalkToOwenAboutEmmasPicture05 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture05() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture06();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What's the matter with you?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	public override void OnFinish() {
		Inventory.Instance.RemoveItem( "Photo", 1 );
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture06
// ==================================================================
public class TalkToOwenAboutEmmasPicture06 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture06() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture07();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Hey!";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Give it back!";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture07
// ==================================================================
public class TalkToOwenAboutEmmasPicture07 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture07() {
		Speaker = "owen_m_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture08();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "No need to.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture08
// ==================================================================
public class TalkToOwenAboutEmmasPicture08 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture08() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture09();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "This is enough.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture09
// ==================================================================
public class TalkToOwenAboutEmmasPicture09 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture09() {
		Speaker = "owen_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture10();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Enough of what?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture10
// ==================================================================
public class TalkToOwenAboutEmmasPicture10 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture10() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture11();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Of you!";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "You all!";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture11
// ==================================================================
public class TalkToOwenAboutEmmasPicture11 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture11() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture12();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You sit around here, expect me to cater";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "to your needs, care about your precious";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "food and all you do is";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture12
// ==================================================================
public class TalkToOwenAboutEmmasPicture12 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture12() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture13();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "fucking around your teeny tiny personal problem.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I don't care about your relationship.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "I don't care if you fucked her and she left you";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture13
// ==================================================================
public class TalkToOwenAboutEmmasPicture13 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture13() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture14();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "and you couldn't take it.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I don't care if she's your beloved sister.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "I'm just fed up with you and your capricios schemes";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture14
// ==================================================================
public class TalkToOwenAboutEmmasPicture14 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture14() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture15();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "or whatever you're trying to pull off here.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I ...";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture15
// ==================================================================
public class TalkToOwenAboutEmmasPicture15 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture15() {
		Speaker = "wendy_m_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture16();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Nate!";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture16
// ==================================================================
public class TalkToOwenAboutEmmasPicture16 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture16() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture17();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What's it?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture17
// ==================================================================
public class TalkToOwenAboutEmmasPicture17 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture17() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture18();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Calm down.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture18
// ==================================================================
public class TalkToOwenAboutEmmasPicture18 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture18() {
		Speaker = "owen_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture19();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "She's right, lad.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture19
// ==================================================================
public class TalkToOwenAboutEmmasPicture19 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture19() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture20();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh, just shut the fuck up.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture20
// ==================================================================
public class TalkToOwenAboutEmmasPicture20 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture20() {
		Speaker = "wendy_m_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture21();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "We need to stick together.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Yeah, the situation is tedious. Yes, we have enough of it.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "But please come to your senses and pull yourself togehter.";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture21
// ==================================================================
public class TalkToOwenAboutEmmasPicture21 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture21() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture22();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Yeah, 'cause that's all you need to do.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture22
// ==================================================================
public class TalkToOwenAboutEmmasPicture22 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture22() {
		Speaker = "owen_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture23();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Pullin' yourself together.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture23
// ==================================================================
public class TalkToOwenAboutEmmasPicture23 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture23() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture24();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Shut up!";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture24
// ==================================================================
public class TalkToOwenAboutEmmasPicture24 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture24() {
		Speaker = "wendy_m_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture25();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Survive.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture25
// ==================================================================
public class TalkToOwenAboutEmmasPicture25 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture25() {
		Speaker = "owen_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture26();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You hear?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture26
// ==================================================================
public class TalkToOwenAboutEmmasPicture26 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture26() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture27();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I can't do this.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I'm out.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture27
// ==================================================================
public class TalkToOwenAboutEmmasPicture27 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture27() {
		Speaker = "wendy_m_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture28();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Nate?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture28
// ==================================================================
public class TalkToOwenAboutEmmasPicture28 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture28() {
		Speaker = "owen_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwenAboutEmmasPicture29();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Leave him alone.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

// ==================================================================
// TalkToOwenAboutEmmasPicture29
// ==================================================================
public class TalkToOwenAboutEmmasPicture29 : MessageBoxPage {
	public TalkToOwenAboutEmmasPicture29() {
		Speaker = "wendy_m_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh, you ...";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	public override void OnFinish() {
		GlobalVariables.HasFledPlane = true;
		MessageBoxManager.Instance.CloseBox();
		SceneChanger.Instance.LoadSceneAtLocation( 2, SpawnPoints.Points["extCargoExit"] );
	}
}