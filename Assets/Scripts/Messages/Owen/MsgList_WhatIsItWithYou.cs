﻿using System;
using UnityEngine;

// ==================================================================
// WhatIsItWithYou01
// ==================================================================
public class WhatIsItWithYou01 : MessageBoxPage {
	public WhatIsItWithYou01() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhatIsItWithYou02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "It's important to me.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhatIsItWithYou02
// ==================================================================
public class WhatIsItWithYou02 : MessageBoxPage {
	public WhatIsItWithYou02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhatIsItWithYou03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "So you don't want to talk.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I see.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhatIsItWithYou03
// ==================================================================
public class WhatIsItWithYou03 : MessageBoxPage {
	public WhatIsItWithYou03() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You see well.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}