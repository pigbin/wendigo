﻿using System;
using UnityEngine;

// ==================================================================
// AboutTheCrash01
// ==================================================================
public class AboutTheCrash01 : MessageBoxPage {
	public AboutTheCrash01() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Yeah?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// AboutTheCrash02
// ==================================================================
public class AboutTheCrash02 : MessageBoxPage {
	public AboutTheCrash02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I can't remember much about it. Of course, I know I was on that plane,";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "but after that?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "I thought that maybe you could tell me more about it.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// AboutTheCrash03
// ==================================================================
public class AboutTheCrash03 : MessageBoxPage {
	public AboutTheCrash03() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Tragical.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// AboutTheCrash04
// ==================================================================
public class AboutTheCrash04 : MessageBoxPage {
	public AboutTheCrash04() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash05();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I thought ...";
			LineType = MessageBoxLineType.Text;
			DelayAfterLine = 0.5f;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "shouldn't we stick together?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "Since we're the only ones left?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// AboutTheCrash05
// ==================================================================
public class AboutTheCrash05 : MessageBoxPage {
	public AboutTheCrash05() {
		Speaker = "owen_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash05b();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Listen, boy. I know we're stuck here in this goddamn fucking icy";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "outback and you know we're all survivors of a goddamn fucking plane crash.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
            LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// AboutTheCrash05b
// ==================================================================
public class AboutTheCrash05b : MessageBoxPage {
	public AboutTheCrash05b() {
		Speaker = "owen_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash06();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Yeah, it's cold here, my arm is broken and there's only";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = " one more chocolate bar to eat. Get over yourself.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// AboutTheCrash06
// ==================================================================
public class AboutTheCrash06 : MessageBoxPage {
	public AboutTheCrash06() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash07();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I guess, I better go.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// AboutTheCrash07
// ==================================================================
public class AboutTheCrash07 : MessageBoxPage {
	public AboutTheCrash07() {
		Speaker = "owen_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		//NextPage = new AboutTheCrash07();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Correct.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}