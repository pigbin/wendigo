﻿using System;
using UnityEngine;

// ==================================================================
// WhoAreYou01
// ==================================================================
public class WhoAreYou01 : MessageBoxPage {
	public WhoAreYou01() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhoAreYou02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Some guy on a broken plane.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhoAreYou02
// ==================================================================
public class WhoAreYou02 : MessageBoxPage {
	public WhoAreYou02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhoAreYou03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "My name's Nathan.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhoAreYou03
// ==================================================================
public class WhoAreYou03 : MessageBoxPage {
	public WhoAreYou03() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WhoAreYou04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Owen.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WhoAreYou04
// ==================================================================
public class WhoAreYou04 : MessageBoxPage {
	public WhoAreYou04() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new AboutTheCrash01();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "About the crash ...";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}