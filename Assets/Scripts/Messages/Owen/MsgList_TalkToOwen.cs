﻿using System;
using UnityEngine;

// ==================================================================
// TalkToOwen01
// ==================================================================
public class TalkToOwen01 : MessageBoxPage {
	public TalkToOwen01() {
		Speaker = "player";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwen02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Hi there.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToOwen02
// ==================================================================
public class TalkToOwen02 : MessageBoxPage {
	public TalkToOwen02() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new TalkToOwen03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You awake?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// TalkToOwen03
// ==================================================================
public class TalkToOwen03 : MessageBoxPage {
	public TalkToOwen03() {
		Speaker = "player";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		//NextPage = new TalkToOwen04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Seems so.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "About the crash ...";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new AboutTheCrash01() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "Who are you?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WhoAreYou01() );
		}
	}
}


// ==================================================================
// OwenIdle01
// ==================================================================
public class OwenIdle01 : MessageBoxPage {
	public OwenIdle01() {
		Speaker = "owen_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Fuck off.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}