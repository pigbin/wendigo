﻿using System;
using UnityEngine;

// ==================================================================
// HereOwen01
// ==================================================================
public class HereOwen01 : MessageBoxPage {
	public HereOwen01() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new HereOwen02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Why are you putting it away?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Aren't you ...";
			LineType = MessageBoxLineType.Text;
			DelayAfterLine = 0.5f;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "going to do something with this?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	public override void OnFinish() {
		GlobalVariables.OwenHasCloth = true;
		Inventory.Instance.RemoveItem( "Piece of cloth", 1 );
	}
}

// ==================================================================
// HereOwen02
// ==================================================================
public class HereOwen02 : MessageBoxPage {
	public HereOwen02() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new HereOwen03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "That's none of your business.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// HereOwen03
// ==================================================================
public class HereOwen03 : MessageBoxPage {
	public HereOwen03() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new HereOwen04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "But ...";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// HereOwen04
// ==================================================================
public class HereOwen04 : MessageBoxPage {
	public HereOwen04() {
		Speaker = "owen_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You don't have to care!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}