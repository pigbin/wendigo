﻿using UnityEngine;

// ==================================================================
// NEED JACKET AND BACKPACK
// ==================================================================
public class PlaneExitReqBoth : MessageBoxPage {
	public PlaneExitReqBoth() {
		Speaker = "wendy";
		LineOne = new MessageBoxLine( "Are you going outside?" );
		LineTwo = new MessageBoxLine( "It's freezing cold out there." );
		LineThree = new MessageBoxLine( "You shouldn't go without a jacket." );
	}
}

// ==================================================================
// NEED JACKET
// ==================================================================
public class PlaneExitReqJacket : MessageBoxPage {
	public PlaneExitReqJacket() {
		Speaker = "wendy";
		LineOne = new MessageBoxLine( "It's freezing cold outside." );
		LineTwo = new MessageBoxLine( "Better search for a jacket." );
		LineThree = new MessageBoxLine( "" );
	}
}

// ==================================================================
// NEED BACKPACK
// ==================================================================
public class PlaneExitReqBackpack : MessageBoxPage {
	public PlaneExitReqBackpack() {
		Speaker = "wendy";
		LineOne = new MessageBoxLine( "Hm, you could need something to store stuff." );
		LineTwo = new MessageBoxLine( "You might find something useful out there." );
		LineThree = new MessageBoxLine( "" );
	}
}

// ==================================================================
// PlaneExitHasBoth01
// ==================================================================
public class PlaneExitHasBoth01 : MessageBoxPage {
	public PlaneExitHasBoth01() {
		Speaker = "owen";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new PlaneExitHasBoth02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Try to find food. I'm starving.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// PlaneExitHasBoth02
// ==================================================================
public class PlaneExitHasBoth02 : MessageBoxPage {
	public PlaneExitHasBoth02() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	public override void OnFinish() {
		MessageBoxManager.Instance.CloseBox();
		SceneChanger.Instance.LoadSceneAtLocation( 2, SpawnPoints.Points["extCargoExit"] );
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Just take everything that seems useful with you!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "We'll see what we can make of it.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}