﻿using System;
using UnityEngine;

// ==================================================================
// YouThinkSo_01
// ==================================================================
public class YouThinkSo_01 : MessageBoxPage {
	public YouThinkSo_01() {
		Speaker = "wendy_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new SeemsUnrealistic_01();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "It has to be!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// SeemsUnrealistic_01
// ==================================================================
public class SeemsUnrealistic_01 : MessageBoxPage {
	public SeemsUnrealistic_01() {
		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new SeemsUnrealistic_02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Seems unrealistic.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// SeemsUnrealistic_02
// ==================================================================
public class SeemsUnrealistic_02 : MessageBoxPage {
	public SeemsUnrealistic_02() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh, I just don't want to think about it!";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}