﻿using System;
using UnityEngine;

// ==================================================================
// Traces1
// ==================================================================
public class Traces1 : MessageBoxPage {
	public Traces1() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new Traces1_2();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Maybe. I ... we don't know.";
			DelayAfterLine = 1f;
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "She went outside to look if there was someone out there,";
			DelayAfterLine = 0f;
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "but since it seemed we're all alone here ...";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// Traces1_2
// ==================================================================
public class Traces1_2 : MessageBoxPage {
	public Traces1_2() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new Traces1_3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "She went once more. To collect food.";
			DelayAfterLine = 1.5f;
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Or wood to have it warm in here.";
			DelayAfterLine = 1.5f;
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "But she never returned.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// Traces1_3
// ==================================================================
public class Traces1_3 : MessageBoxPage {
	public Traces1_3() {
		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new Traces1_4();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// Traces1_4
// ==================================================================
public class Traces1_4 : MessageBoxPage {
	public Traces1_4() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Oh, she must be alright. She just met someone and left.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "You think so?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new YouThinkSo_01() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "Seems unrealistic.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new SeemsUnrealistic_02() );
		}
	}
}