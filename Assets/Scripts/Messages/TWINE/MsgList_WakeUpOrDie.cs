﻿using System;
using UnityEngine;

// ==================================================================
// PAGE 1
// ==================================================================
public class WendyIntroP01 : MessageBoxPage {
	public WendyIntroP01() {
		Speaker = "wendy_worried";

		LineOne = new MessageBoxLine( "Hey, wake up!" );
		LineTwo = new MessageBoxLine();
		LineThree = new MessageBoxLine();

		NextPage = new WendyIntroP02();
	}
}

// ==================================================================
// PAGE 2
// ==================================================================
public class WendyIntroP02 : MessageBoxPage {
	public WendyIntroP02() {
		//Speaker = "wendy";

		LineOne = new MessageBoxLine( "Uuuh ..." );
		LineTwo = new MessageBoxLine();
		LineThree = new MessageBoxLine( "" );

		NextPage = new WendyIntroP03();
	}
}

// ==================================================================
// PAGE 3
// ==================================================================
public class WendyIntroP03 : MessageBoxPage {
	public WendyIntroP03() {
		Speaker = "wendy_worried";

		LineOne = new MessageBoxLine( "Are you okay?" );
		LineTwo = new WendyIntroP03_Line2();
		LineThree = new WendyIntroP03_Line3();
	}

	public class WendyIntroP03_Line2 : MessageBoxLine {
		public WendyIntroP03_Line2() {
			Text = "Ah, I ... I think so ...";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WendyIntroP04_2() );
		}
	}

	public class WendyIntroP03_Line3 : MessageBoxLine {
		public WendyIntroP03_Line3() {
			Text = "No, my head is hurting.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new MessageBoxPage() {
				LineOne = new MessageBoxLine( "Hm, maybe you should rest some more." ),
				LineTwo = new MessageBoxLine(),
				LineThree = new MessageBoxLine()
			} );
		}
	}
}

// ==================================================================
// PAGE 4_2 "Ah"
// ==================================================================
public class WendyIntroP04_2 : MessageBoxPage {
	public WendyIntroP04_2() {
		Speaker = "wendy_happy";

		LineOne = new MessageBoxLine( "I'm so glad you made it." );
		LineTwo = new WendyIntroP04_2_Line2();
		LineThree = new WendyIntroP04_2_Line3();
	}

	class WendyIntroP04_2_Line2 : MessageBoxLine {
		public WendyIntroP04_2_Line2() {
			Text = "What happened?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WendyIntroP05_2() );
		}
	}

	class WendyIntroP04_2_Line3 : MessageBoxLine {
		public WendyIntroP04_2_Line3() {
			Text = "Thank you.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			Close();
		}
	}
}

// ==================================================================
// PAGE 5_2 "What happened?"
// ==================================================================
public class WendyIntroP05_2 : MessageBoxPage {
	public WendyIntroP05_2() {
		Speaker = "wendy";

		LineOne = new MessageBoxLine( "You don't remember?" );
		LineTwo = new MessageBoxLine( "Ah, why else should you ask?" );
		LineThree = new MessageBoxLine( "Well, we ... crashed." );

		NextPage = new WendyIntro_Story01();
	}
}

// ==================================================================
// WendyIntro_Story01
// ==================================================================
public class WendyIntro_Story01 : MessageBoxPage {
	public WendyIntro_Story01() {
		Speaker = "wendy_crying";

		LineOne = new MessageBoxLine() {
			Text = "It was horrible. I ...",
			DelayAfterLine = 0.75f
		};
		LineTwo = new MessageBoxLine() {
			Text = "I just wanted to finally fly home to my parents",
			DelayAfterLine = 0f
		};
		LineThree = new MessageBoxLine( "during the semester break and then ..." );

		NextPage = new WendyIntro_Story01answer();
	}
}

// ==================================================================
// WendyIntro_Story01answer
// ==================================================================
public class WendyIntro_Story01answer : MessageBoxPage {
	public WendyIntro_Story01answer() {
		LineOne = new MessageBoxLine( "I'm sorry." );
		LineTwo = new WendyIntro_Story01answer_Line2();
		LineThree = new WendyIntro_Story01answer_Line3();
	}

	public class WendyIntro_Story01answer_Line2 : MessageBoxLine {
		public WendyIntro_Story01answer_Line2() {
			Text = "You don't have to go on.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WendyIntro_Story01DontGoOn() );
		}
	}

	public class WendyIntro_Story01answer_Line3 : MessageBoxLine {
		public WendyIntro_Story01answer_Line3() {
			Text = "What then?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WendyIntro_Story01WhatThen01() );
		}
	}
}

// ==================================================================
// WendyIntro_Story01DontGoOn
// ==================================================================
public class WendyIntro_Story01DontGoOn : MessageBoxPage {
	public WendyIntro_Story01DontGoOn() {
		Speaker = "wendy";

		LineOne = new MessageBoxLine( "Thank you." );
		LineTwo = new MessageBoxLine( "Maybe you should ask Owen about it." );
		LineThree = new MessageBoxLine( "You know, the guy over there?" );
	}
}

// ==================================================================
// WendyIntro_Story01WhatThen01
// ==================================================================
public class WendyIntro_Story01WhatThen01 : MessageBoxPage {
	public WendyIntro_Story01WhatThen01() {
		Speaker = "wendy";

		LineOne = new MessageBoxLine( "I ..." );
		LineTwo = new MessageBoxLine( "Oh, there was that announcement about a zone of turbulence or something." );
		LineThree = new MessageBoxLine( "And we noticed the plane flying low and wondered and ..." );

		NextPage = new WendyIntro_Story01WhatThen02();
	}
}

// ==================================================================
// WendyIntro_Story01WhatThen02
// ==================================================================
public class WendyIntro_Story01WhatThen02 : MessageBoxPage {
	public WendyIntro_Story01WhatThen02() {
		Speaker = "wendy";

		LineOne = new MessageBoxLine( "I'm sorry, I don't know what to say." );
		LineTwo = new MessageBoxLine();
		LineThree = new MessageBoxLine();

		NextPage = new WendyIntro_Story01WhatThen03();
	}
}

// ==================================================================
// WendyIntro_Story01WhatThen03
// ==================================================================
public class WendyIntro_Story01WhatThen03 : MessageBoxPage {
	public WendyIntro_Story01WhatThen03() {
		LineOne = new MessageBoxLine( "It's okay." );
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "You don't have to go on.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WendyIntro_Story01DontGoOn() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "Are we the only ones who made it?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new WendyIntro_Story01_OnlyOnes() );
		}
	}
}

// ==================================================================
// WendyIntro_Story01_OnlyOnes
// ==================================================================
public class WendyIntro_Story01_OnlyOnes : MessageBoxPage {
	public WendyIntro_Story01_OnlyOnes() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new WendyIntro_Story01_OwenWendy();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Well, Owen's here, too.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Over there, you see?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "And then there was Emma. But she left us.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

// ==================================================================
// WendyIntro_Story01_OwenWendy
// ==================================================================
public class WendyIntro_Story01_OwenWendy : MessageBoxPage {
	public WendyIntro_Story01_OwenWendy() {
		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Hm, I'll go talk to Owen then.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "She left? You mean she died?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new Traces1() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}