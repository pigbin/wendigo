﻿public class CollectPicture : MessageBoxPage {
	public CollectPicture() {
		Speaker = "nathan";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Who is this woman?";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

	}
}