﻿public class EmmasPictureWendy01 : MessageBoxPage {
	public EmmasPictureWendy01() {
		Speaker = "nathan";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new EmmasPictureWendy02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Is this Emma?";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

	}
}

public class EmmasPictureWendy02 : MessageBoxPage {
	public EmmasPictureWendy02() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new EmmasPictureWendy03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Ah, yes! Where did you find it?";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

	}
}

public class EmmasPictureWendy03 : MessageBoxPage {
	public EmmasPictureWendy03() {
		Speaker = "nathan";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new EmmasPictureWendy04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Amongst those bags.";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "What is it with Owen and her?";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

	}
}

public class EmmasPictureWendy04 : MessageBoxPage {
	public EmmasPictureWendy04() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Didn't I tell you? She's his girlfriend.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "No, you didn't.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new NoYouDidnt() );

		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "Now, that's interesting";
			LineType = MessageBoxLineType.Option;

		}

		public override void Select() {
			QueuePage( new NowThatsInteresting01() );

		}
	}
}


public class NoYouDidnt : MessageBoxPage {
	public NoYouDidnt() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I must have missed it. But they already seemed a bit strange.";
			LineType = MessageBoxLineType.Text;
		}

	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "In what way?";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new InWhatWay() );

		}

	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "Yeah, Owen's an interesting guy.";
			LineType = MessageBoxLineType.Option;

		}

		public override void Select() {
			QueuePage( new YeahOwensAnInterestingGuy01() );
		}
	}
}

public class InWhatWay : MessageBoxPage {
	public InWhatWay() {
		Speaker = "wendy_m_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = null;
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "They seemed to quarrel. I don't know what it was all about.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "You see, my head was bursting with pain and I was actually trying to sleep.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}


public class NowThatsInteresting01 : MessageBoxPage {
	public NowThatsInteresting01() {
		Speaker = "wendy_m_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new NowThatsInteresting02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Yeah. And strange.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I actually don't know how to feel about it.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class NowThatsInteresting02 : MessageBoxPage {
	public NowThatsInteresting02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new NowThatsInteresting03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What's the matter?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class NowThatsInteresting03 : MessageBoxPage {
	public NowThatsInteresting03() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new NowThatsInteresting04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "They were quarreling and then ...";
			LineType = MessageBoxLineType.Text;
			DelayAfterLine = 0.5f;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "she was gone.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "As if she left to leave Owen.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class NowThatsInteresting04 : MessageBoxPage {
	public NowThatsInteresting04() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new InWhatWay02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I see.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}


public class YeahOwensAnInterestingGuy01 : MessageBoxPage {
	public YeahOwensAnInterestingGuy01() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "You think so?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy02 : MessageBoxPage {
	public YeahOwensAnInterestingGuy02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I don't really know what I think about him.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "He's secretive.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy03 : MessageBoxPage {
	public YeahOwensAnInterestingGuy03() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I don't know much about you, either.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy04 : MessageBoxPage {
	public YeahOwensAnInterestingGuy04() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy05();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Pardon?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy05 : MessageBoxPage {
	public YeahOwensAnInterestingGuy05() {
		Speaker = "wendy_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy06();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Who are you?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy06 : MessageBoxPage {
	public YeahOwensAnInterestingGuy06() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy07();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Nathan White ...";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "And you are?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy07 : MessageBoxPage {
	public YeahOwensAnInterestingGuy07() {
		Speaker = "wendy_m_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy08();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Wendy Kilworth.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "But what do you know about me?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "That I survived a plane crash.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy08 : MessageBoxPage {
	public YeahOwensAnInterestingGuy08() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy09();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Did you know I study biology?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy09 : MessageBoxPage {
	public YeahOwensAnInterestingGuy09() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy10();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "No. I didn't.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy10 : MessageBoxPage {
	public YeahOwensAnInterestingGuy10() {
		Speaker = "wendy_m";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy11();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "So, who are you?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy11 : MessageBoxPage {
	public YeahOwensAnInterestingGuy11() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy12();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I fled from the loony bin.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy12 : MessageBoxPage {
	public YeahOwensAnInterestingGuy12() {
		Speaker = "wendy_m_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new YeahOwensAnInterestingGuy13();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Seriously?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class YeahOwensAnInterestingGuy13 : MessageBoxPage {
	public YeahOwensAnInterestingGuy13() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "...";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}