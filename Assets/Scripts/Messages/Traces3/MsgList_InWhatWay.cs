﻿public class InWhatWay01 : MessageBoxPage {
	public InWhatWay01() {
		Speaker = "wendy_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new InWhatWay02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "They seemed to quarrel. I don't know what it was all about.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "My head was bursting with pain and I was actually trying to sleep.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

public class InWhatWay02 : MessageBoxPage {
	public InWhatWay02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new InWhatWay03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Don't you think there is something ...";
			LineType = MessageBoxLineType.Text;
			DelayAfterLine = 0.5f;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "suspicious about them?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

public class InWhatWay03 : MessageBoxPage {
	public InWhatWay03() {
		Speaker = "wendy_angry";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new InWhatWay04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What do you mean?";
			LineType = MessageBoxLineType.Text;
        }
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

public class InWhatWay04 : MessageBoxPage {
	public InWhatWay04() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new InWhatWay05();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "He seems to hide something.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

public class InWhatWay05 : MessageBoxPage {
	public InWhatWay05() {
		Speaker = "wendy_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new InWhatWay06();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "He's stressed. We all are.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I am. You are.";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}

public class InWhatWay06 : MessageBoxPage {
	public InWhatWay06() {
		Speaker = "wendy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Aren't you?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Maybe.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new Maybe01() );
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "I'm not sure.";
			LineType = MessageBoxLineType.Option;
		}

		public override void Select() {
			QueuePage( new ImNotSure01() );
		}
	}
}



public class Maybe01 : MessageBoxPage {
	public Maybe01() {
		Speaker = "wendy_happy";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "See?";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "Now you are the suspicious guy!";
			LineType = MessageBoxLineType.Text;
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}
	}
}



public class ImNotSure01 : MessageBoxPage {
	public ImNotSure01() {
		Speaker = "wendy_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new ImNotSure02();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "How can you not be sure about that?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class ImNotSure02 : MessageBoxPage {
	public ImNotSure02() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new ImNotSure03();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "I just am. Not. Ah ...";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class ImNotSure03 : MessageBoxPage {
	public ImNotSure03() {
		Speaker = "wendy_worried";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();

		NextPage = new ImNotSure04();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "What's it with you?";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}

public class ImNotSure04 : MessageBoxPage {
	public ImNotSure04() {
		Speaker = "";

		LineOne = new Line1();
		LineTwo = new Line2();
		LineThree = new Line3();
	}

	class Line1 : MessageBoxLine {
		public Line1() {
			Text = "Nothing.";
			LineType = MessageBoxLineType.Text;
			DelayAfterLine = 0.5f;
		}

		public override void Select() {
		}
	}

	class Line2 : MessageBoxLine {
		public Line2() {
			Text = "I don't know.";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}

	class Line3 : MessageBoxLine {
		public Line3() {
			Text = "";
			LineType = MessageBoxLineType.Text;
		}

		public override void Select() {
		}
	}
}