﻿public static class GlobalVariables {

	public static bool OwenHasCloth;
	public static bool WendyHasCloth;

	public static bool HasFoundPicture;

	public static bool NeedsSleep;

	public static bool HasFledPlane;

	public static float? SanityBuff;

}