﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ItemSounds : MonoBehaviourExtended {

	// Singleton
	private static ItemSounds _instance;
	public static ItemSounds Instance {
		get {
			if(_instance == null) {
				_instance = FindObjectOfType<ItemSounds>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}

	private AudioSource _audioSource;

	[Header( "Sounds" )]
	public AudioClip OpenBag;
	public AudioClip ShuffleBag;
	public AudioClip[] EatingSounds;
	public AudioClip[] DrinkingSounds;

	public AudioClip Sleep;


	void Awake() {
		// Protect singleton
		if (_instance == null) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if (this != _instance)
				Destroy( gameObject );
		}
	}


	protected override void Start() {
		base.Start();

		// Get references
		_audioSource = GetComponent<AudioSource>();
	}


	public void Play(AudioClip sound) {
		_audioSource.PlayOneShot( sound );
	}

	public void Play( AudioClip sound, float vol ) {
		_audioSource.PlayOneShot( sound, vol );
	}
}