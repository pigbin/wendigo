﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class UISounds : MonoBehaviourExtended {

	// Singleton
	private static UISounds _instance;
	public static UISounds Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<UISounds>();
				DontDestroyOnLoad( _instance.gameObject );
			}

			return _instance;
		}
	}

	private AudioSource _audioSource;

	// List of sounds I guess
	[SerializeField] private AudioClip _noiseEffect;
	public AudioClip UiClick;
	public AudioClip UiSelect;


	protected override void Start() {
		base.Start();

		// Get references
		_audioSource = GetComponent<AudioSource>();
	}

	private void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}


	public void Play( AudioClip clip ) {
		if(PlayerStats.Instance.CurrentSanity > 0.25f)
			_audioSource.PlayOneShot( clip );
		else
			_audioSource.PlayOneShot( _noiseEffect );
	}

}