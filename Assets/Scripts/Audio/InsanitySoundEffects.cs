using UnityEngine;
using UnityEngine.Audio;

public class InsanitySoundEffects : MonoBehaviourExtended {

	private AudioSource _audioSource;

	// Sounds
	[SerializeField] private AudioMixer _audioMixer;

	[Header( "25%" )]
	[SerializeField] private AudioClip _footstepDistanceSound;

	[Header( "50%" )]
	[SerializeField] private AudioClip _growlSound;
	[SerializeField] private float _heartbeatMaxVolume;
	private float _heartbeatCurrentVolume = -80f;

	[Header( "Random sounds #1" )]
	[SerializeField] private AudioClip[] _randomSoundPoolOne;
	private float _rspOneCurrentTime;
	private float _rspOneCurrentSoundTime;
	[SerializeField] private float _rspOneMinTime;
	[SerializeField] private float _rspOneMaxTime;

	[Header( "Random sounds #2" )]
	[SerializeField] private AudioClip[] _randomSoundPoolTwo;
	private float _rspTwoCurrentTime;
	private float _rspTwoCurrentSoundTime;
	[SerializeField] private float _rspTwoMinTime;
	[SerializeField] private float _rspTwoMaxTime;

	[Header("25%")]
	[SerializeField] private float _beepMaxVolume;
	private float _beepCurrentVolume = -80f;
	[Header( "Distant shouts" )]
	[SerializeField] private AudioClip[] _distantShoutsPool;
	private float _distantShoutsCurrentTime;
	private float _distantShoutsCurrentSoundTime;
	[SerializeField] private float _distantShoutsMinTime;
	[SerializeField] private float _distantShoutsMaxTime;


	protected override void Start() {
		base.Start();

		// Get references
		_audioSource = GetComponent<AudioSource>();

		// Subscribe
		PlayerController.OnStopMoving += OnStopMoving;

		// Get random timers
		_rspOneCurrentSoundTime = Random.Range( _rspOneMinTime, _rspOneMaxTime );
		_rspTwoCurrentSoundTime = Random.Range( _rspTwoMinTime, _rspTwoMaxTime );
		_distantShoutsCurrentSoundTime = Random.Range( _distantShoutsMinTime, _distantShoutsMaxTime );
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		// Unsubscribe
		PlayerController.OnStopMoving -= OnStopMoving;
	}

	private void Update() {
		if (IsPaused) return;

		InsanityHeartbeat();
		InsanityBeep();

		if(MathfExtension.IsBetween(PlayerStats.Instance.CurrentSanity, 0.25f, 0.5f, CompareFlags.IncludeMax))
			InsanityRandomSounds();

		if (MathfExtension.IsBetween( PlayerStats.Instance.CurrentSanity, 0f, 0.25f, CompareFlags.IncludeMax ))
			InsanityShouts();
	}


	protected override void OnPauseEnter( bool openPauseMenu ) {
		_audioSource.Pause();
	}

	protected override void OnPauseExit() {
		if (_audioSource.clip != null)
			_audioSource.UnPause();
	}


	private void OnStopMoving() {
		InsanityOnStopMovement();
	}


	// IN SANITY
	// ====================================
	private void InsanityHeartbeat() {
		if(MathfExtension.IsBetween( PlayerStats.Instance.CurrentSanity, 0.25f, 0.75f, CompareFlags.IncludeMax ) )
			_heartbeatCurrentVolume = Mathf.Lerp( _heartbeatCurrentVolume, _heartbeatMaxVolume, Time.deltaTime * 0.5f );
		else
			_heartbeatCurrentVolume = Mathf.Lerp( _heartbeatCurrentVolume, -80f, Time.deltaTime * 0.5f );

		_audioMixer.SetFloat( "Heartbeat", _heartbeatCurrentVolume );
	}

	private void InsanityBeep() {
		if (PlayerStats.Instance.CurrentSanity <= 0.25f)
			_beepCurrentVolume = Mathf.Lerp( _beepCurrentVolume, _beepMaxVolume, Time.deltaTime * 0.25f );
		else
			_beepCurrentVolume = Mathf.Lerp( _beepCurrentVolume, -80f, Time.deltaTime * 0.25f );

		_audioMixer.SetFloat( "InsanityBeep", _beepCurrentVolume );
	}

	private void InsanityOnStopMovement() {
		if (MathfExtension.IsBetween( PlayerStats.Instance.CurrentSanity, 0.5f, 0.75f, CompareFlags.IncludeMax )) {
			// MILD
			_audioSource.PlayOneShot( _footstepDistanceSound );

		} else if (MathfExtension.IsBetween( PlayerStats.Instance.CurrentSanity, 0.25f, 0.5f, CompareFlags.IncludeMin )) {
			// MODERATE
			_audioSource.PlayOneShot( _growlSound );
		}
	}

	private void InsanityRandomSounds() {
		_rspOneCurrentTime += Time.deltaTime;
		_rspTwoCurrentTime += Time.deltaTime;

		// #1
		if(_rspOneCurrentTime >= _rspOneCurrentSoundTime) {
			_rspOneCurrentTime = 0f;
			_rspOneCurrentSoundTime = Random.Range( _rspOneMinTime, _rspOneMaxTime );

			// Play random sound from array
			int randIndex = Random.Range( 0, _randomSoundPoolOne.Length );
			_audioSource.PlayOneShot( _randomSoundPoolOne[randIndex] );
		}

		// #2
		if (_rspTwoCurrentTime >= _rspTwoCurrentSoundTime) {
			_rspTwoCurrentTime = 0f;
			_rspTwoCurrentSoundTime = Random.Range( _rspTwoMinTime, _rspTwoMaxTime );

			// Play random sound from array
			int randIndex = Random.Range( 0, _randomSoundPoolTwo.Length );
			_audioSource.PlayOneShot( _randomSoundPoolTwo[randIndex] );
		}
	}

	private void InsanityShouts() {
		_distantShoutsCurrentTime += Time.deltaTime;

		if (_distantShoutsCurrentTime >= _distantShoutsCurrentSoundTime) {
			_distantShoutsCurrentTime = 0f;
			_distantShoutsCurrentSoundTime = Random.Range( _distantShoutsMinTime, _distantShoutsMaxTime );

			// Play random sound from array
			int randIndex = Random.Range( 0, _distantShoutsPool.Length );
			_audioSource.PlayOneShot( _distantShoutsPool[randIndex] );
		}
	}
}