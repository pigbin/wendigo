﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SceneLoadSounds : MonoBehaviourExtended {

	// Singleton
	private static SceneLoadSounds _instance;
	public static SceneLoadSounds Instance {
		get {
			if ( _instance == null ) {
				_instance = FindObjectOfType<SceneLoadSounds>();
			}

			return _instance;
		}
	}

	private AudioSource _audioSource;

	[Header( "Sounds" )]
	[SerializeField] private AudioClip _planeEnterSound;


	protected override void Start() {
		base.Start();

		// Get references
		_audioSource = GetComponent<AudioSource>();
	}


	public void EnterPlaneSound() {
		_audioSource.PlayOneShot( _planeEnterSound );
	}
}