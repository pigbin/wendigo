﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioControl : MonoBehaviour {

	// Singleton
	private static AudioControl _instance;

	public static AudioControl Instance {
		get {
			if ( _instance == null ) {
				_instance = FindObjectOfType<AudioControl>();
				DontDestroyOnLoad( _instance.gameObject );
			}
			return _instance;
		}
	}


	void Awake() {
		// Protect singleton
		if ( _instance == null ) {
			_instance = this;
			DontDestroyOnLoad( this );
		} else {
			if ( this != _instance )
				Destroy( gameObject );
		}
	}


	// Inspector fields
	[SerializeField] private AudioMixer _masterMixer;
	[SerializeField] private int _maxLevel;
	[SerializeField] private int _minLevel;


	void Start() {
		SetAllVolumes();
	}

	void SetAllVolumes() {
		SetVolume( "mainVolume", PlayerConfig.Instance.VolumeMain.Value );
		SetVolume( "ambienceVolume", PlayerConfig.Instance.VolumeAmbient.Value );
	}


	public void SetVolume( string volumeName, float percentage ) {
		_masterMixer.SetFloat( volumeName, _minLevel + percentage * (_maxLevel - _minLevel) );
	}

}
