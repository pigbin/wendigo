﻿using UnityEngine;

public class WeatherSound : MonoBehaviourExtended {

	private AudioSource _audioSource;

	// Sounds
	[Header( "Sounds Ext" )]
	[SerializeField] private AudioClip _sunnySoundExt;
	[SerializeField] private AudioClip _snowySoundExt;
	[SerializeField] private AudioClip _snowStormSoundExt;

	[Header( "Sounds Int" )]
	[SerializeField] private AudioClip _sunnySoundInt;
	[SerializeField] private AudioClip _snowySoundInt;
	[SerializeField] private AudioClip _snowStormSoundInt;


	protected override void Start () {
		base.Start ();

		// Get references
		_audioSource = GetComponent<AudioSource> ();

		// Subscribe to events
		WorldWeather.OnWeatherChange += WeatherChangeEvent;

		WeatherChangeEvent( WorldWeather.Instance.CurrentWeather );
	}


	private void OnLevelWasLoaded(int id) {
		if ( _audioSource == null )
			_audioSource = GetComponent<AudioSource>();

		//WeatherChangeEvent( WorldWeather.Instance.CurrentWeather );
	}


	protected override void OnDestroy () {
		base.OnDestroy ();

		// Unsubscribe from events
		WorldWeather.OnWeatherChange -= WeatherChangeEvent;
	}

	private void WeatherChangeEvent(Weather weather) {
		_audioSource.enabled = true;

		//Debug.Log( weather );
		//Debug.Log( weather == WorldWeather.Instance.SunnyWeather );
		//Debug.Log( weather == WorldWeather.Instance.SnowyWeather );
		//Debug.Log( weather == WorldWeather.Instance.SnowstormWeather );
		//Debug.Log( "---" );

		if (weather == WorldWeather.Instance.SunnyWeather) {
			_audioSource.clip = SceneData.Instance.IsExterior ? _sunnySoundExt : _sunnySoundInt;
			//Debug.Log( 1 );
		} else if (weather == WorldWeather.Instance.SnowyWeather) {
			_audioSource.clip = SceneData.Instance.IsExterior ? _snowySoundExt : _snowySoundInt;
			//Debug.Log( 2 );
		} else if (weather == WorldWeather.Instance.SnowstormWeather) {
			_audioSource.clip = SceneData.Instance.IsExterior ? _snowStormSoundExt : _snowStormSoundInt;
			//Debug.Log( 3 );
		} else
			_audioSource.clip = null;

		_audioSource.Stop();
		_audioSource.Play();

		//Debug.Log( "-------------------------------------------" );
	}


	protected override void OnPauseEnter (bool openPauseMenu) {
		_audioSource.Pause();
	}

	protected override void OnPauseExit () {
		if (_audioSource.clip != null)
			_audioSource.UnPause();
	}

}