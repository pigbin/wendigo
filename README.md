![gitHead.png](https://bitbucket.org/repo/RKLrL9/images/2147041764-gitHead.png)

Play as a survivor of a plane crash in a desolated Canadian mountain region. You have to fight against the cold, hunger and thirst while also getting more and more delusional and paranoid � Or is it your fellow survivors who are getting mad?

The gameplay is split. During the day, you are outside and search for food or a way to reach civilisation, while also fleeing from the semi-real monster that is hunting you. In the evening, you are in the plane together with the other people, talking to them and dispensing the food you (may have) found.

**Genre:** Horror Survival

**Controller:** First Person

**Platform:** PC

**Engine:** Unity 5

**Job of Marco Picker:** Programming and asset implementation

#Project management#
A [Trello board](https://trello.com/b/zftOtLjz/wendy) is available to all team members of the project.